<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists('getUserName')){
	function getUserName($user_id){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('user/user_model', 'user');
		$data 	= $CI->user->get_all_user(['where'=>['user_id'=>$user_id]]);
		if(!empty($data)){

			if($data[0]->user_fullname){
				return $data[0]->user_fullname;
			}

			return $data[0]->user_name;

		}else{
			return $user_id.'-notfound';
		}
	}
}
if(!function_exists('getUserData')){
	function getUserData($user_id = null){
		if($user_id == null){
			return 'cant not be null';
		}
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('user/user_model', 'user');
		$data 	= $CI->user->get_all_user(['where'=>['user_id'=>$user_id], 'single'=>true]);

		if(!empty($data)){
			return $data;
		}

		return '';
	}
}

if(!function_exists('getProductname')){
	function getProductname($product_id, $skuOnly = false){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('product/product_model', 'product');
		$data 	= $CI->product->get_all_product(['where'=>['product_id'=>$product_id]]);
		if(!empty($data)){

			if($skuOnly){
				return $data[0]->product_sku;
			}

			return $data[0]->product_name;
		}else{
			return $product_id.'-';
		}
	}
}

if(!function_exists('getWarehouseProduct')){
	function getWarehouseProduct($product_id, $warehouse_id, $single = false){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('product/Product_warehouse_model', 'product_warehouse');
		$options['where'] = array();

		if(!empty($product_id)){
			$options['where'] = array_merge($options['where'], array(
				'product_id'=>$product_id
			));
		}

		if(!empty($warehouse_id)){
			$options['where'] = array_merge($options['where'], array(
				'warehouse_id'=>$warehouse_id
			));
		}

		$data 	= $CI->product_warehouse->get_data($options);
		if(!empty($data)){
			if($single){
				return $data[0];
			}
			return $data;
		}else{
			return false;
		}
	}
}

if(!function_exists('getWarehouseName')){
	function getWarehouseName($warehouse_id){
		if(empty($warehouse_id)){
			return false;
		}
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('inventory/warehouse_model', 'warehouse');

		$data 	= $CI->warehouse->get_data(array(
			'where'=>array(
				'warehouse_id'=>$warehouse_id
			),
			'single'=>true
		));
		if(!empty($data)){
			return $data->warehouse_name;
		}else{
			return false;
		}
	}
}
if(!function_exists('productSize')){
	function productSize($product_id){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('product/product_model', 'product');
		$CI->load->model('product/size_model', 'size');
		$dataProduct 	= $CI->product->get_all_product(array(
			'where'=>array('product_id'=>$product_id),
			'single'=>true
		));

		if($dataProduct){
			$dataSize = $CI->size->get_all_sizes(array(
				'where'=>array('size_id'=>$dataProduct->size_id),
				'single'=>true
			));
			return ($dataSize) ? $dataSize->size_name : "-";
		}else{
			return '';
		}
	}
}

if(!function_exists('sizeName')){
	function sizeName($size_id = null){
		if(empty($size_id)){
			return null;
		}
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$dataSize = $CI->load->model('product/size_model', 'size');
		$dataSize = $CI->size->get_all_sizes(array(
			'where'=>array('size_id'=>$size_id),
			'single'=>true
		));
		return ($dataSize) ? $dataSize->size_name : "";

	}
}

if(!function_exists('getCourierName')){
	function getCourierName($courier_id){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('shipping/courier_model', 'courier');
		$data 	= $CI->courier->get_all(['where'=>['courier_id'=>$courier_id]]);
		if(!empty($data)){
			return $data[0]->courier_name;
		}else{
			return $courier_id.'-notfound';
		}
	}
}

if(!function_exists('getProductCategoryname')){
	function getProductCategoryname($category_id){
		$CI 	=& get_instance();
		$CI->load->model('base_model');
		$CI->load->model('product/Categories_model', 'categories');
		$data 	= $CI->categories->get_all_categories(['where'=>['category_id'=>$category_id]]);
		if(!empty($data)){
			return $data[0]->category_name;
		}else{
			return $category_id.'-notfound';
		}
	}
}

if(!function_exists('getGroupName')){
	function getGroupName($group_id){
		if(empty($group_id)){
			return '';
		}

		$CI 	=& get_instance();
		$CI->load->model('user/User_group_model', 'group');

		$data_group = $CI->group->get_data(array(
			'where'=>array(
				'group_id'=>$group_id
			),
			'single'=>true
		));

		if(!empty($data_group)){
			return $data_group->group_name;
		}
	}
}

if(!function_exists('getColorName')){
	function getColorName($color_id){
		if(empty($color_id)){
			return '';
		}

		$CI 	=& get_instance();
		$CI->load->model('product/color_model', 'color');

		$data_group = $CI->color->get_data(array(
			'where'=>array(
				'color_id'=>$color_id
			),
			'single'=>true
		));

		if(!empty($data_group)){
			return $data_group->color_name;
		}
	}
}

if(!function_exists('checkRestriction')){
	function isRestriction($user_id, $menu_name){
		if(empty($user_id)){
			return '';
		}

		$user_restriction = modules::run('pengaturan/get_setting', 'role_restriction');
		if(!empty($user_restriction)){
			$user_restriction = unserialize($user_restriction->option_value);
			if(!empty($user_restriction[getRoleNameByUser($user_id)])){
				return in_array($menu_name, $user_restriction[getRoleNameByUser($user_id)]);
			}
		}

		return false;
	}
}

if(!function_exists('getRoleNameByUser')){
	function getRoleNameByUser($user_id){
		$CI 	=& get_instance();
		if(!empty($user_id)){
			$CI->load->model('user/Mapping_model', 'mapping');
			$CI->mapping->set_table('mapping_user_role');
			$roles = $CI->mapping->get_data(array(
				'where'=>array(
					'mapping_user_role.user_id'=>$user_id
				),
				'join'=>array(
					'user_role'=>'mapping_user_role.role_id = user_role.role_id'
				),
			));
			foreach($roles as $role){
				return $role->role_name;
			}
		}

	}
}
if(!function_exists('isHasRole')){
	function isHasRole($user_id, $role_name){
		$CI 	=& get_instance();

		// TODO TABEL mapping_user_role
		$CI->load->model('user/Mapping_model', 'mapping');
		$CI->mapping->set_table('mapping_user_role');
		$data_role = $CI->mapping->get_data(array(
			'where'=>array(
				'mapping_user_role.user_id'=>$user_id
			),
			'join'=>array(
				'user_role'=>'mapping_user_role.role_id = user_role.role_id'
			)
		));
		if(!empty($data_role)){
			foreach($data_role as $role){
				if(strtoupper($role->role_name) == strtoupper($role_name)){
					return true;
				}
			}
		}

		return false;
	}
}
if(!function_exists('getRoleId')){
	function getRoleId($role_name = null){
		$CI 	=& get_instance();
		if(empty($role_name)){
			return null;
		}

		if(!empty($role_name)){
			$CI->load->model('user/user_role_model', 'user_role');
			$data 	= $CI->user_role->get_data(array(
				'where'	=> array(
					'role_name'	=> $role_name
				),
				'single'=>true
			));
			return !empty($data->role_id) ? $data->role_id : null;
		}

	}
}

if (!function_exists('getuserGroup')) {
	function getuserGroup( $user_id = null, $withName = false ){
		$CI =& get_instance();
		$listGroup 		   = array();
		$listGroupWithName = array();

		if ( !empty($user_id) ) {
			$CI->load->model('user/Mapping_model', 'mapping');
			$CI->mapping->set_table('mapping_user_group');
			$group_mapping = $CI->mapping->get_mapping(array(
				'where'=>array(
					'user_id'=>$user_id
				)
			));

			foreach ($group_mapping as $group) {
				$listGroup[] = $group->group_id;
			}

			if ( $withName != false ) {
				$CI->load->model('user/user_group_model', 'user_group');

				foreach ($listGroup as $value) {
					$groupData = $CI->user_group->get_all_group(array(
						'where'  => array('group_id' => $value),
						'single' => TRUE
					));

					$listGroupWithName[$value] = $groupData->group_name;
				}

			}
		}

		return ($withName != false) ? $listGroupWithName : $listGroup ;
	}
}

if( ! function_exists('orderStatuses') ):
	function orderStatuses(){
		$orderStatuses = array(
			"auto-draft" 	=> "Auto Draft",
			"on-hold"		=> "On Hold",
			"processing"	=> "Processing",
			"completed"		=> "Completed",
			"cancelled"		=> "Cancelled",
			"shipped"		=> "Shipped",
			"complete-added-vstock" => "Added To Stock"
		);

		return $orderStatuses;
	}
endif;

if( ! function_exists('bankLists') ):
	function bankLists(){
		$banks = modules::run('pengaturan/getAllSettings', 'bank_view');

		if(!empty($banks['bank']) && $banks['bank'] != '-' ){
			$banks = unserialize($banks['bank']);
			$banks = array_map(function($data){
				return (object) $data;
			}, $banks);
			return $banks;
		}
		return [];
	}
endif;

if( ! function_exists('unitLists') ):
	function unitLists(){
		$unitLists = array(
			"kg"	=> "Kilograms",
			"g"		=> "Grams",
		);

		return $unitLists;
	}
endif;

if( ! function_exists('stockStatus') ):
	function stockStatus(){
		$stockStatuses = array(
			"1"	=> "In Stock",
			"0" => "Out Of Stock",
		);

		return $stockStatuses;
	}
endif;

if( ! function_exists('generateKey') ):
	function generateKey(){
		$CI 		=& get_instance();
		$user_id	= $CI->session->userdata('uid');

		return md5( $user_id . microtime() . rand() );
	}
endif;

if( ! function_exists('activeMenu') ):
	function activeMenu($stringToCompare){
		$CI			=& get_instance();
		$uri_string	= $CI->uri->uri_string();

		return ( $stringToCompare == $uri_string ) ? " active" : "";
	}
endif;

if( ! function_exists('openSubmenu') ):
	function openTreeMenu($stringtoCompare = array()){
		$CI			=& get_instance();
		$uri_string	= $CI->uri->uri_string();

		foreach($stringtoCompare AS $uri):
			if( strpos($uri_string, $uri) !== FALSE ):
				return " open";
			endif;
		endforeach;

		return "";
	}
endif;

if( ! function_exists('openSubmenu') ):
	function openSubMenu($stringtoCompare = array()){
		$CI			=& get_instance();
		$uri_string	= $CI->uri->uri_string();

		foreach($stringtoCompare AS $uri):
			if( strpos($uri_string, $uri) !== FALSE ):
				return ' style="display: block;"';
			endif;
		endforeach;

		return "";
	}
endif;

if( ! function_exists('allUserdata') ):
	function allUserdata(){
		$CI =& get_instance();
		return $CI->session->all_userdata();
	}
endif;

if( ! function_exists('generatePagination') ):
	function generatePagination($total_rows = 0, $per_page = 0, $base_url){
		$CI =& get_instance();

		$CI->load->library('pagination');

		$CI->pagination->initialize(array(
			"base_url"          => $base_url,
			"total_rows"        => $total_rows,
			"per_page"          => $per_page,
			"use_page_numbers"  => TRUE,
			"first_link"        => TRUE,
			"last_link"         => TRUE,
			"num_links"         => 3,
			"full_tag_open"     => '<ul class="pagination pagination-lg justify-content-end">',
			"full_tag_close"    => '</ul>',
			"num_tag_open"      => '<li class="page-item">',
			"num_tag_close"     => '</li>',
			"cur_tag_open"      => '<li class="active"><a class="page-link" href="#">',
			"cur_tag_close"     => '</a></li>',
			"prev_link"         => '&laquo;',
			"prev_tag_open"     => '<li>',
			"prev_tag_close"    => '</li>',
			"next_link"         => '&raquo;',
			"next_tag_open"     => '<li>',
			"next_tag_close"    => '</li>',
			"attributes"        => array(
				"class" => "page-link"
			)
		));

		return $CI->pagination->create_links();
	}
endif;

if (!function_exists('strpos_array')) {
	function strpos_array($haystack, $needle, $offset = 0) {
	    if(!is_array($needle)) $needle = array($needle);
	    foreach($needle as $query) {
	        if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
	    }
	    return false;
	}
}

if (!function_exists('exchelCol')) {
	function exchelCol($colNum = null) {
		$alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		$alphabetNum = count($alphabet) - 1;
		if($colNum === null){
			echo 'error Need number input';
			exit;
		}
		
		if($colNum > $alphabetNum){
			$firstDigit = $colNum % ($alphabetNum + 1);
			return $alphabet[(floor($colNum / ($alphabetNum+ 1)) - 1)].$alphabet[$firstDigit];
		}
		return $alphabet[$colNum];
	}
}

/**
 * Convert number with unit byte to bytes unit
 * @link https://en.wikipedia.org/wiki/Metric_prefix
 * @param string $value a number of bytes with optinal SI decimal prefix (e.g. 7k, 5mb, 3GB or 1 Tb)
 * @return integer|float A number representation of the size in BYTES (can be 0). otherwise FALSE
 * source : https://gist.github.com/Chengings/9597366
 */
if(!function_exists('str2bytes')){
	function str2bytes($value) {
		// only string
		$unit_byte = preg_replace('/[^a-zA-Z]/', '', $value);
		$unit_byte = strtolower($unit_byte);
		// only number (allow decimal point)
		$num_val = preg_replace('/\D\.\D/', '', $value);
		switch ($unit_byte) {
			case 'p':	// petabyte
			case 'pb':
				$num_val *= 1024;
			case 't':	// terabyte
			case 'tb':
				$num_val *= 1024;
			case 'g':	// gigabyte
			case 'gb':
				$num_val *= 1024;
			case 'm':	// megabyte
			case 'mb':
				$num_val *= 1024;
			case 'k':	// kilobyte
			case 'kb':
				$num_val *= 1024;
			case 'b':	// byte
			return $num_val *= 1;
				break; // make sure
			default:
				return FALSE;
		}
		return FALSE;
	}
}

/**
 * Function to make clean slug
 * Source : https://stackoverflow.com/a/9535967
 */
function format_uri( $string, $separator = '-' ){
    $accents_regex	= '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $special_cases	= array( '&' => 'and', "'" => '');
    $string			= mb_strtolower( trim( $string ), 'UTF-8' );
    $string 		= str_replace( array_keys($special_cases), array_values( $special_cases), $string );
    $string	 		= preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string 		= preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string 		= preg_replace("/[$separator]+/u", "$separator", $string);

	return $string;
}

/**
 * Credit to Woocommerce Team
 * source : https://github.com/woocommerce/woocommerce/blob/master/i18n/countries.php
 */
if( ! function_exists('countryLists') ):
	function countryLists(){
		return array(
			'AF' => 'Afghanistan',
			'AX' => '&#197;land Islands',
			'AL' => 'Albania',
			'DZ' => 'Algeria',
			'AS' => 'American Samoa',
			'AD' => 'Andorra',
			'AO' => 'Angola',
			'AI' => 'Anguilla',
			'AQ' => 'Antarctica',
			'AG' => 'Antigua and Barbuda',
			'AR' => 'Argentina',
			'AM' => 'Armenia',
			'AW' => 'Aruba',
			'AU' => 'Australia',
			'AT' => 'Austria',
			'AZ' => 'Azerbaijan',
			'BS' => 'Bahamas',
			'BH' => 'Bahrain',
			'BD' => 'Bangladesh',
			'BB' => 'Barbados',
			'BY' => 'Belarus',
			'BE' => 'Belgium',
			'PW' => 'Belau',
			'BZ' => 'Belize',
			'BJ' => 'Benin',
			'BM' => 'Bermuda',
			'BT' => 'Bhutan',
			'BO' => 'Bolivia',
			'BQ' => 'Bonaire, Saint Eustatius and Saba',
			'BA' => 'Bosnia and Herzegovina',
			'BW' => 'Botswana',
			'BV' => 'Bouvet Island',
			'BR' => 'Brazil',
			'IO' => 'British Indian Ocean Territory',
			'VG' => 'British Virgin Islands',
			'BN' => 'Brunei',
			'BG' => 'Bulgaria',
			'BF' => 'Burkina Faso',
			'BI' => 'Burundi',
			'KH' => 'Cambodia',
			'CM' => 'Cameroon',
			'CA' => 'Canada',
			'CV' => 'Cape Verde',
			'KY' => 'Cayman Islands',
			'CF' => 'Central African Republic',
			'TD' => 'Chad',
			'CL' => 'Chile',
			'CN' => 'China',
			'CX' => 'Christmas Island',
			'CC' => 'Cocos (Keeling) Islands',
			'CO' => 'Colombia',
			'KM' => 'Comoros',
			'CG' => 'Congo (Brazzaville)',
			'CD' => 'Congo (Kinshasa)',
			'CK' => 'Cook Islands',
			'CR' => 'Costa Rica',
			'HR' => 'Croatia',
			'CU' => 'Cuba',
			'CW' => 'Cura&ccedil;ao',
			'CY' => 'Cyprus',
			'CZ' => 'Czech Republic',
			'DK' => 'Denmark',
			'DJ' => 'Djibouti',
			'DM' => 'Dominica',
			'DO' => 'Dominican Republic',
			'EC' => 'Ecuador',
			'EG' => 'Egypt',
			'SV' => 'El Salvador',
			'GQ' => 'Equatorial Guinea',
			'ER' => 'Eritrea',
			'EE' => 'Estonia',
			'ET' => 'Ethiopia',
			'FK' => 'Falkland Islands',
			'FO' => 'Faroe Islands',
			'FJ' => 'Fiji',
			'FI' => 'Finland',
			'FR' => 'France',
			'GF' => 'French Guiana',
			'PF' => 'French Polynesia',
			'TF' => 'French Southern Territories',
			'GA' => 'Gabon',
			'GM' => 'Gambia',
			'GE' => 'Georgia',
			'DE' => 'Germany',
			'GH' => 'Ghana',
			'GI' => 'Gibraltar',
			'GR' => 'Greece',
			'GL' => 'Greenland',
			'GD' => 'Grenada',
			'GP' => 'Guadeloupe',
			'GU' => 'Guam',
			'GT' => 'Guatemala',
			'GG' => 'Guernsey',
			'GN' => 'Guinea',
			'GW' => 'Guinea-Bissau',
			'GY' => 'Guyana',
			'HT' => 'Haiti',
			'HM' => 'Heard Island and McDonald Islands',
			'HN' => 'Honduras',
			'HK' => 'Hong Kong',
			'HU' => 'Hungary',
			'IS' => 'Iceland',
			'IN' => 'India',
			'ID' => 'Indonesia',
			'IR' => 'Iran',
			'IQ' => 'Iraq',
			'IE' => 'Republic of Ireland',
			'IM' => 'Isle of Man',
			'IL' => 'Israel',
			'IT' => 'Italy',
			'CI' => 'Ivory Coast',
			'JM' => 'Jamaica',
			'JP' => 'Japan',
			'JE' => 'Jersey',
			'JO' => 'Jordan',
			'KZ' => 'Kazakhstan',
			'KE' => 'Kenya',
			'KI' => 'Kiribati',
			'KW' => 'Kuwait',
			'KG' => 'Kyrgyzstan',
			'LA' => 'Laos',
			'LV' => 'Latvia',
			'LB' => 'Lebanon',
			'LS' => 'Lesotho',
			'LR' => 'Liberia',
			'LY' => 'Libya',
			'LI' => 'Liechtenstein',
			'LT' => 'Lithuania',
			'LU' => 'Luxembourg',
			'MO' => 'Macao S.A.R., China',
			'MK' => 'Macedonia',
			'MG' => 'Madagascar',
			'MW' => 'Malawi',
			'MY' => 'Malaysia',
			'MV' => 'Maldives',
			'ML' => 'Mali',
			'MT' => 'Malta',
			'MH' => 'Marshall Islands',
			'MQ' => 'Martinique',
			'MR' => 'Mauritania',
			'MU' => 'Mauritius',
			'YT' => 'Mayotte',
			'MX' => 'Mexico',
			'FM' => 'Micronesia',
			'MD' => 'Moldova',
			'MC' => 'Monaco',
			'MN' => 'Mongolia',
			'ME' => 'Montenegro',
			'MS' => 'Montserrat',
			'MA' => 'Morocco',
			'MZ' => 'Mozambique',
			'MM' => 'Myanmar',
			'NA' => 'Namibia',
			'NR' => 'Nauru',
			'NP' => 'Nepal',
			'NL' => 'Netherlands',
			'NC' => 'New Caledonia',
			'NZ' => 'New Zealand',
			'NI' => 'Nicaragua',
			'NE' => 'Niger',
			'NG' => 'Nigeria',
			'NU' => 'Niue',
			'NF' => 'Norfolk Island',
			'MP' => 'Northern Mariana Islands',
			'KP' => 'North Korea',
			'NO' => 'Norway',
			'OM' => 'Oman',
			'PK' => 'Pakistan',
			'PS' => 'Palestinian Territory',
			'PA' => 'Panama',
			'PG' => 'Papua New Guinea',
			'PY' => 'Paraguay',
			'PE' => 'Peru',
			'PH' => 'Philippines',
			'PN' => 'Pitcairn',
			'PL' => 'Poland',
			'PT' => 'Portugal',
			'PR' => 'Puerto Rico',
			'QA' => 'Qatar',
			'RE' => 'Reunion',
			'RO' => 'Romania',
			'RU' => 'Russia',
			'RW' => 'Rwanda',
			'BL' => 'Saint Barth&eacute;lemy',
			'SH' => 'Saint Helena',
			'KN' => 'Saint Kitts and Nevis',
			'LC' => 'Saint Lucia',
			'MF' => 'Saint Martin (French part)',
			'SX' => 'Saint Martin (Dutch part)',
			'PM' => 'Saint Pierre and Miquelon',
			'VC' => 'Saint Vincent and the Grenadines',
			'SM' => 'San Marino',
			'ST' => 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe',
			'SA' => 'Saudi Arabia',
			'SN' => 'Senegal',
			'RS' => 'Serbia',
			'SC' => 'Seychelles',
			'SL' => 'Sierra Leone',
			'SG' => 'Singapore',
			'SK' => 'Slovakia',
			'SI' => 'Slovenia',
			'SB' => 'Solomon Islands',
			'SO' => 'Somalia',
			'ZA' => 'South Africa',
			'GS' => 'South Georgia/Sandwich Islands',
			'KR' => 'South Korea',
			'SS' => 'South Sudan',
			'ES' => 'Spain',
			'LK' => 'Sri Lanka',
			'SD' => 'Sudan',
			'SR' => 'Suriname',
			'SJ' => 'Svalbard and Jan Mayen',
			'SZ' => 'Swaziland',
			'SE' => 'Sweden',
			'CH' => 'Switzerland',
			'SY' => 'Syria',
			'TW' => 'Taiwan',
			'TJ' => 'Tajikistan',
			'TZ' => 'Tanzania',
			'TH' => 'Thailand',
			'TL' => 'Timor-Leste',
			'TG' => 'Togo',
			'TK' => 'Tokelau',
			'TO' => 'Tonga',
			'TT' => 'Trinidad and Tobago',
			'TN' => 'Tunisia',
			'TR' => 'Turkey',
			'TM' => 'Turkmenistan',
			'TC' => 'Turks and Caicos Islands',
			'TV' => 'Tuvalu',
			'UG' => 'Uganda',
			'UA' => 'Ukraine',
			'AE' => 'United Arab Emirates',
			'GB' => 'United Kingdom (UK)',
			'US' => 'United States (US)',
			'UM' => 'United States (US) Minor Outlying Islands',
			'VI' => 'United States (US) Virgin Islands',
			'UY' => 'Uruguay',
			'UZ' => 'Uzbekistan',
			'VU' => 'Vanuatu',
			'VA' => 'Vatican',
			'VE' => 'Venezuela',
			'VN' => 'Vietnam',
			'WF' => 'Wallis and Futuna',
			'EH' => 'Western Sahara',
			'WS' => 'Samoa',
			'YE' => 'Yemen',
			'ZM' => 'Zambia',
			'ZW' => 'Zimbabwe',
		);
	}
endif;
