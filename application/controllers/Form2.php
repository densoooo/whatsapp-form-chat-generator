<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Form class.
 * 
 * @extends CI_Controller
 */
class Form2 extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('form_model');
		
	}
	
	
	public function index() {
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->view('header');
        $this->load->view('form/form');
        $this->load->view('footer');
    }
    
    public function test() {
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->view('header');
        $this->load->view('form/form2');
        $this->load->view('footer');
	}
	
	public function test2() {
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->view('header');
        $this->load->view('form/form3');
        $this->load->view('footer');
	}

	public function saveForm2() {
		$user = $this->input->post('user');
		$form_code = $this->input->post('form_code');

		if($this->form_model->save_form($user, $form_code)) {
			echo "sukses";
		} else {
			echo "gagal";
		}
	}

	public function loadform2($id) {
		if($id != null) {

			$code = $this->form_model->load_form($id);
			if($code != null){

				$this->load->helper('form');
				$this->load->library('form_validation');

				$data = array('form_code' => $code);

				$this->load->view('header');
				$this->load->view('form/form4', $data);
				$this->load->view('footer');
			}
		}
	}

	public function send_message(){
		foreach ($_POST as $key => $value) {
			echo "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";
		}
	}
}
