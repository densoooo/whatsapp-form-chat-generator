<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Copyright to Mbahcoding.com
	Source : http://mbahcoding.com/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html
*/

class Datatable_model extends CI_Model {
	public $_read_db;

	public function __construct(){
		$this->_read_db     = $this->load->database('read', TRUE);
	}

	private function dt_query($options){
		$i = 0;

		if( ! empty($options['table_name']) ):
			$this->_read_db->from($options['table_name']);
		endif;

		if( ! empty($options['select']) ):
			$this->_read_db->select($options['select']);
		endif;

		if( ! empty($options['where']) ):
			$this->_read_db->where($options['where']);
		endif;

		if( ! empty($options['or_where']) ):
			$this->_read_db->or_where($options['or_where']['field'], $options['or_where']['value']);
		endif;

		if( ! empty($options['where_in']) ):
			$this->_read_db->where_in($options['where_in']['field'], $options['where_in']['value']);
		endif;

		if( ! empty($options['or_where_in']) ):
			$this->_read_db->or_where_in($options['or_where_in']['field'], $options['or_where_in']['value']);
		endif;

		if( ! empty($options['where_not_in']) ):
			$this->_read_db->where_not_in($options['where_not_in']['field'], $options['where_not_in']['value']);
		endif;

		if( ! empty($options['or_where_not_in']) ):
			$this->_read_db->or_where_not_in($options['or_where_not_in']['field'], $options['or_where_not_in']['value']);
		endif;

		if ( ! empty($options['like']) ):
			$this->_read_db->like($options['like']);
		endif;

		if( ! empty($options['join']) ):
			foreach ($options['join'] as $key => $value):
				$this->_read_db->join($key, $value);
			endforeach;
		endif;

		if(!empty($options['left_join'])){
            foreach($options['left_join'] as $key => $value){
                $this->_read_db->join($key, $value, 'left');
            }
		}
		if( ! empty($options['group_by']) ):
            $this->_read_db->group_by( $options['group_by'] );
        endif;

		foreach ($options['search'] as $item) :
			if($_POST['search']['value']):
				if($i===0):
					$this->_read_db->group_start();
					$this->_read_db->like($item, $_POST['search']['value']);
				else:
					$this->_read_db->or_like($item, $_POST['search']['value']);
				endif;

				if(count($options['search']) - 1 == $i) :
					$this->_read_db->group_end();
				endif;
			endif;

			$i++;
		endforeach;

		if(isset($_POST['order'])):
			$this->_read_db->order_by($options['order_col'][$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		elseif( ! empty($options['order']) ):
			$order = $options['order'];
			$this->_read_db->order_by(key($order), $order[key($order)]);
		endif;
	}

	public function dt_get($options){
		$this->dt_query($options);

		if($_POST['length'] != -1):
			$this->_read_db->limit($_POST['length'], $_POST['start']);
		endif;

		$query = $this->_read_db->get();

		return $query->result();
	}

	public function dt_filtered($options){
		$this->dt_query($options);

		$query = $this->_read_db->get();
		return $query->num_rows();
	}

	public function dt_all($options){

		if( ! empty($options['table_name']) ):
			$this->_read_db->from($options['table_name']);
		endif;

		if( ! empty($options['select']) ):
			$this->_read_db->select($options['select']);
		endif;

		if( ! empty($options['where']) ):
			$this->_read_db->where($options['where']);
		endif;

		if( ! empty($options['where_in']) ):
			$this->_read_db->where_in($options['where_in']['field'], $options['where_in']['value']);
		endif;

		if ( ! empty($options['like']) ):
			$this->_read_db->like($options['like']);
		endif;

		if(!empty($options['left_join'])){
            foreach($options['left_join'] as $key => $value){
                $this->_read_db->join($key, $value, 'left');
            }
		}

		if( ! empty($options['join']) ):
			foreach ($options['join'] as $key => $value):
				$this->_read_db->join($key, $value);
			endforeach;
		endif;


		return $this->_read_db->count_all_results();
	}

}
