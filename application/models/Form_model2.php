<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Form_model class.
 * 
 * @extends CI_Model
 */
class Form_model2 extends CI_Model {
    /**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
    }
    
    public function save_formw($user, $form_code){
        $data = array(
			'user'   => $user,
			'form_code' => $form_code,
		);
		
		return $this->db->insert('test', $data);
    }

    public function load_form2($id){
        $this->db->select('form_code');
		$this->db->from('test');
		$this->db->where('id', $id);

        return $this->db->get()->row('form_code');
    }
}