<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Model extends CI_Model {

    public $_read_db, $_write_db;
    private $_table         = '';
    private $_debugQuery    = false;
    private $form_sessions;

	function __construct()
	{
	    parent::__construct();

        $this->_read_db     = $this->load->database('read', TRUE);
        $this->_write_db    = $this->load->database('write', TRUE);

        $this->form_sessions  = allUserdata();
    }

	function __destruct(){
		// closing database connection
		$this->_read_db->close();
		$this->_write_db->close();
    }
    
    /**
     * Get data from database with filter
     *
     * @param   array  $options
     * @return  object
     */
    public function get_data($options = array()){
        if ( ! empty($options['select']) ){
			$this->_read_db->select($options['select']);
        }
        
        if ( ! empty($options['select_sum']) ){
			$this->_read_db->select_sum($options['select_sum']);
        }

		if ( ! empty($options['where']) ){
			$this->_read_db->where($options['where']);
		}

		if ( ! empty($options['or_where']) ){
			$this->_read_db->or_where($options['or_where']);
		}

		if( ! empty($options['where_in']) ):
			$this->_read_db->where_in($options['where_in']['field'], $options['where_in']['value']);
		endif;

		if( ! empty($options['or_where_in']) ):
			$this->_read_db->or_where_in($options['or_where_in']['field'], $options['or_where_in']['value']);
		endif;

		if( ! empty($options['where_not_in']) ):
			$this->_read_db->where_not_in($options['where_not_in']['field'], $options['where_not_in']['value']);
		endif;

		if( ! empty($options['or_where_not_in']) ):
			$this->_read_db->or_where_not_in($options['or_where_not_in']['field'], $options['or_where_not_in']['value']);
		endif;

		if ( ! empty($options['like']) ){
			$this->_read_db->like($options['like']);
		}

		if ( ! empty($options['order']) ){
			$this->_read_db->order_by($options['order']['key'], $options['order']['sort']);
		}

		if ( ! empty($options['join']) ){
			foreach($options['join'] as $key => $value){
				$this->_read_db->join($key, $value);
			}
        }
        
        if ( ! empty($options['left_join']) ){
			foreach($options['left_join'] as $key => $value){
				$this->_read_db->join($key, $value, 'left');
			}
		}

		if ( ! empty($options['limit']) ){
			if ( ! empty($options['offset']) ){
				$this->_read_db->limit($options['limit'], $options['offset']);
			} else {
				$this->_read_db->limit($options['limit']);
			}
		}

		if(!empty($options['from'])){
			$this->_read_db->from($options['from']);
        }

        if( ! empty($options['group_by']) ):
            $this->_read_db->group_by( $options['group_by'] );
        endif;
        
        $data = $this->read();

        if ( ! empty($options['count']) ){
			return $data->num_rows();
		} elseif( ! empty($options['single']) ){
			return $data->row();
		} elseif( ! empty($options['array']) ){
			return $data->result_array();
		} else {
			return $data->result();
		}
    }

    /**
     * Insert data into database
     *
     * @param   array  $params
     * @return  bool
     */
	function write($params) {
        // $params = array_merge($params, array(
        //     "wholeseller_id"    => $this->bu_sessions['wid'], 
        //     "user_id"           => $this->bu_sessions['uid']
        // ));

        $this->_write_db->insert($this->_table, $params);
        $this->showDebug(0);

        if ($this->_write_db->affected_rows() == 1){
    		return $this->_write_db->insert_id();
        }

        return FALSE;

	}

	/**
     * Insert batch data into database
     *
     * @param   array  $params
     * @return  bool
     */
	function batch_write($params){
		$this->_write_db->insert_batch($this->_table, $params);
		$this->showDebug(0);

		if ($this->_write_db->affected_rows() > 0){
			return $this->_write_db->insert_id();
		}

		return FALSE;
	}

    /**
     * Get data from database
     *
     * @return  object
     */
	function read() {
        $result = $this->_read_db->get($this->_table);
        $this->showDebug(1);

        return $result;
	}

    /**
     * Update data into database
     *
     * @param   array  $params
     * @return  int
     */
	function update($params) {
         $this->_write_db->update($this->_table,$params);
         $this->showDebug(0);

         return $this->_write_db->affected_rows() > 0;
	}

	/**
	* Batch update data into database
	*
	* @param   array  $params
	* @return  int
	*/
	function batch_update($params, $where) {
		$this->_write_db->update_batch($this->_table, $params, $where);
		$this->showDebug(0);

		return $this->_write_db->affected_rows() > 0;
	}

    /**
     * Delete data from database
     *
     * @param   array  $params
     * @return  int
     */
	function delete($params) {
        $this->_write_db->delete($this->_table,$params);
        $this->showDebug(0);

        return $this->_write_db->affected_rows() > 0;
	}

    /**
     * Set table name
     *
     * @param   string  $current_table
     */
    function setTable($current_table) {
        $this->_table = $current_table;
	}

    /**
     * Get table
     *
     * @return  $this
     */
    function getTable() {
        return $this->_table;
	}

    /**
     * Set to show debug or not
     *
     * @param   int  $read
     * @return  bool
     */
    public function showDebug($read) {
        if ($this->_debugQuery){

            if ($read){ //read for query read
                print_r($this->_read_db->last_query());
            } else {
                print_r($this->_write_db->last_query());
            }

       }
    }

    /**
     * Set debugging mode
     *
     * @param   $int  $mode
     */
    function setDebug($mode) {
	    //set debuging mode
        if ($mode){
            $this->_debugQuery = true;
        } else {
            $this->_debugQuery = false;
        }
	}

	public function manualQuery($query){
		return $this->_read_db->query($query)->result();
	}

}
