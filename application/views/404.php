<style>
html, body {
    height: 100%;
    margin: 0;
    padding: 0;
}

img {
    padding: 0;
    display: block;
    margin: 0 auto;
    object-fit: cover;
    height: 100%;
    width: 100%;
}
</style>

<img src="<?php echo base_url('assets/img/404.jpg') ?>">