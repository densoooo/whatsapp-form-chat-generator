<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('/assets/img/apple-icon.png')?>">
  <link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/favicon.png')?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Whatsapp Form Chat
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url('/assets/css/material-dashboard.min.css?v=2.1.1')?>" rel="stylesheet" />
  <link href="<?php echo base_url('/assets/css/flax.css')?>" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/themes/dark.css">
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('/assets/img/sidebar-1.jpg')?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item dashboard">
            <a class="nav-link" href="<?php echo base_url('dashboard')?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
        </ul>
          <?php if($this->session->userdata('role_id') < 3) { ?>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link">
              <p>User</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item user-userlist">
            <a class="nav-link" href="<?php echo base_url('user/userlist')?>">
              <i class="material-icons">person</i>
              <p>List User</p>
            </a>
          </li>
          <li class="nav-item user-createuser">
            <a class="nav-link" href="<?php echo base_url('user/createuser')?>">
              <i class="material-icons">person_add</i>
              <p>Create User</p>
            </a>
          </li>
        </ul>
        <?php } ?>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link">
              <p>Campaign</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item campaign">
            <a class="nav-link" href="<?php echo base_url('campaign')?>">
              <i class="material-icons">insert_drive_file</i>
              <p>List Campaign</p>
            </a>
          </li>
          <li class="nav-item campaign-create">
            <a class="nav-link" href="<?php echo base_url('campaign/create')?>">
              <i class="material-icons">note_add</i>
              <p>Create New Campaign</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link">
              <p>WA Form Builder</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item form-create">
            <a class="nav-link" href="<?php echo base_url('form/create')?>">
              <i class="material-icons">assignment_returned</i>
              <p>Create Form</p>
            </a>
          </li>
          <li class="nav-item form">
            <a class="nav-link" href="<?php echo base_url('form')?>">
              <i class="material-icons">assignment</i>
              <p>List Form</p>
            </a>
          </li>
          <li class="nav-item response-responselist">
            <a class="nav-link" href="<?php echo base_url('response/responselist')?>">
              <i class="material-icons">assignment_turned_in</i>
              <p>Response List</p>
            </a>
          </li>
          <li class="nav-item form-report">
            <a class="nav-link" href="<?php echo base_url('form/report')?>">
              <i class="material-icons">assessment</i>
              <p>Form Report</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item group">
            <a class="nav-link">
              <p>A|B Testing</p>
            </a>
          </li>
        </ul>
        <ul class="nav">
          <li class="nav-item abtesting-create">
            <a class="nav-link" href="<?php echo base_url('abtesting/create')?>">
              <i class="material-icons">library_add</i>
              <p>Create A|B Testing</p>
            </a>
          </li>
          <li class="nav-item abtesting">
            <a class="nav-link" href="<?php echo base_url('abtesting')?>">
              <i class="material-icons">library_books</i>
              <p>List A|B Testing</p>
            </a>
          </li>
        </ul>
          </li>
          <?php if($this->session->userdata('role_id') == 1) { ?>
        <ul class="nav">
          <li class="nav-item group">
            <a class="nav-link" href="<?php echo base_url('group')?>">
              <i class="material-icons">people</i>
              <p>Group List</p>
            </a>
          </li>
        </ul>
          <?php } ?>
      </div>
    </div>