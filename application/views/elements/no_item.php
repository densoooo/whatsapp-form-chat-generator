<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans);
body{
    color:#fff;
    font-family: 'Open Sans', sans-serif;
    max-height:700px;
    overflow: hidden;
}
.c{
    text-align: center;
    display: block;
    position: relative;
    width:80%;
    margin:250px auto;
}
._404{
    font-size: 220px;
    position: relative;
    display: inline-block;
    color: #555;
    z-index: 2;
    letter-spacing: 15px;
}
._1{
    text-align:center;
    display:block;
    position:relative;
    color: #555;
    letter-spacing: 12px;
    font-size: 4em;
    line-height: 80%;
}
._2{
    text-align:center;
    display:block;
    position: relative;
    color: #555;
    font-size: 20px;
}
.text{
    font-size: 70px;
    text-align: center;
    position: relative;
    display: inline-block;
    margin: 19px 0px 0px 0px;
    /* top: 256.301px; */
    z-index: 3;
    width: 100%;
    line-height: 1.2em;
    display: inline-block;
}


.btn{
    background-color: rgb( 255, 255, 255 );
    position: relative;
    display: inline-block;
    width: 358px;
    padding: 5px;
    z-index: 5;
    font-size: 25px;
    margin:0 auto;
    color:#555;
    text-decoration: none;
    margin-top: 30px
}
.right{
    float:right;
    width:60%;
}

hr{
    padding: 0;
    border: none;
    border-top: 5px solid #555;
    color: #555;
    text-align: center;
    margin: 0px auto;
    height:10px;
    width:729px;
    z-index: -10;
}
</style>

<div class='c'>
    <div class='_404'>OOPS!</div>
    <hr>
    <div class='_1'><?php echo $_1; ?></div>
    <div class='_2'>WAS FOUND</div>
    <a class='btn' href='<?php echo $url; ?>'><?php echo $button; ?></a>
</div>