<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Whatsapp Chat</h1>
			</div>
			<?= form_open() ?>
				<div class="form-group">
					<label for="nama">Nama</label>
					<input required type="text" class="form-control" id="nama" name="nama" placeholder="Nama Anda">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input required type="email" class="form-control" id="email" name="email" placeholder="Email Anda">
				</div>
                <div class="form-group">
					<label for="nomorTelpon">Nomor Telpon</label>
					<input required type="number" class="form-control no-spinners" id="nomorTelpon" name="nomorTelpon" placeholder="Nomor Telpon Anda">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Chat Via Whatsapp">
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<style>
	/* hide up/down arrows ("spinners") on input fields marked type=number */
	.no-spinners [type='number'] {
	-moz-appearance:textfield;
	}

	.no-spinners::-webkit-outer-spin-button,
	.no-spinners::-webkit-inner-spin-button {
	-webkit-appearance: none;
	margin: 0;
	}

	.edit-input {
    display:none;
	}
</style>