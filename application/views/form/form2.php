<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php foreach ($_POST as $key => $value) {
    echo "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";
} ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Whatsapp Chat</h1>
			</div>
			<form action="<?php echo base_url('form/send_message');?>" method="post" accept-charset="utf-8">
				<div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" class="edit-input" />
					<a class="edit" href="javascript:void(0);">Edit</a>
					<input required type="text" class="form-control" id="nama" name="nama" placeholder="Nama Anda">
					<a class="remove_field" href="javascript:void(0);">Remove</a>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="edit-input" />
					<a class="edit" href="javascript:void(0);">Edit</a>
					<input required type="email" class="form-control" id="email" name="email" placeholder="Email Anda">
					<a class="remove_field" href="javascript:void(0);">Remove</a>
				</div>
                <div class="form-group">
					<label for="nomorTelpon">Nomor Telpon</label>
					<input type="text" class="edit-input" />
					<a class="edit" href="javascript:void(0);">Edit</a>
					<input required type="text" class="form-control no-spinners" id="nomorTelpon" name="nomorTelpon" placeholder="Nomor Telpon Anda">
					<a class="remove_field" href="javascript:void(0);">Remove</a>
				</div>
				<div class="end-group"></div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Chat Via Whatsapp">
					<a class="edit-button-toggle" href="javascript:void(0);">Edit</a>
					<div class="button-editor">
						<label for="edit-button" class="edit-button">Button Label</label>
						<input type="text" class="edit-button form-control" />
						<label for="edit-number" class="edit-number">Nomor WA Anda</label>
						<input type="number" class="edit-number form-control" />
						<p class="edit-number">Gunakan awalan 62 untuk nomor Whatsapp anda, contoh : 6285711188899</p>
						<button type="button" class="save-button">Save Button</button>
					</div>
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<button type="button" class="add_field">Tambah Field</button>
<button type="button" class="save">Save</button>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
  $(".container").on("click",".edit", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('label'); 
    lbl.hide();
    dad.find('.edit-input').val(lbl.text()).show().focus();
  });
    
  $(".container").on('focusout', '.edit-input', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
	  dad.find('.form-control').prop({id: label, name: label, placeholder: this.value + ' Anda'})
	  dad.find('label').prop('for', label);
      dad.find('label').text(this.value).show();
  });

  $(".container").on("click",".edit-button-toggle", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
	var btn = dad.find('.btn')
    dad.find('.edit-button').val(btn.val()).show().focus();
	dad.find('.edit-number').show();
	dad.find('.save-button').show();
  });
    
//   $(".container").on('focusout', '.edit-button', function(e) {
//       var dad = $(this).parent();
//       $(this).hide();
// 	  dad.find('.editor').hide();
// 	  dad.find('.edit-button').hide();
// 	  dad.parent().find('.btn').val(this.value);
//   });

  $(".container").on("click",".remove_field", function(e){
  	$(this).parent('div').remove();
  });
  
  $('button.add_field').on("click",function(e){
  	e.preventDefault();
    $(
		`<div class="form-group">
					<label for="label">Label</label>
					<input type="text" class="edit-input" />
					<a class="edit" href="javascript:void(0);">Edit</a>
					<input required type="text" class="form-control" id="label" name="label" placeholder="Label Anda">
					<a class="remove_field" href="javascript:void(0);">Remove</a>
				</div>`
		).insertBefore('.container .end-group'); //add input box
  });

  $('button.save').on("click", function(e){
	// e.preventDefault();
	var str = $("<div/>").append($('.container').clone());
	str.find('a').remove();
	var html = str.html();
	// console.log(html);
	$.post(
		'../form/saveForm',{
			user: "joko",
			form_code: html
		}
	)
  });

  $('button.save-button').on('click', function(e){
	e.preventDefault();
	var dad = $(this).parent();
	var btnLabel = dad.find('input.edit-button').val();
	var phoneNumber = dad.find('input.edit-number').val();
	$(this).hide();
	dad.find('.edit-button').hide();
	dad.find('.edit-number').hide();
	dad.parent().find('.btn').val(btnLabel);
	// dad.parent().parent().find('#nomorTelponSelf').val(phoneNumber);
	$(
		`<div class="form-group">
					<input required type="text" style="display:none;" class="form-control" id="nomorTelponSelf" name="nomorTelponSelf" value="`+phoneNumber+`">
				</div>
		
		`
	).insertBefore('.container .end-group');
  })
});
</script>

<style>

.edit-input {
    display:none;
}

.edit-button {
    display:none;
}

.edit-number {
	display:none;
}

.save-button {
	display:none;
}
</style>