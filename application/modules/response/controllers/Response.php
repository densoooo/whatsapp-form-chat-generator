<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Response extends MX_Controller {

	public function __construct(){

        $this->load->model('response/response_model', 'response');
        $this->load->model('response/followup_model', 'followup');
        $this->load->model('form/form_model', 'form');
    }

    function responselist(){
        if($this->session->userdata('nama')){
            $data['recent_followup'] = $this->followup->recent_followup();
            $data['status_count'] = $this->response->count_status();
            $data['pricing_count'] = $this->response->count_pricing();

			$this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('response/response_list', $data);
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function responsedata(){
        if($this->session->userdata('nama')){
            
            $form_id = ($this->input->get('form') !== null ? $this->input->get('form') : null);
            $status = ($this->input->get('status') !== null ? $this->input->get('status') : null);
            // $form_name = ($this->input->get('form_name') !== null ? $this->input->get('form_name') : null);
            
            if($this->input->get('form_name')) {
                $form_name = $this->input->get('form_name');
                $list_id = $this->form->get_list_id($form_name);
            } else {
                $form_name = null;
            }

            if($form_id){
                $dataResponse = $this->response->get_all_response('response_data.form_id',$form_id);
            } else if($status !== null){
                $dataResponse = $this->response->get_all_response('response_data.status_followup',$status);
            } else if ($form_name){
                $dataResponse = $this->response->get_all_response('response_data.form_id',$list_id);
            } else {
                $dataResponse = $this->response->get_all_response();
            }

			// if($form_id && $status !== null && $form_name){
            //     $dataResponse = $this->response->get_all_response(array('response_data.form_id'=>$form_id, 'response_data.status_followup'=>$status));
            // } else if($form_id && $form_name){
            //     $dataResponse = $this->response->get_all_response(array('response_data.form_id'=>$form_id));
            // } else if($status !== null && $form_name){
            //     $dataResponse = $this->response->get_all_response(array('response_data.status_followup'=>$status));
            // } else if($form_id){
            //     $dataResponse = $this->response->get_all_response(array('response_data.form_id'=>$form_id));
            // } else if($status !== null){
            //     $dataResponse = $this->response->get_all_response(array('response_data.status_followup'=>$status));
            // } else if ($form_name){
            //     $dataResponse = $this->response->get_all_response(array('response_data.status_followup'=>$status));
            // } else {
            //     $dataResponse = $this->response->get_all_response();
            // }

            if($dataResponse){
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($dataResponse),
                    "recordsFiltered" => sizeof($dataResponse),
                    "data" => $dataResponse);
            } else {
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => []);
            }
            echo json_encode($output);
		} else {
			redirect(base_url());
		}
    }

    public function insertfollowup(){
        if($this->session->userdata('nama')){
			$this->form_validation->set_rules('follupDesc', 'follupDesc', 'required');
            $this->form_validation->set_rules('follupResult', 'follupResult', 'required');
            $this->form_validation->set_rules('follupStatus', 'follupStatus', 'required');

            $data = [
                'response_id'           => $this->input->post('response_id', TRUE),
                'form_id'               => $this->input->post('form_id', TRUE),
                'followup_description'  => $this->input->post('follupDesc', TRUE),
                'created_date'          => date('Y-m-d H:i:s'),
                'created_by'            => $this->session->userdata('nama'),
                'next_followup_date'    => $this->input->post('nextDate', TRUE),
                'followup_status'       => 1,
                'status'                => 1
            ];

            // if($this->input->post('follupStatus', TRUE) == 1){
            //     $data = [
            //         'response_id'           => $this->input->post('response_id', TRUE),
            //         'form_id'               => $this->input->post('form_id', TRUE),
            //         'followup_date'         => date('Y-m-d H:i:s'),
            //         'followup_description'  => $this->input->post('follupDesc', TRUE),
            //         'created_date'          => date('Y-m-d H:i:s'),
            //         'created_by'            => $this->session->userdata('nama'),
            //         'next_followup_date'    => $this->input->post('nextDate', TRUE),
            //     ];
            // } else {
            //     $data = [
            //         'response_id'           => $this->input->post('response_id', TRUE),
            //         'form_id'               => $this->input->post('form_id', TRUE),
            //         'followup_date'         => date('Y-m-d H:i:s'),
            //         'followup_description'  => $this->input->post('follupDesc', TRUE),
            //         'followup_result'       => $this->input->post('follupResult', TRUE),
            //         'created_date'          => date('Y-m-d H:i:s'),
            //         'created_by'            => $this->session->userdata('nama'),
            //         'closing_date'          => date('Y-m-d H:i:s'),
            //         'followup_status'       => $this->input->post('follupStatus', TRUE),
            //         'status'                => $this->input->post('follupStatus', TRUE)
            //     ];
            // }

            $input = $this->followup->insert_followup($data);

            if($input){
                $param = [ 'status_followup' => 1 ];
                $this->response->update_status($data['response_id'], $param);

                echo "sukses";
            }
		} else {
			redirect(base_url());
		}
    }

    public function updatefollowup(){
        if($this->session->userdata('nama')){
            $response_id = $this->input->post('response_id', TRUE);
            $followup_id = $this->input->post('followup_id', TRUE);

            if($this->input->post('followupStatus', TRUE) == 1){
                $data = [
                    'followup_result'       => $this->input->post('followupResult', TRUE),
                    'followup_date'         => date('Y-m-d H:i:s'),
                    'followup_status'       => $this->input->post('followupStatus', TRUE),
                ];
            } else {
                $data = [
                    'followup_result'       => $this->input->post('followupResult', TRUE),
                    'followup_date'         => date('Y-m-d H:i:s'),
                    'followup_status'       => $this->input->post('followupStatus', TRUE),
                    'closing_date'          => date('Y-m-d H:i:s'),
                ];
            }

            $update = $this->followup->update_followup($followup_id, $data);
            if($update){
                $param = [ 'status_followup' => $data['followup_status'] ];
                $this->response->update_status($response_id, $param);
                echo "sukses";
            } else {
                echo "gagal";
            }
        } else {
			redirect(base_url());
		} 
    }

    public function followuphistory($param){
        if($this->session->userdata('nama')){
			if($param){
                $dataHistory = $this->followup->followup_history($param);
                if($dataHistory){
                    $output = array(
                        "draw" => $this->input->post('draw', TRUE),
                        "recordsTotal" => sizeof($dataHistory),
                        "recordsFiltered" => sizeof($dataHistory),
                        "data" => $dataHistory);
                } else {
                    $output = array(
                        "draw" => $this->input->post('draw', TRUE),
                        "recordsTotal" => 0,
                        "recordsFiltered" => 0,
                        "data" => []);
                }
                echo json_encode($output);
            }
		} else {
			redirect(base_url());
		}
    }

    public function response_detail($param){
        if($this->session->userdata('nama')){
            $data = $this->response->get_detail($param);

            echo json_encode($data);
        } else {
			redirect(base_url());
		}
    }
}