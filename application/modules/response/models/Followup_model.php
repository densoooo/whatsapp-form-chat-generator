<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Followup_model extends Base_Model {

    function __construct() {
        parent::__construct();

        $this->setTable('history_followup');
    }

    public function insert_followup($params){
    
        $this->write($params);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
      
    public function followup_history($param){
        $this->_read_db->select('followup_id');
        $this->_read_db->select('followup_date');
        $this->_read_db->select('followup_description');
        $this->_read_db->select('followup_result');
        $this->_read_db->select('created_date');
        $this->_read_db->select('created_by');
        $this->_read_db->select('next_followup_date');
        $this->_read_db->select('closing_date');
        $this->_read_db->select('followup_status');
        $this->_read_db->where('response_id', $param);

        $history = $this->read()->result();

        if(!empty($history)){
            foreach($history as $r) {
                ($r->followup_status == 0) ? $status = "0" : ($r->followup_status == 1) ? $status = "Next Followup" : $status ="Closing"; 
                $data[] = array(
                     $r->followup_id,
                     $r->followup_date,
                     $r->followup_description,
                     $r->followup_result,
                     $r->created_date,
                     $r->created_by,
                     $r->next_followup_date,
                     $r->closing_date,
                     $status,
                );
            }
        } else {
            $data = null;
        }

        return $data;
    }

    public function update_followup($id, $params){
		$this->_write_db->where('followup_id', $id);

		return $this->update($params);
    }
    
    public function recent_followup(){
        $user = $this->session->userdata('nama');

        $this->_read_db->where('created_by', $user);
        $this->_read_db->where('followup_status', 1);
        $this->_read_db->where('followup_result', null);
        $this->_read_db->order_by('next_followup_date', 'desc');
        $this->_read_db->limit(5);

        $history = $this->read()->result();

        return $history;
    }

}