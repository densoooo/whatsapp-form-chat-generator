<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Response_model extends Base_Model {

	function __construct() {
    parent::__construct();

    $this->setTable('response_data');
  }

  /**
   * Get all responses data from database with filter
   *
   * @param   array  $options
   * @return  object
   */
  public function get_all_response($field = FALSE, $options = FALSE) {
    // $response = $this->get_data($options);
    
    $user_id = $this->session->userdata('uid');

    $this->_read_db->select("response_data.response_id");
    $this->_read_db->select("response_data.form_id");
    $this->_read_db->select("response_data.nama");
    $this->_read_db->select("response_data.nomor_telpon");
    $this->_read_db->select("response_data.response_submited_date");
    $this->_read_db->select("response_data.cs_number");
    $this->_read_db->select("response_data.status_followup");
    $this->_read_db->join("form_data", "form_data.form_id = response_data.form_id");
    $this->_read_db->where("form_data.user_id", $user_id);
    // $this->_read_db->where_in($field, $options);
    ($options != FALSE ? $this->_read_db->where_in($field, $options) : null);
    
    $response = $this->read()->result();

    if(!empty($response)){
      foreach($response as $r) {
        // ($r->status_followup == 0 ? $status = "Followup" : ($r->status_followup == 1 ? $status = "Next Followup" : $status ="Closing")); 
        switch($r->status_followup){
          case 0 :
            $status = "Followup";
            break;
          case 1 :
            $status = "Next Followup";
            break;
          case 2 :
            $status = "Closing";
            break;
          case 3 :
            $status = "Reject";
            break;
          case 4 :
            $status = "Not Respond";
            break;
        }
        // ($r->status_followup == 0) ? $status = "Followup" : ($r->status_followup == 1) ? $status = "Next Followup" : $status ="Closing"; 
        // if($r->status_followup == 0) {$status = "Followup";} else if($status = "Followup" == 1) {$status = "Next Followup";} else if($status = "Followup" == 2){$status ="Closing";};
        $data[] = array(
            $r->response_id,
            $r->nama,
            $r->nomor_telpon,
            $r->cs_number,
            $r->response_submited_date,
            $status.' <a class="button btn btn-light btn-sm" style="float:right;" value="'. $r->response_id .'" form-id="'. $r->form_id .'">Detail</a>',
          );
      }
    } else {
      $data = null;
    }

    return $data;
  }

  public function save_response($data){
    return $this->write($data);
  }

  public function update_status($id, $param){
    $this->_write_db->where('response_id', $id);

    return $this->update($param);
  }

  public function get_response_data(){
    $this->setTable('form_data');

    $user_id = $this->session->userdata('uid');

    $this->_read_db->select("form_data.form_id");
    $this->_read_db->select("response_data.status_followup");
    $this->_read_db->join("response_data", "form_data.form_id = response_data.form_id");
    $this->_read_db->where("form_data.user_id", $user_id);

    $data = $this->read()->result();

    $this->setTable('response_data');

    return $data;
  }

  public function get_detail($param){
    $user_id = $this->session->userdata('uid');

    $this->_read_db->select("response_data.response_id");
    $this->_read_db->select("response_data.nama");
    $this->_read_db->select("response_data.nomor_telpon");
    $this->_read_db->select("response_data.response_submited_date");
    $this->_read_db->select("response_data.cs_number");
    $this->_read_db->select("response_data.additional_info");
    $this->_read_db->select("response_data.status_followup");
    $this->_read_db->join("form_data", "form_data.form_id = response_data.form_id");
    $this->_read_db->where("form_data.user_id", $user_id);
    $this->_read_db->where('response_data.response_id', $param);
    
    $response = $this->read()->row();

    return $response;
  }

  public function count_status(){
    $user_id = $this->session->userdata('uid');

    $this->_read_db->select("response_data.status_followup");
    $this->_read_db->join("form_data", "form_data.form_id = response_data.form_id");
    $this->_read_db->where("form_data.user_id", $user_id);

    $response = $this->read()->result();
    $response_data = array('followup'=>0, 'next_followup'=>0, 'closing'=>0, 'reject'=>0, 'not_respond'=>0);

    if(!empty($response)){
      foreach($response as $r) {
        switch($r->status_followup){
          case 0 :
            $response_data['followup'] += 1;
            break;
          case 1 :
            $response_data['next_followup'] += 1;
            break;
          case 2 :
            $response_data['closing'] += 1;
            break;
          case 3 :
            $response_data['reject'] += 1;
            break;
          case 4 :
            $response_data['not_respond'] += 1;
            break;
        }
      }
    }

    return $response_data;
  }

  public function count_pricing(){
    $user_id = $this->session->userdata('uid');

    $this->_read_db->select("response_data.status_followup");
    $this->_read_db->select("form_data.form_pricing");
    $this->_read_db->join("form_data", "form_data.form_id = response_data.form_id");
    $this->_read_db->where("form_data.user_id", $user_id);

    $response = $this->read()->result();
    $pricing_data = array('followup'=>0, 'next_followup'=>0, 'closing'=>0, 'reject'=>0, 'not_respond'=>0);

    if(!empty($response)){
      foreach($response as $r) {
        switch($r->status_followup){
          case 0 :
          $pricing_data['followup'] += $r->form_pricing;
            break;
          case 1 :
            $pricing_data['next_followup'] += $r->form_pricing;
            break;
          case 2 :
            $pricing_data['closing'] += $r->form_pricing;
            break;
          case 3 :
            $pricing_data['reject'] += $r->form_pricing;
            break;
          case 4 :
            $pricing_data['not_respond'] += $r->form_pricing;
            break;
        }
      }
    }

    return $pricing_data;
  }

}