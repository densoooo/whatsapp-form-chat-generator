<link href="<?php echo base_url('assets/css/jquery.eeyellow.Timeline.css'); ?>" rel="stylesheet">
<style>
.swal2-container{
    z-index:4000;
}
</style>

<div id="myContent" class="bringins-content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Follow Up Status</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <p>Customer Name</p>
                        </div>
                        <div class="col-md-9">
                            <p id="custName"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p>Customer Phone Number</p>
                        </div>
                        <div class="col-md-9">
                            <p id="custPhoneNumber"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p>CS Phone Number</p>
                        </div>
                        <div class="col-md-9">
                            <p id="CSPhoneNumber"></p>
                        </div>
                    </div>
                    <div class="additional-info">
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p>Followup Status</p>
                        </div>
                        <div class="col-md-9">
                            <p id="followupStatus"></p> 
                        </div>
                    </div>
                    <button class="btn btn-primary btn-add-followup" style="float:right;">Add New Followup</button>
                </div>
            </div>
        </div>
    </div>    
    <div class="row">
        <div class="col-md-12">
            <h3>Follow Up History</h3>
            <div class="VivaTimeline">
                <dl>
                                                       
                </dl>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">List Response</h3>
                    <?php 
                    $text = null;
                    foreach($status_count as $key => $value){
                        $text .= $key." : ".$value." | ";
                    } ?>
                    <p class="card-category"><?php echo $text; ?></p>
                </div>
                <div class="card-body">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Customer</th>
                                <th>Nomor Telpon</th>
                                <th>CS Number</th>
                                <th>Submited Date</th>
                                <th>Status Followup</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nama Customer</th>
                                <th>Nomor Telpon</th>
                                <th>CS Number</th>
                                <th>Submited Date</th>
                                <th>Status Followup</th>
                            </tr>
                        </tfoot>
                    </table>
                    <h3>Total Form Pricing</h3>
                    <div class="row">
                        <?php 
                            foreach($pricing_count as $key => $value){
                                echo "<div class='col-md-3'>$key</div>
                                    <div class='col-md-9'>Rp. ".number_format("$value",2,",",".")."</div>";
                            } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css');?>" type="text/css"/>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  var baseURL = "<?php echo base_url("/"); ?>";
</script>
<script>
    $(document).ready(function() {
        var param = window.location.href.slice(window.location.href.indexOf('?') + 1);
        var response_id;
        var form_id;

        var resposeTable =  $('#example').DataTable({
            destroy: true,
            "ajax": baseURL+"response/responsedata?"+param,
            "columnDefs": [
                {
                    "targets": 0,
                    "visible": false,
                    "searchable": false
                }
            ],
            "order": [[ 0, "asc" ]]
        });

        $('table').on('click', 'a.button', function(){
            response_id = $(this).attr('value');
            form_id = $(this).attr('form-id');

            getTimelineData("init");
        });

        if(param.indexOf('detail') != -1){
            var response_id = param.slice(7);

            getTimelineData("init");
        }

        function getTimelineData(param){
            $.ajax({
                url: baseURL+"response/response_detail/"+response_id,
                dataType: 'json',
                success: function(data){

                    switch(data.status_followup){
                        case "0" :
                            var status = "Followup"
                            break;
                        case "1" :
                            var status = "Next Followup"
                            break;
                        case "2" :
                            var status = "Closing"
                            break;
                        case "3" :
                            var status = "Reject"
                            break;
                        case "4" :
                            var status = "Not Respond"
                            break;   
                    }

                    $('#custName').text(data.nama);
                    $('#custPhoneNumber').text(data.nomor_telpon);
                    $('#CSPhoneNumber').text(data.cs_number);
                    $('#followupStatus').text(status);
                    var additional_info = $.parseJSON(data.additional_info);

                    $.each(additional_info,function(key,value){
                        $('.additional-info').append(
                            `<div class="row">
                                <div class="col-md-3">
                                    <p>`+key+`</p>
                                </div>
                                <div class="col-md-9">
                                    <p id="`+key+`">`+value+`</p>
                                </div>
                            </div>`);
                    });

                    var width = ($('.main-panel').width() / $( window ).width()) * 100;
                    
                    if(param == "init"){
                        $('#myContent').bringins({
                            position: "right",
                            width: width+"%",
                            margin: 50,
                            color: "#eee",
                            closeButton: "rgb(102,102,102)",
                            zIndex: "3000"
                        });
                    }

                    $.ajax({
                        url: baseURL+"response/followuphistory/"+response_id,
                        dataType: 'json',
                        success: function(result){
                            $.each(result.data, function(key,value){
                                var isLastElement = key == result.data.length -1;
                                if(isLastElement && value[8] !== 'Closing'){
                                    var form = 
                                                `
                                                <div class="row">
                                                    <div class="col-md-3" style="margin-bottom:30px;">
                                                        Followup Result
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        <textarea rows="4" class="form-control" type="text" id="followupResult"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3" style="margin-bottom:30px;">
                                                        Followup Status
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        <select required name="follupStatus" id="my-select">
                                                            <option value="" data-placeholder>Select an option</option>
                                                            <option value="1">Next Followup</option>
                                                            <option value="2">Closing</option>
                                                            <option value="3">Reject</option>
                                                            <option value="4">Not Respond</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" style="float:right;">
                                                    <button class="btn btn-primary btn-submit" style="margin-right:30px;margin-bottom:30px;" value="`+value[0]+`">Update Followup</button>
                                                </div>`;
                                                param = 'init';
                                } else { 
                                    var form = 
                                                `
                                                <div class="row">
                                                    <div class="col-md-3 pull-left" style="margin-bottom:30px;">
                                                        Followup Result
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+value[3]+`
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 pull-left" style="margin-bottom:30px;">
                                                        Followup Status
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+value[8]+`
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 pull-left" style="margin-bottom:30px;">
                                                        Next Followup Date
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+value[6]+`
                                                    </div>
                                                </div>
                                                `;
                                    param = 'none';
                                }

                                $('.VivaTimeline dl').append(
                                    `
                                    <dd class="pos-right clearfix">
                                        <div class="circ"></div>
                                        <div class="time">`+value[4]+`</div>
                                        <div class="events">
                                            <div class="events-header">Event Heading<hr></div>                                
                                            <div class="events-body">                                                                                          
                                                <div class="row">
                                                    <div class="col-md-3 pull-left" style="margin-bottom:30px;">
                                                        Customer Name
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+data.nama+`
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 pull-left" style="margin-bottom:30px;">
                                                        Description
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+value[2]+`
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 pull-left">
                                                        Followup By
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom:30px;">
                                                        `+value[5]+`
                                                    </div>
                                                </div>
                                                    `+form+`
                                                </div>
                                            </div>                                
                                        </div>
                                    </dd> 
                                    `
                                )
                            })
                            $('.VivaTimeline').vivaTimeline();
                            if(param == 'init'){
                                easydropdown('#my-select');
                            }
                        }
                    })
                }
            })
        }

        $(document).on('click', '.bringins-close-button', function(){
            setTimeout(function() {
                $('.additional-info').empty();
                $('.VivaTimeline dl').empty();
            }, 1000);
        })

        $(document).on('click', '.btn-submit', function(){
            var result = $('#followupResult').val();
            var status = $('#my-select').val();
            var followup_id = $(this).val();

            $.ajax({
                type: "POST",
                url: baseURL+"response/updatefollowup",
                data : {
                    "response_id" : response_id,
                    "followup_id" : followup_id,
                    "followupResult" : result, 
                    "followupStatus" : $("#my-select").val(),
                },
                success: function(data){
                    if(data == "sukses"){
                        resposeTable.ajax.reload();
                        $('.VivaTimeline dl').empty();
                        getTimelineData();
                        Swal.fire(
                            'Updated!',
                            'Response has been updated.',
                            'success'
                        ).then((result) => {
                        if (result.value) {
                            addfollowup();
                        }
                        })
                    }
                }
            });
        });

        // $('form#followupForm').submit(function(e) {
        //     var form = $(this);
        //     e.preventDefault();

        //     $.ajax({
        //         type: "POST",
        //         url: baseURL+"response/insertfollowup",
        //         data : {
        //             "follupDesc" : $("#follupDesc").val(), 
        //             "follupResult" : $("#follupResult").val(), 
        //             "nextDate" : $("#nextDate").val(), 
        //             "follupStatus" : $("#my-select").val(),
        //             "response_id" : response_id,
        //             "form_id" : form_id,
        //         },
        //         success: function(data){
        //             $('#followupForm').trigger("reset");

        //             $('#test').DataTable({
        //                 destroy: true,
        //                 "ajax": baseURL+"response/followuphistory/"+response_id,
        //                 "columnDefs": [
        //                     {
        //                         "targets": 0,
        //                         "visible": false,
        //                         "searchable": false
        //                     }
        //                 ],
        //                 "order": [[ 0, "asc" ]]
        //             });

        //             $('#example').DataTable({
        //                 destroy: true,
        //                 "ajax": baseURL+"response/responsedata/"+form_id,
        //                 "columnDefs": [
        //                     {
        //                         "targets": 0,
        //                         "visible": false,
        //                         "searchable": false
        //                     }
        //                 ],
        //                 "order": [[ 0, "asc" ]]
        //             });
        //         },
        //         error: function(data) { alert(data); }
        //     });
        // });

        $(document).on('click', '.btn-add-followup', function(){
            addfollowup();
        });

        function addfollowup(){
            Swal.fire({
                title: 'Add New Followup',
                width: 600,
                html:
                   `
                    <div class="row" style="text-align: left;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Follow Up Description</label>
                                <input required type="text" class="form-control" id="followupDesc" name="followupDesc">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="text-align: left;margin-bottom:30px;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="">Next Followup Date</label>
                                <br>
                                <input type="text" id="nextDate" name="nextDate" data-input>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="float:right;">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary btn-new-followup">Submit</button>
                                <button class="btn btn-danger btn-swal-cancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                   `,
                allowOutsideClick : false,
                showCancelButton: false,
                showConfirmButton: false,
                showCloseButton: true,
                onOpen : function(e){
                    $(e).find("#nextDate").flatpickr({
                        altInput: true,
                        minDate: "today",
                        altFormat: "F j, Y",
                        dateFormat: "Y-m-d",
                    });
                }
            });
        }

        $(document).on('click', '.btn-new-followup', function(){
            var description = $('#followupDesc').val();
            var date = $('#nextDate').val();

            if(description == ''){
                $.notify({
                    icon: "notification_important",
                    message: "Description can't be empty"

                },{
                    type: 'danger',
                    delay: 3000,
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    z_index: 4300,
                });
                return false;
            } else if (date == ''){
                $.notify({
                    icon: "notification_important",
                    message: "Please input next followup date"

                },{
                    type: 'danger',
                    delay: 3000,
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    z_index: 4300,
                });
                return false;
            }
            
            $.ajax({
                type: "POST",
                url: baseURL+"response/insertfollowup",
                data : {
                    "follupDesc" : description, 
                    "nextDate" : date, 
                    "response_id" : response_id,
                    "form_id" : form_id,
                },
                success: function(data){
                    if(data == "sukses"){
                        Swal.fire(
                            'Success!',
                            'Your data has been submitted.',
                            'success'
                        );

                        resposeTable.ajax.reload();
                        $('.VivaTimeline dl').empty();
                        getTimelineData();
                    }
                }
            });
        });

        $(document).on('click', '.btn-swal-cancel', function(){
            swal.close();
        });
    });
</script>
