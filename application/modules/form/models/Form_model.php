<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_model extends Base_Model {

	function __construct() {
        parent::__construct();

        $this->setTable('form_data');
    }

    /**
     * Insert form data into database
     *
     * @param   array  $options
     * @return  object
     */
    function save_form($data) {
        return $this->write($data);
    }

    public function list_form($user_id, $source = FALSE){
        $this->_read_db->select('form_data.form_id');
        $this->_read_db->select("form_data.form_title");
        $this->_read_db->select("form_data.form_pricing");
        $this->_read_db->select("form_data.form_slug");
        $this->_read_db->select("form_data.form_message");
        $this->_read_db->select("form_data.campaign_group_id");
        $this->_read_db->select("form_data.campaign_name");
        $this->_read_db->select("form_data.form_created_date");
        $this->_read_db->select("count(response_data.form_id) as response");
        $this->_read_db->join("response_data", "form_data.form_id = response_data.form_id", "left");
        $this->_read_db->where("form_data.user_id", $user_id);
        $this->_read_db->group_by("form_data.form_id");

        $result = $this->read()->result();
        $data = null;

        if(!$source){
            foreach($result as $row){
                $name = $this->campaign_group($row->campaign_group_id);
    
                $data[] = array(
                    $row->form_id,
                    '<a href="'. base_url("response/responselist?form_name=$row->campaign_name") .'">'.$row->campaign_name.'</a>',
                    $name,
                    $row->form_title,
                    $row->form_pricing,
                    $row->form_created_date,
                    '<a href="'. base_url("response/responselist?form=$row->form_id") .'">Response ('. $row->response .')</a>',
                    '<a class="fas fa-eye detail" href="javascript:void(0)" value="'.$row->form_id.'" rel="tooltip" data-original-title="See Detail"></a>
                    <a class="material-icons link" href="javascript:void(0)" value="'.$row->form_id.'" rel="tooltip" data-original-title="Show Embed Link">link</a>
                        <!-- <a class="material-icons" href="javascript:void(0)" title="Edit">edit</a> -->
                    <a class="material-icons delete" href="javascript:void(0)" value="'.$row->form_id.'" rel="tooltip" data-original-title="Delete">delete</a>'
                );
            }
            return $data;

        } else if ($source == 'abtesting'){
            foreach($result as $row){
                $name = $this->campaign_group($row->campaign_group_id);
    
                $data[] = array(
                    $row->form_id,
                    '',
                    $row->campaign_name,
                    $name,
                    $row->form_slug,
                    $row->form_title,
                    $row->form_message,
                    $row->form_created_date,
                    '<a class="btn btn-primary btn-sm btn-select" href="javascript:void(0)" value="'.$row->form_id.'">Select Form</a>
                     <a class="btn btn-primary btn-sm btn-change" href="javascript:void(0)" style="display:none;">Change Form</a>'
                );
            }
            return $data;
        }
    }

    public function campaign_group($param){
        $this->setTable('campaign_data');

        $this->_read_db->select("campaign_name");
        $this->_read_db->where("campaign_id", $param);

        $data = $this->read()->row();

        return $data->campaign_name;
    }

    public function load_form($param){
        
        $this->_read_db->select("form_slug");
        $this->_read_db->select("form_data");
        $this->_read_db->where("form_id", $param);

        $form = $this->read()->row();

		if ($form){
			return $form;
        }
        
        $this->_read_db->flush_cache();
        $this->_read_db->select("form_data");
        $this->_read_db->where("form_slug", $param);

        $form = $this->read()->row();
        
        if($form){
            return $form->form_data;
        }
        return false;
    }

    public function add_form_show($param){
        $this->_read_db->select("form_show");
        $this->_read_db->where("form_slug", $param);

        $data = $this->read()->row();
        $show = $data->form_show+1;

        $this->_write_db->where('form_slug',$param);

        return $this->update(['form_show'=>$show]);
    }

    public function get_end($param){
        $this->setTable('form_data');

        $this->_read_db->select("form_phone_number");
        $this->_read_db->select("form_message");
        $this->_read_db->where("form_slug", $param);

        $data = $this->read()->row();

        return $data;
    }

    public function get_id($param){
        $this->setTable('form_data');

        $this->_read_db->select("form_id");
        $this->_read_db->where("form_slug", $param);

        $data = $this->read()->row();

        return $data->form_id;
    }

    public function update_appear($data, $param){
        $this->_write_db->where('form_slug',$param);

        return $this->update($data);
    }

    public function delete_form($id){
        $user_id = $this->session->userdata('uid');

        return $this->delete( array('form_id' => $id, 'user_id' => $user_id) );
    }

    public function update_slug($slug, $new_slug){
        $this->_write_db->where('form_slug',$slug);

        return $this->update(['form_slug'=>$new_slug]);
    }

    public function update_click($slug){
        $this->_read_db->select('form_click');
        $this->_read_db->where('form_slug', $slug);

        $data = $this->read()->row();
        $click = $data->form_click;

        $this->_write_db->where('form_slug', $slug);
        return $this->update(['form_click'=>$click+1]);
    }

    public function get_form_statistic($user_id){
        $this->_read_db->select("form_show");
        $this->_read_db->select("form_click");
        $this->_read_db->where("user_id", $user_id);

        $data = $this->read()->result();

        return $data;
    }

    public function get_report_data(){
        $user_id = $this->session->userdata('uid');

        $this->_read_db->select('form_data.form_id');
        $this->_read_db->select('campaign_data.campaign_name as campaign_group');
        $this->_read_db->select("form_data.campaign_name as campaign_name");
        $this->_read_db->select("response_data.nama");
        $this->_read_db->select("response_data.nomor_telpon");
        $this->_read_db->select("response_data.response_submited_date");
        $this->_read_db->select("history_followup.followup_description");
        $this->_read_db->select("history_followup.followup_result");
        $this->_read_db->select("history_followup.followup_date");
        $this->_read_db->select("history_followup.next_followup_date");
        $this->_read_db->select("history_followup.closing_date");
        $this->_read_db->select("history_followup.status");
        $this->_read_db->join("campaign_data","form_data.campaign_group_id = campaign_data.campaign_id");
        $this->_read_db->join("response_data","form_data.form_id = response_data.form_id");
        $this->_read_db->join("history_followup","response_data.response_id = history_followup.response_id", "left");
        $this->_read_db->where("form_data.user_id", $user_id);
        $this->_read_db->order_by("form_data.form_id", "ASC");
        $this->_read_db->order_by("response_data.nama", "ASC");
        $this->_read_db->order_by("history_followup.followup_date", "ASC");

        $result = $this->read()->result();

        $data = null;

        if($result){
            foreach($result as $row){
                $status = ($row->status == 2 ? "Closing" : ($row->status == 1 ? "Next Followup" : "Followup"));

                $data[] = array(
                    $row->form_id,
                    $row->campaign_group,
                    $row->campaign_name,
                    $row->nama,
                    $row->nomor_telpon,
                    $row->response_submited_date,
                    $row->followup_description,
                    $row->followup_result,
                    $row->followup_date,
                    $row->next_followup_date,
                    $row->closing_date,
                    $status,
                );
            }
        }

        return $data;
    }

    public function get_list_id($param){
        $user_id = $this->session->userdata('uid');

        $this->_read_db->select('form_id');
        $this->_read_db->where('campaign_name', $param);
        // $this->_read_db->where("user_id", $user_id);

        $result = $this->read()->result();

        $data = array();

        if($result){
            foreach($result as $row){
                array_push($data, $row->form_id);
            }
        }

        return $data;
    }
}
//bikin else return false