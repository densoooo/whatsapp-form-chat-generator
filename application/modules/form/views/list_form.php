<link rel="stylesheet" href="<?php echo base_url('assets/css/form-style.css');?>" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url('assets/css/all.min.css');?>" type="text/css"/>

<div id="myContent" class="bringins-content">
    <div class="row">
        <div class="col-md-12">
            <div class="form-display">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-primary btn-direct">Copy Direct Link</button>
                    <button class="btn btn-primary btn-edit"><a class="material-icons">edit</a>&nbsp;Edit Slug</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 slug-group" style="display:none;">
                    <label class="bmd-label-floating">Slug</label>
                    <input required type="text" class="form-control slug-form">
                    <p>Slug Only 6 - 20 Characters And Contain Alphabet, Number, Underscore ( _ ), or Hyphen ( - )</p>
                </div>
                <div class="col-md-12">
                    <div class="row editor-group" style="display:none;float:right;">
                        <button class="btn btn-primary btn-save" style="float:right;"><a class="material-icons">save</a>&nbsp;Save</button>
                        <button class="btn btn-primary btn-cancel" style="float:right;"><a class="material-icons">cancel</a>&nbsp;Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Whatsapp Form Generator</h3>
                    <?php
                        $url = base_url('form/create');
                        if($campaignGroup != null) {
                            echo "<a class='btn btn-danger' style='float:right;' href='$url'>Create New Form</a>";
                        } else {
                            echo "<a class='btn btn-danger disabled' style='float:right; color:white;' aria-disabled='true'>Create New Campaign First</a>";
                        }
                    ?>
                    <a class='btn btn-danger' style='float:right; margin-right:30px;' href='<?php echo base_url('form/report'); ?>'>Form Report</a>
                </div>
                <div class="card-body card-table">
                    <div class="filter-group" style="margin-bottom:30px;">
                        <p class='filter-label'>Campaign Group Filter</p>
                        <button class="btn btn-primary btn-reset" style="float:right;display:none">Reset Filter</button>
                    </div>
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Form Name</th>
                                <th>Campaign Group</th>
                                <th>Form Title</th>
                                <th>Pricing</th>
                                <th>Create Date</th>
                                <th>Response</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Form Name</th>
                                <th>Campaign Group</th> 
                                <th>Form Title</th>
                                <th>Pricing</th>
                                <th>Create Date</th>
                                <th>Response</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  var baseURL = "<?php echo base_url("/"); ?>";
</script>
<script>
    $(document).ready(function() {
        var response_id;
        var slug;
        var embed_code;

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        var table = $('#example').DataTable({
            destroy: true,
            "ajax": baseURL+"form/formdata",
            "columnDefs": [
                // { 
                //     "orderable": false, 
                //     "targets": 7 
                // },
                { 
                    "orderable": false, 
                    "targets": 6
                },
                {
                    "targets": 0,
                    "visible": false,
                    "searchable": false
                },

            ],
            "order": [[ 0, "asc" ]],
            initComplete: function () {
                this.api().columns().every( function (i) {
                    var column = this;
                    if(i == 2){
                        var select = $('<select id="group-filter"><option value="" data-placeholder>Select Group</option></select>')
                            .insertAfter('.filter-label')
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
        
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();

                                $(".btn-reset").show();
                            });
        
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' );
                        });
                        easydropdown('#group-filter');
                    }
                });
            }
        });

        $('.btn-reset').on('click', function(){
            $(this).hide();
            table.search('').columns().search('').draw();
            $("#group-filter").prop("selectedIndex", 0);
        });

        var campaign_group = getUrlParameter('campaign_group');
        if(campaign_group){
            table.columns(2).search("^"+campaign_group+"$", true, false, true).draw();
        }

        $('table').on('click', 'a.detail', function(){
            response_id = $(this).attr('value');
            $('#myContent').bringins({
                position: "right",
                width: "50%",
                margin: 50,
                color: "rgba(245,245,245)",
                closeButton: "rgb(102,102,102)",
                zIndex: "3000"
            });

            $.ajax({
                url: baseURL+"form/displayform/"+response_id,
                dataType: 'json',
                success: function(result) {
                    slug = result.slug;

                    $('div.form-display').html(result.form_code);
                    $('.btn-direct').attr('slug', result.slug);
                    $('.slug-form').val(result.slug);
                    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
                }
            });
        });

        $('table').on('click', 'a.delete', function(){
            form_id = $(this).attr('value');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if(result.value){
                    $.ajax({
                        url: baseURL+"form/delete/"+form_id,
                        success: function(result) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            table.ajax.reload();
                        }
                    });
                }
            });
        });

        $('table').on('click', 'a.link', function(){
            form_id = $(this).attr('value');

            $.ajax({
                url: baseURL+"form/displayform/"+form_id,
                dataType: 'json',
                success: function(result) {
                    embed_code = result.script;

                    Swal.fire({
                        title: 'Click Code To Copy',
                        width: 600,
                        html:
                            $('<p>')
                            .addClass('embedcode')
                            .text(embed_code),
                        type: 'info',
                    })
                }
            })
        });

        $('body').on('click', 'p.embedcode', function(){
            copyToClipboard('.embedcode');
            Swal.fire({
                title: 'Code Coppied',
                width: 600,
                html:
                    $('<p>')
                    .addClass('embedcode2')
                    .text(embed_code),
                type: 'success',
            })
        })

        $('#myContent').on('click', 'button.btn-direct', function(){
            var url = baseURL+"_";
            var $temp = $("<input>");
            
            $("body").append($temp);
            $temp.val(url+$(this).attr('slug')).select();
            document.execCommand("copy");
            $temp.remove();

            $.notify({
                icon: "done",
                message: "Direct Link Coppied"
            },{
                type: 'success',
                timer: 4000,
                z_index: 4031,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
        });

        $('#myContent').on('click', 'button.btn-edit', function(){
            $('.slug-group').show();
            $('.slug-form').focus();
            $(this).hide();
            $('.editor-group').show();
        });

        $('#myContent').on('click', 'button.btn-cancel', function(){
            $('.slug-form').val(slug);
            $('.btn-edit').show();
            $('.slug-group, .editor-group').hide();
        });

        $('#myContent').on('click', 'button.btn-save', function(){
            var new_slug = $('.slug-form').val();
            var regex = /^[\w\-]{6,20}$/i;

            if (!regex.test(new_slug)) {
                $.notify({
                    icon: "error",
                    message: "Slug Only 6 - 20 Characters And Contain Alphabet, Number, Underscore, or Hyphen"
                },{
                    type: 'danger',
                    timer: 4000,
                    z_index: 4031,
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
                return false;
            }

            $.ajax({
                url: baseURL+"form/checkslug/"+slug+"/"+new_slug,
                success: function(result) {
                    if(result != "exist"){
                        $.ajax({
                            url: baseURL+"form/displayform/"+response_id,
                            dataType: 'json',
                            success: function(result) {
                                slug = result.slug;

                                $('div.form-display').html(result.form_code);
                                // $('.embed-code').val(result.script);
                                // $('.embed-link').val(result.link);
                                $('.slug-form').val(result.slug);
                                $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
                            }
                        });

                        $('#example').DataTable().ajax.reload();

                        $.notify({
                            icon: "done",
                            message: "Update Slug Success"
                        },{
                            type: 'success',
                            timer: 4000,
                            z_index: 4031,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });

                        $('.slug-form').val(new_slug);
                        $('.btn-direct').attr('slug', new_slug);
                        $('.btn-edit').show();
                        $('.slug-group, .editor-group').hide();
                    } else {
                        $.notify({
                            icon: "error",
                            message: "Slug Already Exist"
                        },{
                            type: 'danger',
                            timer: 4000,
                            z_index: 4031,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });
                        return false;
                    }
                }
            });
        });

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }
    });
</script>