<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Report</h3>
                </div>
                <div class="card-body">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Form ID</th>
                                <th>Campaign Group</th>
                                <th>Campaign Name</th>
                                <th>Customer Name</th>
                                <th>Customer Phone Number</th>
                                <th>Submitted Date</th>
                                <th>Followup Description</th>
                                <th>Followup Result</th>
                                <th>Followup Date</th>
                                <th>Next Followup Date</th>
                                <th>Closing Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                        <tfoot>
                            <tr>
                                <th>Form ID</th>
                                <th>Campaign Group</th>
                                <th>Campaign Name</th>
                                <th>Customer Name</th>
                                <th>Customer Phone Number</th>
                                <th>Submitted Date</th>
                                <th>Followup Description</th>
                                <th>Followup Result</th>
                                <th>Followup Date</th>
                                <th>Next Followup Date</th>
                                <th>Closing Date</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js"></script>

<script type="text/javascript">
  var baseURL = "<?php echo base_url("/"); ?>";
</script>
<script>
    $(document).ready(function() {
        var response_id;
        var slug;
        var table = $('#example').DataTable({
            destroy: true,
            "ajax": baseURL+'form/reporting_data',
            dom: '<"top"B>lfrtip',
            buttons: [
                'print',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    });
</script>