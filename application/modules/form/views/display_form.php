<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <base target="_parent">
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('/assets/img/apple-icon.png')?>">
  <link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/favicon.png')?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url('/assets/css/material-dashboard.css?v=2.1.1') ?>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link href="../assets/demo/demo.css" rel="stylesheet" /> -->
</head>
<body class="">
    <?php 
        echo $form_code;
    ?>
    
    <div class="row">
		<div class="col-md-12">
            <div class="card">
            <h3>To Embed This Form, Copy This Code</h3>
            <textarea>
                <?php echo trim($script); ?>
            </textarea>
            </div>
        </div>
    </div>
</body>

<style>

.edit-input {
    display:none;
}

.edit-button {
    display:none;
}

.edit-number {
	display:none;
}

.save-button {
	display:none;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    var form_id = window.location.pathname.split("/")[3];
    console.log(form_id);
    $('#chatForm').submit(function () {
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "form_id").val(form_id);
        $('#chatForm').append(input);
    });

    console.log($('html').height());
</script>