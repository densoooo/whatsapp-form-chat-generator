<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.minicolors.css');?>" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url('assets/css/form-style.css');?>" type="text/css"/>

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="card form-chat">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Form Detail</h3>
                    <!-- <p class="card-category">Chat us on Whatsapp</p> -->
                </div>
                <div class="card-body">
                    <div class="form-detail">
                        <form id="chatForm2">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Nomor Whatsapp Anda</label>
                                        <input required type="number" class="form-control" id="phoneNumberSelf" name="phoneNumberSelf">
                                        <span class="bmd-help">Gunakan awalan 62 untuk nomor Whatsapp anda, contoh : 6285711188899</span>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <a href="javascript:void(0);" class="material-icons btn-add-number" style="font-size:35px; color:green;">control_point</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row message-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Pesan Ke Whatsapp</label>
                                        <input required type="text" class="form-control" id="message" name="message">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="bmd-label-floating">Group Campaign</label>
                                <br>
                                <select required name="groupGroup" id="my-select">
                                    <option value="" data-placeholder>Select an option</option>
                                    <?php 
                                        foreach($campaignGroup as $data) {
                                            echo "<option value='$data[0]'>$data[1]</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Campaign Name</label>
                                        <input required type="text" class="form-control" id="campaignName" name="campaignName">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Pricing</label>
                                        <input required type="number" class="form-control" id="pricing" name="pricing">
                                        <span class="bmd-help">Harga dari produk yang anda iklankan</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:30px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="save btn btn-primary">Save</button>
                                        <a class="btn btn-danger" href="<?php echo base_url("form") ?>">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<div class="container">
	<div class="row">
		<div class="col-md-5">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Form Configuration</h3>
                </div>
                <div class="card-body">   
                    <div class="header-section">
                        <p style="font-size:20px;">Header</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker" class="color-picker" style="height:30px;font-size:20px;border:none;" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="body-section">
                        <p style="font-size:20px;">Body</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker3" class="color-picker3" style="height:30px;font-size:20px;border:none" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="button-section">
                        <p style="font-size:20px;">Button</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Button Color</label>
                                    <br>
                                    <input type="text" id="color-picker2" class="color-picker2" style="height:30px;font-size:20px;border:none" value="#4f93ce"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>                       
        </div>
		<div class="col-md-7">
            <div id="form-container">
                <div class="containers">
                    <form id="chatForm" action="<?php echo base_url('form/send_message');?>" method="post" accept-charset="utf-8">
                        <div class="card-header" style="margin-bottom:30px;">
                            <h1 class="card-title" rel="tooltip" data-original-title="Click to Edit">Whatsapp Chat</h1>
                            <input type="text" class="edit-title" style="display:none; width:100%;" />
                            <p class="card-category" style="text-align: center;" rel="tooltip" data-original-title="Click to Edit">Chat us on Whatsapp</p>
                            <input type="text" class="edit-subtitle" style="display:none; width:100%;"/>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input required type="text" class="form-control" id="nama" name="nama" readonly>
                                <label class="control-label" style="--colorlabel:#337ab7">Nama</label><i class="bar foo" style="--color:#337ab7"></i>
                            </div>
                            <div class="form-group">
                                <input required type="number" class="form-control" id="nomorTelpon" name="nomorTelpon" readonly>
                                <label class="control-label" style="--colorlabel:#337ab7">Nomor Telpon</label><i class="bar" style="--color:#337ab7"></i>
                            </div>
                            <div class="end-group"></div>
                            <div class="add-group hr-sect">
                                <a href="javascript:void(0);" class="material-icons btn-add-number" style="font-size:35px; color:green;">add</a>
                                <a href="javascript:void(0);" style="color:black;font-size:20px;">Add Field</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="button-container">
                                <button type="submit" class="button btn-submit" disabled><i class="material-icons remove_field" style="float:left; color:white;" href="javascript:void(0);">send</i>&nbsp;<span>Chat Via Whatsapp</span></button>
                                <a class="edit-button-toggle" href="javascript:void(0);">Edit</a>
                                <div class="button-editor" style="margin-bottom:20px; margin-top:-20px;">
                                    <label for="edit-button" class="edit-button" style="display:none;">Button Label</label>
                                    <input type="text" class="edit-button form-control" style="display:none;"/><i class="bar edit-button" style="display:none;"></i>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script src="<?php echo base_url('/assets/js/plugins/jquery.minicolors.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  var formNumber = 0;

//   $("#chatForm :input ").not(".edit-button, .add_field, .edit-title, .edit-subtitle").prop("disabled", true);

  $(".containers").on("click",".edit", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('label'); 
    lbl.hide();
    dad.find('.edit-input').val(lbl.text()).show().focus();
  });
    
  $(".containers").on('focusout', '.edit-input', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
	  dad.find('.form-control').prop({id: label, name: label})
	  dad.find('label').prop('for', label);
      dad.find('label').text(this.value).show();
  });

  $(".containers").on("click",".edit-button-toggle", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
	var btn = dad.find('.button span')
    dad.find('.edit-button').val(btn.text()).show().focus();
	dad.find('.edit-number').show();
  });
    
  $(".containers").on('focusout', '.edit-button', function(e) {
      var dad = $(this).parent();
      $(this).hide();
	  dad.find('.editor').hide();
	  dad.find('.edit-button').hide();
	  dad.parent().find('.button span').text(this.value);
  });

  $(".containers").on("click",".remove_field", function(e){
  	$(this).parent('div').remove();
  });
  
  $('button.add_field').on("click",function(e){
  	e.preventDefault();
    $(
        `<div class="row">
                    <div class="col-md-12">
                        <div class="form-group bmd-form-group">
                            <input type="text" class="edit-input" style="display:none;"/>
                            <label class="bmd-label-floating">Label</label>
                            <input required type="text" class="form-control" id="label" name="label">
                            <a class="edit" href="javascript:void(0);">Edit</a>
                            <a class="material-icons remove_field" style="float:right; font-size:40px; color:red;" href="javascript:void(0);">clear</a>
                        </div>
                    </div>
                </div>
            `
    ).insertBefore('.container .end-group');

    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
  });

  $('div.add-group').on("click",function(e){
  	e.preventDefault();
    $(
        `<div class="form-group">
                <input required type="text" class="form-control" id="label" name="label" readonly>
                <label class="control-label">Label</label><i class="bar"></i>
                <input type="text" class="edit-input" style="display:none;"/>
                <a class="edit" href="javascript:void(0);">Edit</a>
                <a class="material-icons remove_field" style="float:right; font-size:40px; color:red;" href="javascript:void(0);">clear</a>
            </div>
            `
    ).insertBefore('.containers .end-group');

    // $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
  });

  $('button.save').on("click", function(e){
    e.preventDefault();
    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", false);
    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("readonly", false);
    $("a.edit").hide();
    $("a.remove_field").hide();
    $(".edit-button-toggle").hide();

    var color = $('.foo').css('--color');
    $('.bar').css('--color',color);
    $('.control-label').css('--colorlabel',color);

	var str = $("<div/>").append($('#form-container').clone());
	// str.find('a').remove();
    str.find('.add-group').remove();
    str.find('.form-detail').remove();
    str.find('hr').remove();

    var html = str.html();
    
    var top = $(this).parent().parent().parent().parent();
    var formTitle = $('#chatForm .card-title').text();
    var phoneNumber = top.find('#phoneNumberSelf').val();
    var formMessage = top.find('#message').val();
    var campaignName = top.find('#campaignName').val();
    var campaignID = top.find('#my-select').val();
    var pricing = top.find('#pricing').val();

    if(formTitle != '' && phoneNumber != '' && formMessage != '' && campaignName != '' && campaignID != ''){
        if(formNumber > 0){
            var i = 1
            var arrayNumber = [];
            arrayNumber.push({number: phoneNumber, appear: 0});
            while(i <= formNumber){
                if($('#phoneNumberSelf'+i).val() != '' && typeof $('#phoneNumberSelf'+i).val() !== 'undefined'){
                    arrayNumber.push({number: $('#phoneNumberSelf'+i).val(), appear: 0});
                }
                i++;
            }
        }
        $.post(
            '../form/saveForm',
            {   campaign_name: campaignName,
                campaign_group_id: campaignID,
                form_code: html,
                formTitle: formTitle,
                phoneNumber: (formNumber == 0) ? phoneNumber : JSON.stringify(arrayNumber),
                formMessage: formMessage,
                pricing: pricing
            },
            function(data) {
                if(data == "sukses"){
                    alert("Form Berhasil Dibuat");
                    window.location.href = window.location.origin+'/form';
                } else {
                    alert("Form Gagal Dibuat");
                }
            }
        )
    } else {
        alert("Form Tidak Boleh Kosong");
    }
  });

  $('.containers .card-header').on('click','.card-title', function(e){
    e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('.card-title'); 
    lbl.hide();
    dad.find('.edit-title').val(lbl.text()).show().focus();
  });

  $(".containers .card-header").on('focusout', '.edit-title', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
      dad.find('.card-title').text(this.value).show();
  });

  $('.containers .card-header').on('click','.card-category', function(e){
    e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('.card-category'); 
    lbl.hide();
    dad.find('.edit-subtitle').val(lbl.text()).show().focus();
  });

  $(".containers .card-header").on('focusout', '.edit-subtitle', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
      dad.find('.card-category').text(this.value).show();
  });

  $(".btn-add-number").on('click', function(e){
    e.preventDefault();
    if(formNumber < 10){
        formNumber += 1;
        $(
        `<div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <input placeholder="Nomor Whatsapp Anda" required type="number" class="form-control" id="phoneNumberSelf`+formNumber+`" name="phoneNumberSelf`+formNumber+`">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <a href="javascript:void(0);" class="material-icons btn-remove-number" style="font-size:35px; color:red;">close</a>
                                </div>
                            </div>
                        </div>`
        ).insertBefore('.container .message-group');
    } else {
        $(this).css('color', 'black');
    }
  });

  $(".container").on('click', '.btn-remove-number', function(e){
    if(formNumber == 10){
        $(".btn-add-number").css('color', 'green');
    }
    formNumber -= 1;
    $(this).parent().parent().parent('div').remove();
  });
});

$.minicolors.defaults = $.extend($.minicolors.defaults, {
  changeDelay: 200,
  letterCase: 'uppercase',
  defaultValue: '#4f93ce',
});

$('.color-picker').minicolors({
    change: function(value, opacity) {
        $('.card-header').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);

        if ( L > 0.179 ) {
            $('#chatForm .card-title').css( 'color', '#3c4858' );
            $('#chatForm .card-category').css( 'color', '#3c4858' );
        } else {
            $('#chatForm .card-title').css( 'color', 'white' );
            $('#chatForm .card-category').css( 'color', 'white' );
        }
    }
});

$('.color-picker2').minicolors({
    change: function(value, opacity) {
        $('.btn-submit').css('background-color', value);
        $('.btn-submit').css('color', value);
        $('.bar').css('--color',value);
        $('.control-label').css('--colorlabel',value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        if ( L > 0.179 ) {
            $('.btn-submit span').css( 'color', 'black' );
        } else {
            $('.btn-submit span').css( 'color', 'white' );
        }
    }
});

$('.color-picker3').minicolors({
    change: function(value, opacity) {
        $('.containers').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        // if ( L > 0.179 ) {
        //     $('.btn-submit span').css( 'color', 'black' );
        // } else {
        //     $('.btn-submit span').css( 'color', 'white' );
        // }
    }
});

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var r = parseInt(result[1], 16);
    var g = parseInt(result[2], 16);
    var b = parseInt(result[3], 16);

    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function contrast(R,G,B) {
	var C, L;    
    C = [ R/255, G/255, B/255 ];
    for ( var i = 0; i < C.length; ++i ) {
        if ( C[i] <= 0.03928 ) {
            C[i] = C[i] / 12.92
        } else {
            C[i] = Math.pow( ( C[i] + 0.055 ) / 1.055, 2.4);
        }
    }
    L = 0.2126 * C[0] + 0.7152 * C[1] + 0.0722 * C[2];
    return L;
}

</script>