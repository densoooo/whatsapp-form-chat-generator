<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.minicolors.css');?>" type="text/css"/>
<style>
  @import url(https://fonts.googleapis.com/css?family=Roboto);
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  
div#form-container body,
div#form-container input,
div#form-container select,
div#form-container textarea,
div#form-container p,
div#form-container a,
div#form-container * {
  font-family: 'Roboto', sans-serif;
  box-sizing: border-box;
  text-decoration:none;
}
div#form-container .material-icons {
  font-family: 'Material Icons';
  text-decoration:none;
}
div#form-container body::after,
div#form-container body::before,
div#form-container input::after,
div#form-container input::before,
div#form-container select::after,
div#form-container select::before,
div#form-container textarea::after,
div#form-container textarea::before,
div#form-container body *::after,
div#form-container body *::before {
  box-sizing: border-box;
}
div#form-container body {
  background-image: -webkit-linear-gradient(top, #f2f2f2, #e6e6e6);
  background-image: linear-gradient(top, #f2f2f2, #e6e6e6);
}
div#form-container h1 {
  font-family: Roboto,Helvetica,Arial,sans-serif;
  font-weight: 300;
  font-size: 2rem;
  text-align: center;
}
div#form-container .containers {
  position: relative;
  max-width: 40rem;
  margin: 5rem auto;
  background: #fff;
  width: 100%;
  padding: 3rem 5rem 0;
  border-radius: 1px;
}
div#form-container .containers::before {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  box-shadow: 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2);
  -webkit-transform: scale(0.98);
  transform: scale(0.98);
  -webkit-transition: -webkit-transform 0.28s ease-in-out;
  transition: -webkit-transform 0.28s ease-in-out;
  transition: transform 0.28s ease-in-out;
  transition: transform 0.28s ease-in-out, -webkit-transform 0.28s ease-in-out;
  z-index: -1;
}
div#form-container .containers:hover::before {
  -webkit-transform: scale(1);
  transform: scale(1);
}
div#form-container .button-container {
  text-align: center;
}
div#form-container fieldset {
  margin: 0 0 3rem;
  padding: 0;
  border: none;
}
div#form-container .card-header {
  margin: -3rem -5rem 0;
  border-bottom: 1px solid #eee;
  padding: .75rem 1.25rem;
  padding-top: 2rem;
}
div#form-container .form-radio,
div#form-container .form-group {
  position: relative;
  margin-top: 2.25rem;
  margin-bottom: 2.25rem;
}
div#form-container .form-inline > .form-group,
div#form-container .form-inline > .btn {
  display: inline-block;
  margin-bottom: 0;
}
div#form-container .form-help {
  margin-top: 0.125rem;
  margin-left: 0.125rem;
  color: #b3b3b3;
  font-size: 0.8rem;
}
div#form-container .checkbox .form-help,
div#form-container .form-radio .form-help,
div#form-container .form-group .form-help {
  position: absolute;
  width: 100%;
}
div#form-container .checkbox .form-help {
  position: relative;
  margin-bottom: 1rem;
}
div#form-container .form-radio .form-help {
  padding-top: 0.25rem;
  margin-top: -1rem;
}
div#form-container .form-group input {
  height: 1.9rem;
}
div#form-container .form-group textarea {
  resize: none;
}
div#form-container .form-group select {
  width: 100%;
  font-size: 1rem;
  height: 1.6rem;
  padding: 0.125rem 0.125rem 0.0625rem;
  background: none;
  border: none;
  line-height: 1.6;
  box-shadow: none;
}
div#form-container .form-group .control-label {
  position: absolute;
  top: 0.25rem;
  pointer-events: none;
  padding-left: 0.125rem;
  z-index: 1;
  color: #b3b3b3;
  font-size: 1rem;
  font-weight: normal;
  font-family: 'Roboto', sans-serif;
  -webkit-transition: all 0.28s ease;
  transition: all 0.28s ease;
}
div#form-container .form-group .bar {
  position: relative;
  border-bottom: 0.0625rem solid #999;
  display: block;
}
div#form-container .form-group .bar::before {
  content: '';
  height: 0.125rem;
  width: 0;
  left: 50%;
  bottom: -0.0625rem;
  position: absolute;
  background: var(--color, #337ab7);
  -webkit-transition: left 0.28s ease, width 0.28s ease;
  transition: left 0.28s ease, width 0.28s ease;
  z-index: 2;
}
div#form-container .form-group input,
div#form-container .form-group textarea {
  display: block;
  background: none;
  padding: 0.125rem 0.125rem 0.0625rem;
  font-size: 1rem;
  border-width: 0;
  border-color: transparent;
  line-height: 1.9;
  width: 100%;
  color: transparent;
  -webkit-transition: all 0.28s ease;
  transition: all 0.28s ease;
  box-shadow: none;
}
div#form-container .form-group input[type="file"] {
  line-height: 1;
}
div#form-container .form-group input[type="file"] ~ .bar {
  display: none;
}
div#form-container .form-group select,
div#form-container .form-group input:focus,
div#form-container .form-group input:valid,
div#form-container .form-group input.form-file,
div#form-container .form-group input.has-value,
div#form-container .form-group textarea:focus,
div#form-container .form-group textarea:valid,
div#form-container .form-group textarea.form-file,
div#form-container .form-group textarea.has-value {
  color: #333;
}
div#form-container .form-group select ~ .control-label,
div#form-container .form-group input:focus ~ .control-label,
div#form-container .form-group input:valid ~ .control-label,
div#form-container .form-group input.form-file ~ .control-label,
div#form-container .form-group input.has-value ~ .control-label,
div#form-container .form-group textarea:focus ~ .control-label,
div#form-container .form-group textarea:valid ~ .control-label,
div#form-container .form-group textarea.form-file ~ .control-label,
div#form-container .form-group textarea.has-value ~ .control-label {
  font-size: 0.8rem;
  color: gray;
  top: -1rem;
  left: 0;
}
div#form-container .form-group select:focus,
div#form-container .form-group input:focus,
div#form-container .form-group textarea:focus {
  outline: none;
}
div#form-container .form-group select:focus ~ .control-label,
div#form-container .form-group input:focus ~ .control-label,
div#form-container .form-group textarea:focus ~ .control-label {
  color: var(--colorlabel,#337ab7);
}
div#form-container .form-group select:focus ~ .bar::before,
div#form-container .form-group input:focus ~ .bar::before,
div#form-container .form-group textarea:focus ~ .bar::before {
  width: 100%;
  left: 0;
}
div#form-container .checkbox label,
div#form-container .form-radio label {
  position: relative;
  cursor: pointer;
  padding-left: 2rem;
  text-align: left;
  color: #333;
  display: block;
}
div#form-container .checkbox input,
div#form-container .form-radio input {
  width: auto;
  opacity: 0.00000001;
  position: absolute;
  left: 0;
}
div#form-container .radio {
  margin-bottom: 1rem;
}
div#form-container .radio .helper {
  position: absolute;
  top: -0.25rem;
  left: -0.25rem;
  cursor: pointer;
  display: block;
  font-size: 1rem;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  color: #999;
}
div#form-container .radio .helper::before,
div#form-container .radio .helper::after {
  content: '';
  position: absolute;
  left: 0;
  top: 0;
  margin: 0.25rem;
  width: 1rem;
  height: 1rem;
  -webkit-transition: -webkit-transform 0.28s ease;
  transition: -webkit-transform 0.28s ease;
  transition: transform 0.28s ease;
  transition: transform 0.28s ease, -webkit-transform 0.28s ease;
  border-radius: 50%;
  border: 0.125rem solid currentColor;
}
div#form-container .radio .helper::after {
  -webkit-transform: scale(0);
  transform: scale(0);
  background-color: #337ab7;
  border-color: #337ab7;
}
div#form-container .radio label:hover .helper {
  color: #337ab7;
}
div#form-container .radio input:checked ~ .helper::after {
  -webkit-transform: scale(0.5);
  transform: scale(0.5);
}
div#form-container .radio input:checked ~ .helper::before {
  color: #337ab7;
}
div#form-container .checkbox {
  margin-top: 3rem;
  margin-bottom: 1rem;
}
div#form-container .checkbox .helper {
  color: #999;
  position: absolute;
  top: 0;
  left: 0;
  width: 1rem;
  height: 1rem;
  z-index: 0;
  border: 0.125rem solid currentColor;
  border-radius: 0.0625rem;
  -webkit-transition: border-color 0.28s ease;
  transition: border-color 0.28s ease;
}
div#form-container .checkbox .helper::before,
div#form-container .checkbox .helper::after {
  position: absolute;
  height: 0;
  width: 0.2rem;
  background-color: #337ab7;
  display: block;
  -webkit-transform-origin: left top;
  transform-origin: left top;
  border-radius: 0.25rem;
  content: '';
  -webkit-transition: opacity 0.28s ease, height 0s linear 0.28s;
  transition: opacity 0.28s ease, height 0s linear 0.28s;
  opacity: 0;
}
div#form-container .checkbox .helper::before {
  top: 0.65rem;
  left: 0.38rem;
  -webkit-transform: rotate(-135deg);
  transform: rotate(-135deg);
  box-shadow: 0 0 0 0.0625rem #fff;
}
div#form-container .checkbox .helper::after {
  top: 0.3rem;
  left: 0;
  -webkit-transform: rotate(-45deg);
  transform: rotate(-45deg);
}
div#form-container .checkbox label:hover .helper {
  color: #337ab7;
}
div#form-container .checkbox input:checked ~ .helper {
  color: #337ab7;
}
div#form-container .checkbox input:checked ~ .helper::after,
div#form-container .checkbox input:checked ~ .helper::before {
  opacity: 1;
  -webkit-transition: height 0.28s ease;
  transition: height 0.28s ease;
}
div#form-container .checkbox input:checked ~ .helper::after {
  height: 0.5rem;
}
div#form-container .checkbox input:checked ~ .helper::before {
  height: 1.2rem;
  -webkit-transition-delay: 0.28s;
  transition-delay: 0.28s;
}
div#form-container .radio + .radio,
div#form-container .checkbox + .checkbox {
  margin-top: 1rem;
}
div#form-container .has-error .legend.legend,
div#form-container .has-error.form-group .control-label.control-label {
  color: #d9534f;
}
div#form-container .has-error.form-group .form-help,
div#form-container .has-error.form-group .helper,
div#form-container .has-error.checkbox .form-help,
div#form-container .has-error.checkbox .helper,
div#form-container .has-error.radio .form-help,
div#form-container .has-error.radio .helper,
div#form-container .has-error.form-radio .form-help,
div#form-container .has-error.form-radio .helper {
  color: #d9534f;
}
div#form-container .has-error .bar::before {
  background: #d9534f;
  left: 0;
  width: 100%;
}
div#form-container .button {
  position: relative;
  background: currentColor;
  border: 1px solid currentColor;
  font-size: 1.1rem;
  color: #4f93ce;
  margin: 0 0 3rem 0;
  padding: 0.75rem 3rem;
  cursor: pointer;
  -webkit-transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  overflow: hidden;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}
div#form-container .button span {
  color: #fff;
  position: relative;
  z-index: 1;
}
div#form-container .button::before {
  content: '';
  position: absolute;
  background: #071017;
  border: 50vh solid #1d4567;
  width: 30vh;
  height: 30vh;
  border-radius: 50%;
  display: block;
  top: 50%;
  left: 50%;
  z-index: 0;
  opacity: 1;
  -webkit-transform: translate(-50%, -50%) scale(0);
  transform: translate(-50%, -50%) scale(0);
}
div#form-container .button:hover {
  color: #337ab7;
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.2);
}
div#form-container .button:active::before,
div#form-container .button:focus::before {
  -webkit-transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  -webkit-transform: translate(-50%, -50%) scale(1);
  transform: translate(-50%, -50%) scale(1);
  opacity: 0;
}
div#form-container .button:focus {
  outline: none;
}

div#form-container input::-webkit-outer-spin-button,
div#form-container input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

div#form-container input[type=number] {
    -moz-appearance:textfield; /* Firefox */
}

div#form-container .hr-sect {
	display: flex;
	flex-basis: 100%;
	align-items: center;
	color: rgba(0, 0, 0, 0.35);
	font-size: 12px;
	margin: 8px 0px;
}

div#form-container .hr-sect::before,
div#form-container .hr-sect::after {
	content: "";
	flex-grow: 1;
	background: rgba(0, 0, 0, 0.35);
	height: 1px;
	font-size: 0px;
	line-height: 0px;
}
</style>

<div class="container form-detail">
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="bmd-label-floating">Group Campaign</label>
                        <br>
                        <select required name="groupGroup" id="my-select">
                            <option value="" data-placeholder>Select an option</option>
                            <?php 
                                foreach($campaignGroup as $data) {
                                    echo "<option value='$data[0]'>$data[1]</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Campaign Name</label>
                                <input required type="text" class="form-control" id="campaignName" name="campaignName">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="form-container">
    <div class="containers">
        <form>
            <div class="card-header card-header-primary" style="margin-bottom:30px;">
                <h1 class="card-title">Whatsapp Chat</h1>
                <input type="text" class="edit-title" style="display:none; width:100%;" />
                <p class="card-category" style="text-align: center;">Chat us on Whatsapp</p>
                <input type="text" class="edit-subtitle" style="display:none; width:100%;"/>
                <div id="color-picker2" class="color-picker2" ></div>
            </div>
            <div class="form-group">
                <input required type="text" class="form-control" id="nama" name="nama" >
                <label class="control-label" style="--colorlabel:#337ab7">Nama</label><i class="bar" style="--color:#337ab7"></i>
            </div>
            <div class="form-group">
                <input required type="number" class="form-control" id="nomorTelpon" name="nomorTelpon" >
                <label class="control-label" style="--colorlabel:#337ab7">Nomor Telpon</label><i class="bar" style="--color:#337ab7"></i>
            </div>
            <div class="end-group"></div>
            <div class="add-group hr-sect">
                <a href="javascript:void(0);" class="material-icons btn-add-number" style="font-size:35px; color:green;">add</a>
                <a href="javascript:void(0);" style="color:black;font-size:20px;font-family: 'Roboto', sans-serif;">Add Field</a>
            </div>
            <div class="form-group">
                <div class="button-container">
                    <div id="color-picker" class="color-picker" ></div>
                    <button type="submit" class="button btn-submit" disabled><span>Chat Via Whatsapp</span></button>
                    <a class="edit-button-toggle" href="javascript:void(0);" style="font-family: 'Roboto', sans-serif;">Edit</a>
                    <div class="button-editor" style="margin-bottom:20px; margin-top:-20px;">
                        <label for="edit-button" class="edit-button" style="display:none;font-family: 'Roboto', sans-serif;">Button Label</label>
                        <input type="text" class="edit-button form-control" style="display:none;"/><i class="bar edit-button" style="display:none;"></i>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="card form-chat">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Whatsapp Chat</h3>
                    <input type="text" class="edit-title" style="display:none;" />
                    <p class="card-category">Chat us on Whatsapp</p>
                    <input type="text" class="edit-subtitle" style="display:none;"/>
                </div>
                <div class="card-body">
                    <form id="chatForm" action="<?php echo base_url('form/send_message');?>" method="post" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Nama</label>
                                    <input required type="text" class="form-control" id="nama" name="nama">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Nomor Telpon</label>
                                    <input required type="number" class="form-control" id="nomorTelpon" name="nomorTelpon">
                                </div>
                            </div>
                        </div>
                        <div class="end-group"></div>
                        <div class="row add-group" style="margin-bottom:30px;">
                            <div style="display: table; margin: 0 auto;">
                                <button type="button" class="add_field btn btn-info">Add Field</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Chat Via Whatsapp">
                            <a class="edit-button-toggle" href="javascript:void(0);">Edit</a>
                            <div class="button-editor" style="margin-top:20px;">
                                <label for="edit-button" class="edit-button" style="display:none;">Button Label</label>
                                <input type="text" class="edit-button form-control" style="display:none;"/>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="form-detail">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Nomor Whatsapp Anda</label>
                                    <input required type="number" class="form-control" id="phoneNumberSelf" name="phoneNumberSelf">
                                    <p>Gunakan awalan 62 untuk nomor Whatsapp anda, contoh : 6285711188899</p>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <a href="javascript:void(0);" class="material-icons btn-add-number" style="font-size:35px; color:green;">control_point</a>
                                </div>
                            </div>
                        </div>
                        <div class="row message-group">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Pesan Ke Whatsapp</label>
                                    <input required type="text" class="form-control" id="message" name="message">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<div class="container">
    <div class="row">
		<div class="col-md-12">
            <button type="button" class="save btn btn-primary">Save</button>
            <a class="btn btn-danger" href="<?php echo base_url("form") ?>">Cancel</a>
        </div>
    </div>
</div>

<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script src="<?php echo base_url('/assets/js/plugins/jquery.minicolors.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  var formNumber = 0;

  $("#chatForm :input ").not(".edit-button, .add_field").prop("disabled", true);

  $(".containers").on("click",".edit", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('label'); 
    lbl.hide();
    dad.find('.edit-input').val(lbl.text()).show().focus();
  });
    
  $(".containers").on('focusout', '.edit-input', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
	  dad.find('.form-control').prop({id: label, name: label})
	  dad.find('label').prop('for', label);
      dad.find('label').text(this.value).show();
  });

  $(".containers").on("click",".edit-button-toggle", function(e){
  	e.preventDefault();
    var dad = $(this).parent();
	var btn = dad.find('.button span')
    dad.find('.edit-button').val(btn.text()).show().focus();
	dad.find('.edit-number').show();
  });
    
  $(".containers").on('focusout', '.edit-button', function(e) {
      var dad = $(this).parent();
      $(this).hide();
	  dad.find('.editor').hide();
	  dad.find('.edit-button').hide();
	  dad.parent().find('.button span').text(this.value);
  });

  $(".containers").on("click",".remove_field", function(e){
  	$(this).parent('div').remove();
  });
  
  $('button.add_field').on("click",function(e){
  	e.preventDefault();
    $(
        `<div class="row">
                    <div class="col-md-12">
                        <div class="form-group bmd-form-group">
                            <input type="text" class="edit-input" style="display:none;"/>
                            <label class="bmd-label-floating">Label</label>
                            <input required type="text" class="form-control" id="label" name="label">
                            <a class="edit" href="javascript:void(0);">Edit</a>
                            <a class="material-icons remove_field" style="float:right; font-size:40px; color:red;" href="javascript:void(0);">clear</a>
                        </div>
                    </div>
                </div>
            `
    ).insertBefore('.container .end-group');

    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
  });

  $('div.add-group').on("click",function(e){
  	e.preventDefault();
    $(
        `<div class="form-group">
                <input required type="text" class="form-control" id="label" name="label" disabled>
                <label class="control-label">Label</label><i class="bar"></i>
                <input type="text" class="edit-input" style="display:none;"/>
                <a class="edit" href="javascript:void(0);">Edit</a>
                <a class="material-icons remove_field" style="float:right; font-size:40px; color:red;" href="javascript:void(0);">clear</a>
            </div>
            `
    ).insertBefore('.containers .end-group');

    // $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
  });

  $('button.save').on("click", function(e){
    $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", false);
    $("a.edit").hide();
    $("a.remove_field").hide();
    $(".edit-button-toggle").hide();

	var str = $("<div/>").append($('.form-chat').clone());
	// str.find('a').remove();
    str.find('.add-group').remove();
    str.find('.form-detail').remove();
    str.find('hr').remove();
	var html = str.html();
    var top = $(this).parent().parent().parent().parent();
    var formTitle = top.find('.card-title').text();
    var phoneNumber = top.find('#phoneNumberSelf').val();
    var formMessage = top.find('#message').val();
    var campaignName = top.find('#campaignName').val();
    var campaignID = top.find('#my-select').val();

    if(formTitle != '' && phoneNumber != '' && formMessage != '' && campaignName != '' && campaignID != ''){
        if(formNumber > 0){
            var i = 1
            var arrayNumber = [];
            arrayNumber.push({number: phoneNumber, appear: 0});
            while(i <= formNumber){
                if($('#phoneNumberSelf'+i).val() != '' && typeof $('#phoneNumberSelf'+i).val() !== 'undefined'){
                    arrayNumber.push({number: $('#phoneNumberSelf'+i).val(), appear: 0});
                }
                i++;
            }
        }
        $.post(
            '../form/saveForm',
            {   campaign_name: campaignName,
                campaign_group_id: campaignID,
                form_code: html,
                formTitle: formTitle,
                phoneNumber: (formNumber == 0) ? phoneNumber : JSON.stringify(arrayNumber),
                formMessage: formMessage,
            },
            function(data) {
                if(data == "sukses"){
                    alert("Form Berhasil Dibuat");
                    window.location.href = window.location.origin+'/form';
                } else {
                    alert("Form Gagal Dibuat");
                }
            }
        )
    } else {
        alert("Form Tidak Boleh Kosong");
    }
  });

  $('.card-header').on('click','.card-title', function(e){
    e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('.card-title'); 
    lbl.hide();
    dad.find('.edit-title').val(lbl.text()).show().focus();
  });

  $(".card-header").on('focusout', '.edit-title', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
      dad.find('.card-title').text(this.value).show();
  });

  $('.card-header').on('click','.card-category', function(e){
    e.preventDefault();
    var dad = $(this).parent();
    var lbl = dad.find('.card-category'); 
    lbl.hide();
    dad.find('.edit-subtitle').val(lbl.text()).show().focus();
  });

  $(".card-header").on('focusout', '.edit-subtitle', function(e) {
      var dad = $(this).parent();
	  var label = this.value.toLowerCase();
      $(this).hide();
      dad.find('.card-category').text(this.value).show();
  });

  $(".btn-add-number").on('click', function(e){
    e.preventDefault();
    if(formNumber < 10){
        formNumber += 1;
        $(
        `<div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <input placeholder="Nomor Whatsapp Anda" required type="number" class="form-control" id="phoneNumberSelf`+formNumber+`" name="phoneNumberSelf`+formNumber+`">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <a href="javascript:void(0);" class="material-icons btn-remove-number" style="font-size:35px; color:red;">close</a>
                                </div>
                            </div>
                        </div>`
        ).insertBefore('.container .message-group');
    } else {
        $(this).css('color', 'black');
    }
  });

  $(".container").on('click', '.btn-remove-number', function(e){
    if(formNumber == 10){
        $(".btn-add-number").css('color', 'green');
    }
    formNumber -= 1;
    $(this).parent().parent().parent('div').remove();
  });
});

$.minicolors.defaults = $.extend($.minicolors.defaults, {
  changeDelay: 200,
  letterCase: 'uppercase',
  defaultValue: '#4f93ce',
});

$('.color-picker').minicolors({
    change: function(value, opacity) {
        $('.btn-submit').css('background-color', value);
        $('.btn-submit').css('color', value);
        $('.bar').css('--color',value);
        $('.control-label').css('--colorlabel',value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        if ( L > 0.179 ) {
            $('.btn-submit span').css( 'color', 'black' );
        } else {
            $('.btn-submit span').css( 'color', 'white' );
        }
    }
});

$('.color-picker2').minicolors({
    change: function(value, opacity) {
        $('.card-header').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);

        if ( L > 0.179 ) {
            $('.card-title').css( 'color', '#3c4858' );
            $('.card-category').css( 'color', '#3c4858' );
        } else {
            $('.card-title').css( 'color', 'white' );
            $('.card-category').css( 'color', 'white' );
        }
    }
});

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var r = parseInt(result[1], 16);
    var g = parseInt(result[2], 16);
    var b = parseInt(result[3], 16);

    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function contrast(R,G,B) {
	var C, L;    
    C = [ R/255, G/255, B/255 ];
    for ( var i = 0; i < C.length; ++i ) {
        if ( C[i] <= 0.03928 ) {
            C[i] = C[i] / 12.92
        } else {
            C[i] = Math.pow( ( C[i] + 0.055 ) / 1.055, 2.4);
        }
    }
    L = 0.2126 * C[0] + 0.7152 * C[1] + 0.0722 * C[2];
    return L;
}

</script>