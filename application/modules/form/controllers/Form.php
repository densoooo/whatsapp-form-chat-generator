<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends MX_Controller {

	public function __construct(){

        $this->load->helper('url');

        $this->load->model('form/form_model', 'form');
        $this->load->model('campaign/campaign_model', 'campaign');
        $this->load->model('abtesting/abtesting_model', 'abtesting');
        $this->load->model('response/response_model', 'response');
        $this->load->model('response/followup_model', 'followup');
    }

    public function index(){
        if($this->session->userdata('nama')){
            $user_id = $this->session->userdata('uid');
        
            $listForm = $this->form->list_form($user_id);
            if(!empty($listForm)){
                $data['listForm'] = $listForm;
            } else {
                $data['listForm'] = [];
            }

            $param = $this->session->userdata('uid');
            $data['campaignGroup'] = $campaignGroup = $this->campaign->get_all_campaign(array('where'=>array('user_id'=>$param)));
            $data['recent_followup'] = $this->followup->recent_followup();

            $this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('form/list_form', $data);
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function report(){
        $data['recent_followup'] = $this->followup->recent_followup();

        $this->load->view('elements/sidebar');
        $this->load->view('elements/navbar', $data);
        $this->load->view('form/reporting');
        $this->load->view('elements/footer');
    }

    public function reporting_data(){
        $data = $this->form->get_report_data();

        if($data){
            $output = array(
                "draw" => $this->input->post('draw', TRUE),
                "recordsTotal" => sizeof($data),
                "recordsFiltered" => sizeof($data),
                "data" => $data);
        } else {
            $output = array(
                "draw" => $this->input->post('draw', TRUE),
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []);
        }
        echo json_encode($output);
    }

    public function formdata($source = FALSE){
        if($this->session->userdata('nama')){

            $user_id = $this->session->userdata('uid');

            if(!$source){
                $dataForm = $this->form->list_form($user_id);
            } else if ($source == 'abtesting'){
                $dataForm = $this->form->list_form($user_id, $source);
            }

            if($dataForm){
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($dataForm),
                    "recordsFiltered" => sizeof($dataForm),
                    "data" => $dataForm);
            } else {
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => []);
            }
            echo json_encode($output);
		} else {
			redirect(base_url());
		}
    }

    public function create(){
        if($this->session->userdata('nama')){
            $param = $this->session->userdata('uid');
            $campaignGroup = $this->campaign->get_all_campaign(array('where'=>array('user_id'=>$param)));

            $data['recent_followup'] = $this->followup->recent_followup();
    
            if($campaignGroup){
                $data['campaignGroup'] = $campaignGroup;
                $this->load->view('elements/sidebar');
                $this->load->view('elements/navbar', $data);
                $this->load->view('form/create_form', $data);
                $this->load->view('elements/footer');
            } else {
                $data['_1'] = "NO CAMPAIGN";
                $data['button'] = 'CREATE CAMPAIGN';
                $data['url'] = base_url('campaign/create');

                $this->load->view('elements/sidebar');
                $this->load->view('elements/navbar', $data);
                $this->load->view('elements/no_item', $data);
                $this->load->view('elements/footer');
            }
		} else {
			redirect(base_url());
		}
    }

    public function saveForm() {
        if($this->session->userdata('nama')){
            $data = [
                'user_id' => $this->session->userdata('uid',TRUE),
                'form_slug' => $this->generate_slug(),
                'campaign_group_id' => $this->input->post('campaign_group_id'),
                'campaign_name' => $this->input->post('campaign_name',TRUE),
                'form_created_date' => date('Y-m-d H:i:s'),
                'form_edited_date' => date('Y-m-d H:i:s'),
                'form_data' => $this->input->post('form_code'),
                'form_title' => $this->input->post('formTitle',TRUE),
                'form_phone_number' => $this->input->post('phoneNumber',TRUE),
                'form_message' => $this->input->post('formMessage',TRUE),
                'form_pricing' => $this->input->post('pricing',TRUE),
            ];
    
            if($this->form->save_form($data)) {
                echo "sukses";
            } else {
                echo "gagal";
            }
		} else {
			redirect(base_url());
		}
    }

    public function generate_slug(){
        if($this->session->userdata('nama')){
            $id = round(microtime(true) * 1000); 

            $alphabet='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
            $base = strlen($alphabet);
            $short = '';
            while($id) {
                $id = ($id-($r=$id%$base))/$base;
                $short = $alphabet{abs($r)} . $short;
            };

            return $short;
        }
    }

    //belum
    public function editform($id){
        if($this->session->userdata('nama')){
            $param = $this->session->userdata('group_id');
            $campaignGroup = $this->campaign->get_all_campaign(array('where'=>array('group_id'=>$param)));
    
            if($campaignGroup){
                $data['campaignGroup'] = $campaignGroup;
            }
            $data['recent_followup'] = $this->followup->recent_followup();
    
            $this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('form/create_form', $data);
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function displayform($id) {
        if($this->session->userdata('nama')){
            if($id != null) {
                $code = $this->form->load_form($id);
                if($code != null){ 
                    $slug = $code->form_slug;

                    $source = base_url("form/formscript/$slug");  
                    $script = "<div id='form-chat-$slug' class='form-chat'></div><script src='$source'></script>";
                    $link = base_url("_$slug");
    
                    $data['form_code'] = $code->form_data;
                    $data['script'] = $script;
                    $data['link'] = $link;
                    $data['slug'] = $slug;
    
                    echo json_encode($data);
                }
            }
		} else {
			redirect(base_url());
		}
    }
    
    public function loadform($id) {
		if($id != null) {
			$code = $this->form->load_form($id);
			if($code != null){   
                $data = array('form_code' => $code->form_data);

				$this->load->view('form/form4', $data);
				$this->load->view('elements/script-only');
			}
		}
    }

    public function formscript($slug){
        if($slug != null) {

            $this->form->add_form_show($slug);
            $code = $this->form->load_form($slug);
            
            if($code != null){   
                $baseurl = base_url();
                $code = trim(preg_replace('/\s+/', ' ', $code));
                $code = str_replace('"',"\'",$code);

                $script = "var baseURL = '".$baseurl."';var script = document.createElement('script');script.src = '".$baseurl."assets/js/views/widget/form-widget-4.js';document.head.appendChild(script);";
                (isset($form_style)) ? $script = "var form_style = '".$form_style."';" . $script : null;
                $script2 = "document.getElementById('form-chat-$slug').innerHTML = '".$code."';";

                echo $script;
                echo $script2;
            }
		}
    }
    
    public function send_message(){
        $additonalInfo = array();
        foreach ($_POST as $key => $value) {
            switch (htmlspecialchars($key)) {
                case "nama" :
                    $nama = htmlspecialchars($value);
                    break;
                case "nomorTelpon" :
                    $nomorTelpon = htmlspecialchars($value);
                    break;
                case "form_slug" :
                    $form_slug = htmlspecialchars($value);
                    break;
                case "ip_address" :
                    $ip_address = htmlspecialchars($value);
                    break;
                case "form_browser" :
                    $form_browser = htmlspecialchars($value);
                    break;
                case "form_url" :
                    $form_url = htmlspecialchars($value);
                    break;
                case "test_id" :
                    $test_id = htmlspecialchars($value);
                    break;
                default :
                    $additonalInfo[htmlspecialchars($key)] = htmlspecialchars($value);
                    break;
            }
        }

        $form_id = $this->form->get_id($form_slug);

        if(isset($test_id)){
            $test_data = $this->abtesting->get_style_click($test_id);

            $style = $test_data->form_variant;
            $click = $test_data->form_variant_click;

            $array_style = json_decode($style, true);
            $key = substr(array_search($form_id, $array_style), 0, -3);

            $array = json_decode($click, true);
            $array["$key"] = $array["$key"] + 1;

            $this->check_threshold($test_id, $form_id, $array["$key"]);

            $style = json_encode($array);
            $this->abtesting->update_style_click($style, $test_id);
            $this->form->update_click($form_slug);
        }
        
        $data = [
            'form_id' => $form_id,
            'response_submited_date' => date('Y-m-d H:i:s'),
            'nama' => $nama,
            'ip_address' => $ip_address,
            'browser' => $form_browser,
            'url_referer' => $form_url,
            'nomor_telpon' => $nomorTelpon,
            'additional_info' => json_encode($additonalInfo)
        ];

        $this->redirect_whatsapp($form_slug, $data);
    }

    public function redirect_whatsapp($slug, $data){
        if($slug){
            $message = $this->form->get_end($slug);

            $wa_messsage = rawurlencode($message->form_message);

            if(strlen($message->form_phone_number) < 15){
                $phone = $message->form_phone_number;
            } else {
                $array = $arraySorted = json_decode($message->form_phone_number);
                
                usort($arraySorted, function($previous, $next) {
                    return $previous->appear > $next->appear ? 1 : -1;
                });

                $phone = $arraySorted[0]->number;

                foreach($array as $item){
                    if($item->number == $phone) {
                        $item->appear += 1;
                    }
                }

                $phoneNumberData = [ 'form_phone_number' => json_encode($array)];

                $this->form->update_appear($phoneNumberData, $slug);
            }

            $data['cs_number'] = $phone;
            $this->response->save_response($data);

            redirect("https://api.whatsapp.com/send?phone=$phone&text=$wa_messsage");
        }
    }

    public function delete($id){
        if($this->session->userdata('nama')){
            if($id != null){
                $this->form->delete_form($id);

                redirect('form');
            }
        }
    }

    public function checkslug($slug, $newslug){
        if($this->session->userdata('nama')){
            if($this->form->load_form($newslug)){
                echo "exist";
            } else {
                $this->form->update_slug($slug, $newslug);
                echo "notexsist";
            }
        }
    }

    public function get_style($form_id, $test_id){
        $style_data = $this->abtesting->get_style_data($form_id, $test_id);

        $arrayShow = json_decode($style_data->form_variant_show, true);
        $arrayID = json_decode($style_data->form_variant, true);
                
        $keys =  array_search(min($arrayShow), $arrayShow);

        if($keys == 'style1'){
            $data = $this->form->load_form($form_id);
            $code = $data->form_data;
        } else if($keys == 'style2'){
            $style_id = $arrayID['style2_id'];
            $code = $this->abtesting->get_variant($style_id);
        } else if($keys == 'style3'){
            $style_id = $arrayID['style3_id'];
            $code = $this->abtesting->get_variant($style_id);
        }

        $arrayShow["$keys"] = $arrayShow["$keys"]+1;

        $data = [ 'form_variant_show' => json_encode($arrayShow)];
        $this->abtesting->update_appear($test_id, $data);
        
        return [$code, $keys];
    }

    public function check_threshold($test_id, $form_id, $click_number){
        $result = $this->abtesting->check_threshold($test_id);

        $percent = ($click_number/$result->testing_click_target)*100;
        $completed = ($percent >= $result->testing_threshold) ? true : false;

        if($completed){
            $this->abtesting->update_test_status($test_id, 2);
        }

        return;
    }
}