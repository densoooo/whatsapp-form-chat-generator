<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class err404 extends MX_Controller {

    public function __construct(){

        $this->load->helper('url');

        $this->load->model('form/form_model', 'form');
        $this->load->model('campaign/campaign_model', 'campaign');
        $this->load->model('abtesting/abtesting_model', 'abtesting');
    }

    public function index(){
        $this->load->view('404');
    }
}