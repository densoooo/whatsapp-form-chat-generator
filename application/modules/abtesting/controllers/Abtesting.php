<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abtesting extends MX_Controller {

	public function __construct(){

        $this->load->helper('url');

        $this->load->model('abtesting/abtesting_model', 'abtesting');
        $this->load->model('form/form_model', 'form');
        $this->load->model('response/followup_model', 'followup');
    }

    public function index(){
        if($this->session->userdata('nama')){
            $user_id = $this->session->userdata('uid');

            $data['dataForm'] = $this->form->list_form($user_id);
            $data['recent_followup'] = $this->followup->recent_followup();

            $this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('abtesting/test_main', $data);
            $this->load->view('elements/footer');
        } else {
            redirect(base_url());
        } 
    }

    public function create(){
        if($this->session->userdata('nama')){
            $user_id = $this->session->userdata('uid');

            $data['dataForm'] = $this->form->list_form($user_id);
            $data['recent_followup'] = $this->followup->recent_followup();

            if($data['dataForm']){
                $this->load->view('elements/sidebar');
                $this->load->view('elements/navbar', $data);
                $this->load->view('abtesting/test_create', $data);
                $this->load->view('elements/footer');
            } else {
                $data['_1'] = "NO FORM";
                $data['button'] = 'CREATE FORM';
                $data['url'] = base_url('form/create');

                $this->load->view('elements/sidebar');
                $this->load->view('elements/navbar', $data);
                $this->load->view('elements/no_item', $data);
                $this->load->view('elements/footer');
            }
        } else {
            redirect(base_url());
        } 
    }

    public function testingdata(){
        if($this->session->userdata('nama')){
            $dataTest = $this->abtesting->get_data_test();

            if($dataTest){
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($dataTest),
                    "recordsFiltered" => sizeof($dataTest),
                    "data" => $dataTest);
            } else {
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => []);
            }

            echo json_encode($output);
        } else {
            redirect(base_url());
        } 
    }

    public function add(){
        if($this->session->userdata('nama')){

            $data = [
                'user_id' => $this->session->userdata('uid'),
                'testing_name' => $this->input->post('test_name', TRUE),
                'form_variant' => $this->input->post('form_variant', TRUE),
                'form_variant_show' => $this->input->post('form_variant_show', TRUE),
                'form_variant_click' => $this->input->post('form_variant_click', TRUE),
                'form_style_number' => $this->input->post('form_style_number', TRUE),
                'testing_click_target' => $this->input->post('click_target', TRUE),
                'testing_threshold' => $this->input->post('testing_threshold',TRUE),
                'testing_status' => 1,
                'testing_created_date' => date('Y-m-d H:i:s'),
                'testing_edited_date' => date('Y-m-d H:i:s'),
            ];

            $test_id = $this->abtesting->save_testing($data);

            if($test_id){
                $this->session->set_flashdata('notice_message', array(
                    "type"			=> "info",
                    "icon"			=> "fa-info",
                    "icon-text"		=> "Info",
                    "message"		=> 'Success Create New Test'
                ));
                echo "sukses";
            } else {
                echo "gagal";
            }
        } else {
			redirect(base_url());
		} 
    }   

    public function insert_variant($test_id, $form_id, $style2_code = FALSE, $style3_code = FALSE){
        if($this->session->userdata('nama')){
            if($style2_code){
                $data = [
                    'form_id' => $form_id,
                    'testing_id' => $test_id,
                    'variant_data' => $style2_code,
                    'variant_created_date' => date('Y-m-d H:i:s'),
                    'variant_edited_date' => date('Y-m-d H:i:s'),
                ];

                $style2_id = $this->abtesting->save_style($data);
            }

            if($style3_code){
                $data = [
                    'form_id' => $form_id,
                    'testing_id' => $test_id,
                    'variant_data' => $style3_code,
                    'variant_created_date' => date('Y-m-d H:i:s'),
                    'variant_edited_date' => date('Y-m-d H:i:s'),
                ];

                $style3_id = $this->abtesting->save_style($data);
            }

            if($style2_code && $style3_code){
                $variant['style2_id'] = $style2_id;
                $variant['style3_id'] = $style3_id;
            } else if ($style2_code){
                $variant['style2_id'] = $style2_id;
            } else if($style3_code){
                $variant['style2_id'] = $style3_id;
            }

            return json_encode($variant);
        } else {
            redirect(base_url());
        } 
    }

    public function prev_test($form_id, $test_id){
        if($this->session->userdata('nama')){
            $count = $this->abtesting->check_prev($form_id);

            if($count > 1){
                $this->abtesting->update_prev($form_id, $test_id);
            }
        } else {
            redirect(base_url());
        } 
    }

    public function update_status($test_id, $status, $form_id){
        if($this->session->userdata('nama')){
            if($status == 0){
                $this->abtesting->update_test_status($test_id, $status);
            } else if($status == 1){
                $this->abtesting->update_prev($form_id, $test_id);
                $this->abtesting->update_test_status($test_id, $status);
            }
            echo "sukses";
        } else {
            redirect(base_url());
        } 
    }

    public function get_style_code($test_id, $form_id){
        $variant_id = $this->abtesting->get_style_id($test_id);
        $variant_id = json_decode($variant_id);

        $data[0] = $this->form->load_form($form_id)->form_data;
        $data[1] = $this->abtesting->get_variant($variant_id->style2_id);
        (count(get_object_vars($variant_id)) == 2) ? $data[2] = $this->abtesting->get_variant($variant_id->style3_id) : null;
        
        echo json_encode($data);
    }

    public function test_embed_code($test_id){
        if($this->session->userdata('nama')){
            if($test_id != null) {

                $source = base_url("abtesting/testscript/$test_id");  
                $script = "<div id='form-chat-$test_id' class='form-chat'></div><script src='$source'></script>";

                echo $script;

                // $data = $this->abtesting->get_form_id($test_id);
                // if($data != null){ 
                //     $list_id = json_decode($data->form_variant, true);
                //     $list_appear = json_decode($data->form_variant_show, true);

                //     $key = array_search(min($list_appear), $list_appear);

                //     $id = $list_id[$key."_id"]; 
                //     $code = $this->form->load_form($id);

                //     print_r($code);
                //     return;
                //     $slug = $code->form_slug;

                //     $source = base_url("form/formscript/$slug");  
                //     $script = "<div id='form-chat-$slug' class='form-chat'></div><script src='$source'></script>";
                //     $link = base_url("_$slug");
    
                //     $data['form_code'] = $code->form_data;
                //     $data['script'] = $script;
                //     $data['link'] = $link;
                //     $data['slug'] = $slug;
    
                //     echo json_encode($data);
                // }
            }
		} else {
			redirect(base_url());
		}
    }

    public function testscript($test_id){
        if($test_id != null){
            $data = $this->abtesting->get_form_id($test_id);

            if($data != null){ 

                $list_id = json_decode($data->form_variant, true);
                $list_appear = json_decode($data->form_variant_show, true);

                $key = array_search(min($list_appear), $list_appear);

                $id = $list_id[$key."_id"]; 
                $data = $this->form->load_form($id);

                $list_appear["$key"] = $list_appear["$key"]+1;

                $appear_data = [ 'form_variant_show' => json_encode($list_appear)];
                $this->abtesting->update_appear($test_id, $appear_data);

                $code = $data->form_data;
                $slug = $data->form_slug;

                if($code != null){   
                    $baseurl = base_url();
                    $code = trim(preg_replace('/\s+/', ' ', $code));
                    $code = str_replace('"',"\'",$code);
    
                    $script = "var baseURL = '".$baseurl."';var form_slug = '".$slug."'; var test_id = '".$test_id."';var script = document.createElement('script'); script.src = '".$baseurl."assets/js/views/widget/form-widget-4.js';document.head.appendChild(script);";
                    $script2 = "document.getElementById('form-chat-$test_id').innerHTML = '".$code."';";
    
                    echo $script;
                    echo $script2;
                }
            }
        }
    }
}