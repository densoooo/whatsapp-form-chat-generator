<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abtesting_model extends Base_Model {

	function __construct() {
        parent::__construct();

        $this->setTable('ab_testing');
    }

    /**
     * Insert testing data into database
     *
     * @param   array  $options
     * @return  object
     */
    function save_testing($data) {
        $this->write($data);

        $this->_read_db->select('testing_id');
        $this->_read_db->where(['user_id'=>$data['user_id'],'testing_name'=>$data['testing_name']]);
        $this->_read_db->limit(1);
        $this->_read_db->order_by('user_id',"DESC");

        $result = $this->read()->row();

        return $result->testing_id;
    }

    public function get_data_test(){
        $this->_read_db->select('testing_id');
        $this->_read_db->select('testing_name');
        $this->_read_db->select('form_variant_show');
        $this->_read_db->select('form_variant_click');
        $this->_read_db->select('testing_click_target');
        $this->_read_db->select('testing_threshold');
        $this->_read_db->select('testing_status');
        $this->_read_db->select('testing_created_date');
        $this->_read_db->select('testing_edited_date');
        $this->_read_db->select('testing_threshold_pass');

        $this->_read_db->where('user_id',$this->session->userdata('uid'));

        $result = $this->read()->result();
        $data = null;
        foreach($result as $row){
            // $tag_select = '<select class="form-control" id="test-status" name="test-status" data-id="'.$row->testing_id.'" form-id="'.$row->form_id.'" required>';
            // $tag_select2 = '</select>';

            // $status = ($row->testing_status == 0 ? $tag_select."<option value=0 selected>Inactive</option><option value=1>Active</option>".$tag_select2 
            //         : ($row->testing_status == 1 ? $tag_select."<option value=0>Inactive</option><option value=1 selected>Active</option>".$tag_select2
            //         : "Finished"));

            $status = ($row->testing_status == 0 ? "Inactive" 
                    : ($row->testing_status == 1 ? "Active"
                    : "Finished"));

            $data[] = array(
                $row->testing_id,
                $row->testing_name,
                $row->form_variant_show,
                $row->form_variant_click,
                $row->testing_click_target,
                $row->testing_threshold." %",
                $status,
                $row->testing_created_date,
                $row->testing_threshold_pass,
                "<button class='btn btn-primary btn-sm btn-detail'>Detail</button>
                 <button class='btn btn-primary btn-sm btn-embed' test-id=".$row->testing_id.">Show Embed Code</button>"
            );
        }
        return $data;
    }

    function save_style($data){
        $this->setTable('form_variant');

        $this->write($data);

        $this->_read_db->select('variant_id');
        $this->_read_db->where(['form_id'=>$data['form_id'],'testing_id'=>$data['testing_id']]);
        $this->_read_db->limit(1);
        $this->_read_db->order_by('variant_id',"DESC");

        $result = $this->read()->row();

        $this->setTable('ab_testing');

        return $result->variant_id;
    }

    function update_variant($test_id, $form_variant){
        $this->_write_db->where('testing_id', $test_id);

        return $this->update(['form_variant'=>$form_variant]);
    }

    function get_style_data($form_id, $test_id){
        $this->_read_db->select('form_variant');
        $this->_read_db->select('form_variant_show');
        $this->_read_db->select('form_variant_click');
        $this->_read_db->select('testing_click_target');
        $this->_read_db->select('testing_threshold');
        $this->_read_db->select('testing_threshold_type');

        $this->_read_db->where(['form_id'=>$form_id,'testing_id'=>$test_id]);
        $this->_read_db->limit(1);
        $this->_read_db->order_by('testing_id',"DESC");

        return $this->read()->row();
    }

    public function get_variant($variant_id){
        $this->setTable('form_variant');

        $this->_read_db->select('variant_data');
        $this->_read_db->where(['variant_id'=>$variant_id]);

        $result = $this->read()->row();

        $this->setTable('ab_testing');

        return $result->variant_data;
    }

    public function get_style_click($test_id){
        $this->_read_db->select('form_variant');
        $this->_read_db->select('form_variant_click');
        $this->_read_db->where(['testing_id'=>$test_id]);

        $result = $this->read()->row();

        return $result;
    }

    public function update_style_click($style, $test_id){
        $this->_write_db->where('testing_id', $test_id);

        return $this->update(['form_variant_click'=>$style]);
    }

    public function update_appear($test_id, $data){
        $this->_write_db->where('testing_id', $test_id);

        return $this->update($data);
    }

    public function check_prev($form_id){
        $this->_read_db->where(['form_id'=>$form_id]);

        $result = $this->read()->result();

        return sizeof($result);
    }

    public function update_prev($form_id, $test_id){
        $this->_write_db->where('form_id', $form_id);
        $this->_write_db->where_not_in('testing_status', 2);
        $this->_write_db->where_not_in('testing_id', $test_id);

        return $this->update(['testing_status'=>0]);
    }

    public function check_threshold($test_id){
        $this->_read_db->select('testing_click_target');
        $this->_read_db->select('testing_threshold');
        $this->_read_db->where('testing_id', $test_id);

        $result = $this->read()->row();

        return $result;
    }

    public function get_style_id($test_id){
        $this->_read_db->select('form_variant');
        $this->_read_db->where('testing_id', $test_id);

        $result = $this->read()->row();

        return $result->form_variant;
    }

    public function update_code($original_code, $test_id){
        $this->setTable('form_variant');

        $this->_write_db->where('testing_id', $test_id);

        $result = $this->update(['variant_data'=>$original_code]);

        $this->setTable('ab_testing');

        return $result;
    }

    public function update_test_status($test_id, $status){
        $this->_write_db->where('testing_id', $test_id);

        return $this->update(['testing_status'=>$status]);
    }

    public function get_test_number(){
        $user_id = $this->session->userdata('uid');

        $this->_read_db->select('testing_status');
        $this->_read_db->where('user_id', $user_id);

        $result = $this->read()->result();

        return $result;
    }

    public function get_form_id($test_id){
        $this->_read_db->select('form_variant');
        $this->_read_db->select('form_variant_show');
        $this->_read_db->where('testing_id', $test_id);

        $result = $this->read()->row();

        return $result;
    }
}