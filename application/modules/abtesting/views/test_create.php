<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link rel="stylesheet" href="<?php echo base_url('assets/css/form-style.css');?>" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css');?>" type="text/css"/>
<link rel="stylesheet" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css">
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.minicolors.css');?>" type="text/css"/>

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Create New A|B Testing</h3>
                </div>
                <div class="card-body">
                    <form id="testingForm" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Test Name</label>
                                    <input required type="text" class="form-control" id="testName" name="testName" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Click Target</label>
                                    <input required type="number" class="form-control" id="clickTarget" name="clickTarget" >
                                </div>
                            </div>
                        </div>
                        <div class="row" style="align-items: center;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Testing Threshold</label>
                                    <input required type="number" class="form-control" id="testingThreshold" name="testingThreshold" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>% Click Target</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="#example">Select Form</label>
                                <table id="example" class="display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th></th>
                                            <th>Form Name</th>
                                            <th>Campaign Group</th>
                                            <th>Form Slug</th>
                                            <th>Form Title</th>
                                            <th>Form Message</th>
                                            <th>Create Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th></th>
                                            <th>Form Name</th>
                                            <th>Campaign Group</th> 
                                            <th>Form Slug</th>
                                            <th>Form Title</th>
                                            <th>Form Message</th>
                                            <th>Create Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-submit" value="Create New Test">
                            <a class="btn btn-danger" href="<?php echo base_url("abtesting") ?> ">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<div class="container form-style-list-1" style="margin-bottom:50px;display:none;">
	<div class="row justify-content-center">
		<div class="col-md-6">
            <div class="form-display">
            </div>
        </div>
        <div class="col-md-6 add-style-group">
            <div class="clearfix">
                <button class="btn btn-primary add-style"><i class="material-icons btn-add-number" style="font-size:35px; color:green;">add</i>Add Form Style</button>
            </div>
        </div>
    </div>
</div>

<div class="container form-style-list-2" style="margin-bottom:50px;display:none;">
	<div class="row justify-content-center">
		<div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Form Configuration</h3>
                </div>
                <div class="card-body">   
                    <div class="header-section">
                        <p style="font-size:20px;">Header</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker" class="color-picker" style="height:30px;font-size:20px;border:none;" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="body-section">
                        <p style="font-size:20px;">Body</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker3" class="color-picker3" style="height:30px;font-size:20px;border:none" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="button-section">
                        <p style="font-size:20px;">Button</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Button Color</label>
                                    <br>
                                    <input type="text" id="color-picker2" class="color-picker2" style="height:30px;font-size:20px;border:none" value="#4f93ce"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-delete"><i class="material-icons" style="font-size:35px; color:red;">delete</i>Remove Form Style</button>
                </div>
            </div>
        </div>
        <div class="col-md-6 add-style-group">
            <div class="form-display">
            
            </div>
        </div>
    </div>
</div>

<div class="container form-style-list-3" style="display:none;">
	<div class="row justify-content-center">
		<div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Form Configuration</h3>
                </div>
                <div class="card-body">   
                    <div class="header-section">
                        <p style="font-size:20px;">Header</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker" class="color-picker" style="height:30px;font-size:20px;border:none;" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="body-section">
                        <p style="font-size:20px;">Body</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Background Color</label>
                                    <br>
                                    <input type="text" id="color-picker3" class="color-picker3" style="height:30px;font-size:20px;border:none" value="#ffffff"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="button-section">
                        <p style="font-size:20px;">Button</p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Button Color</label>
                                    <br>
                                    <input type="text" id="color-picker2" class="color-picker2" style="height:30px;font-size:20px;border:none" value="#4f93ce"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-delete"><i class="material-icons" style="font-size:35px; color:red;">delete</i>Remove Form Style</button>
                </div>
            </div>
        </div>
        <div class="col-md-6 add-style-group">
            <div class="form-display">
            
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script src="<?php echo base_url('/assets/js/plugins/jquery.minicolors.min.js');?>" type="text/javascript"></script>
<script>
$(document).ready(function(){
    var baseURL = "<?php echo base_url("/"); ?>";
    var style = 1;
    var style2 = false;
    var style3 = false;
    var vacant = 2;
    var code;

    var dataTable = $('#example').DataTable({
        destroy: true,
        "ajax": baseURL+"form/formdata/abtesting",
        "columnDefs": [
            { 
                "orderable": false, 
                "targets": 8 
            },
            {
                "targets": 0,
                "visible": false,
                "searchable": false
            },
            {  
                "width": "20%", 
                "targets": 4 
            },
            {
                "orderable": false,
                "className": 'select-checkbox',
                "targets":   1,
                "data": null,
                "defaultContent": '',
                'checkboxes': {
                    'selectRow': true
                }
            }

        ],
        "select": {
            "style":    'multi',
        },
        "dom" : "rtip",
        "order": [[ 0, "asc" ]]
    });

    // $('table').on('click', 'a.btn-select', function(){
    //     $.ajax({
    //         url: baseURL+"form/displayform/"+form_id,
    //         dataType: 'json',
    //         success: function(result) {
    //             slug = result.slug;
    //             code = result.form_code;

    //             dataTable.columns(4).search("^"+slug+"$", true, false, true).draw();

    //             $('a.btn-select').hide();
    //             $('a.btn-change').show();

    //             $('.form-style-list-1 .form-display').html(result.form_code);
    //             $("#chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);

    //             $('.form-style-list-1').show();
    //         }
    //     });
    // });

    $('table').on('click', 'a.btn-change', function(){
        if(style > 1){
            if (confirm('Are you sure you want to change from? Any unsaved edit will be lose')) {
                dataTable.columns(4).search("", true, false, true).draw();

                style = 1;
                
                $('div').remove('#form-container');

                $('a.btn-change').hide();
                $('a.btn-select').show();
                $('.form-style-list-1, .form-style-list-2, .form-style-list-3').hide();
            }
        } else {
            dataTable.columns(4).search("", true, false, true).draw();
                
            $('div').remove('#form-container');

            $('a.btn-change').hide();
            $('a.btn-select').show();
            $('.form-style-list-1, .form-style-list-2, .form-style-list-3').hide();
        }

        style2 = false;
        style3 = false;
        vacant = 2;

        $('button.add-style').prop('disabled', false);
        $('#formId').val('');
    });

    // $('button.add-style').on("click",function(e){
    //     style += 1 ;

    //     if(!style2){
    //         vacant = 2;
    //         style2 = true;
    //     } else {
    //         vacant = 3;
    //         style3 = true;
    //     }

    //     $('.form-style-list-'+vacant).show();
    //     $('.form-style-list-'+vacant+' .form-display').append(code);
    //     $(".form-style-list-"+vacant+" .edit-button-toggle").show();

    //     $('.form-style-list-'+vacant+' .card-header').on('click','.card-title', function(e){
    //         e.preventDefault();
    //         var dad = $(this).parent();
    //         var lbl = dad.find('.card-title'); 
    //         lbl.hide();
    //         dad.find('.edit-title').val(lbl.text()).show().focus();
    //     });

    //     $(".form-style-list-"+vacant+" .card-header").on('focusout', '.edit-title', function(e) {
    //         var dad = $(this).parent();
    //         var label = this.value.toLowerCase();
    //         $(this).hide();
    //         dad.find('.card-title').text(this.value).show();
    //     });

    //     $('.form-style-list-'+vacant+' .card-header').on('click','.card-category', function(e){
    //         e.preventDefault();
    //         var dad = $(this).parent();
    //         var lbl = dad.find('.card-category'); 
    //         lbl.hide();
    //         dad.find('.edit-subtitle').val(lbl.text()).show().focus();
    //     });

    //     $(".form-style-list-"+vacant+" .card-header").on('focusout', '.edit-subtitle', function(e) {
    //         var dad = $(this).parent();
    //         var label = this.value.toLowerCase();
    //         $(this).hide();
    //         dad.find('.card-category').text(this.value).show();
    //     });

    //     $(".form-style-list-"+vacant+" .containers").on("click",".edit-button-toggle", function(e){
    //         e.preventDefault();
    //         var dad = $(this).parent();
    //         var btn = dad.find('.button span')
    //         dad.find('.edit-button').val(btn.text()).show().focus();
    //         dad.find('.edit-number').show();
    //     });
            
    //     $(".containers").on('focusout', '.edit-button', function(e) {
    //         var dad = $(this).parent();
    //         $(this).hide();
    //         dad.find('.editor').hide();
    //         dad.find('.edit-button').hide();
    //         dad.parent().find('.button span').text(this.value);
    //     });

    //     $('html, body').animate({
    //         scrollTop: $('.form-style-list-'+vacant).offset().top
    //     }, 1000);

    //     (style == 3) ? $(this).prop('disabled', true) : null;

    //     $(".form-style-list-"+vacant+" #chatForm :input ").not(".edit-title, .edit-subtitle, .edit-button, .add_field, .edit-input").prop("disabled", true);

    // });

    // $('button.btn-delete').on('click', function(){
    //     var dad = $(this).parents('.container');
    //     var current = dad.attr("class").split(' ')[1].split('-')[3];

    //     $('.form-style-list-'+current+' div').remove('#form-container');
    //     style -= 1;

    //     if(current == 2){
    //         vacant = 2;
    //         style2 = false;
    //     } else {
    //         vacant = 3;
    //         style3 = false;
    //     }

    //     (style != 3) ? $('button.add-style').prop('disabled', false) : null;

    //     dad.hide();
    // });

    $('form#testingForm').submit(function(e) {
        e.preventDefault();
        var test_name = $('#testName').val();
        var click_target = $('#clickTarget').val();
        var testing_threshold = $('#testingThreshold').val();
        var threshold_type = $('#thresholdType').val();
        var rows_selected = dataTable.column(1).checkboxes.selected();
        var style = {};
        var show = {};

        $.each(rows_selected, function(index, rowId){
            var i = index+1;
            style["style"+i+"_id"] = rowId[0];
            show["style"+i] = 0;
        });
        
        var styleNumber = Object.keys(style).length;
        var listStyle = JSON.stringify(style);
        var listShow = JSON.stringify(show);

        var html_code = function(style_number){
            $(".form-style-list-"+style_number+" #chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", false);
            $(".form-style-list-"+style_number+" #chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("readonly", false);
            $(".form-style-list-"+style_number+" a.edit").hide();
            $(".form-style-list-"+style_number+" a.remove_field").hide();
            $(".form-style-list-"+style_number+" .edit-button-toggle").hide();

            var color = $('.form-style-list-'+style_number+' .foo').css('--color');
            $('.form-style-list-'+style_number+' .bar').css('--color',color);
            $('.form-style-list-'+style_number+' .control-label').css('--colorlabel',color);

            var str = $("<div/>").append($('.form-style-list-'+style_number+' #form-container').clone());
            return str.html();
        }

        if(styleNumber < 2){
            $.notify({
                icon: "notification_important",
                message: "Choose minimum 2 forms"
            },{
                type: 'danger',
                timer: 4000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        } else if (testing_threshold > 100){
            $.notify({
                icon: "notification_important",
                message: "Testing Threshold must lower than 100%"
            },{
                type: 'danger',
                timer: 4000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });

            $('#testingThreshold').focus();
            return false;
        }
        
        $.post(
            '../abtesting/add',
            {   
                test_name: test_name,
                click_target: click_target,
                testing_threshold: testing_threshold,
                form_style_number: styleNumber,
                form_variant: listStyle,
                form_variant_show: listShow,
                form_variant_click: listShow
            },
            function(data) {
                if(data == "sukses"){
                    window.location.href = window.location.origin+'/abtesting';
                } else {
                    $.notify({
                        icon: "notification_important",
                        message: "Failed To Create Test"
                    },{
                        type: 'danger',
                        timer: 4000,
                        placement: {
                            from: 'top',
                            align: 'right'
                        }
                    });
                }
                return false;
            }
        )
        return false;
    });

    // $('input.btn-submit').on('click', function(e){
    //     console.log("AA");
    //     return false;
    // });
});

$.minicolors.defaults = $.extend($.minicolors.defaults, {
  changeDelay: 200,
  letterCase: 'uppercase',
  defaultValue: '#4f93ce',
});

$('.form-style-list-2 .color-picker').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-2 .card-header').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);

        if ( L > 0.179 ) {
            $('.form-style-list-2 #chatForm .card-title').css( 'color', '#3c4858' );
            $('.form-style-list-2 #chatForm .card-category').css( 'color', '#3c4858' );
        } else {
            $('.form-style-list-2 #chatForm .card-title').css( 'color', 'white' );
            $('.form-style-list-2 #chatForm .card-category').css( 'color', 'white' );
        }
    }
});

$('.form-style-list-2 .color-picker2').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-2 .btn-submit').css('background-color', value);
        $('.form-style-list-2 .btn-submit').css('color', value);
        $('.form-style-list-2 .bar').css('--color',value);
        $('.form-style-list-2 .control-label').css('--colorlabel',value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        if ( L > 0.179 ) {
            $('.form-style-list-2 .btn-submit span').css( 'color', 'black' );
        } else {
            $('.form-style-list-2 .btn-submit span').css( 'color', 'white' );
        }
    }
});

$('.form-style-list-2 .color-picker3').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-2 .containers').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        // if ( L > 0.179 ) {
        //     $('.btn-submit span').css( 'color', 'black' );
        // } else {
        //     $('.btn-submit span').css( 'color', 'white' );
        // }
    }
});

$('.form-style-list-3 .color-picker').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-3 .card-header').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);

        if ( L > 0.179 ) {
            $('.form-style-list-3 #chatForm .card-title').css( 'color', '#3c4858' );
            $('.form-style-list-3 #chatForm .card-category').css( 'color', '#3c4858' );
        } else {
            $('.form-style-list-3 #chatForm .card-title').css( 'color', 'white' );
            $('.form-style-list-3 #chatForm .card-category').css( 'color', 'white' );
        }
    }
});

$('.form-style-list-3 .color-picker2').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-3 .btn-submit').css('background-color', value);
        $('.form-style-list-3 .btn-submit').css('color', value);
        $('.form-style-list-3 .bar').css('--color',value);
        $('.form-style-list-3 .control-label').css('--colorlabel',value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        if ( L > 0.179 ) {
            $('.form-style-list-3 .btn-submit span').css( 'color', 'black' );
        } else {
            $('.form-style-list-3 .btn-submit span').css( 'color', 'white' );
        }
    }
});

$('.form-style-list-3 .color-picker3').minicolors({
    change: function(value, opacity) {
        $('.form-style-list-3 .containers').css('background-color', value);

        var rgb = hexToRgb(value);
        var L = contrast(rgb.r, rgb.g, rgb.b);
        // if ( L > 0.179 ) {
        //     $('.btn-submit span').css( 'color', 'black' );
        // } else {
        //     $('.btn-submit span').css( 'color', 'white' );
        // }
    }
});

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var r = parseInt(result[1], 16);
    var g = parseInt(result[2], 16);
    var b = parseInt(result[3], 16);

    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function contrast(R,G,B) {
	var C, L;    
    C = [ R/255, G/255, B/255 ];
    for ( var i = 0; i < C.length; ++i ) {
        if ( C[i] <= 0.03928 ) {
            C[i] = C[i] / 12.92
        } else {
            C[i] = Math.pow( ( C[i] + 0.055 ) / 1.055, 2.4);
        }
    }
    L = 0.2126 * C[0] + 0.7152 * C[1] + 0.0722 * C[2];
    return L;
}
</script>