<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">A/B Testing List</h3>
                </div>
                <div class="card-body">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Test Name</th>
                                <th>Form Show</th>
                                <th>Form Click</th>
                                <th>Form Click Target</th>
                                <th>Form Click Threshold</th>
                                <th>Testing Status</th>
                                <th>Test Created Date</th>
                                <th>Test Threshold Pass</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Test Name</th>
                                <th>Form Show</th>
                                <th>Form Click</th>
                                <th>Form Click Target</th>
                                <th>Form Click Threshold</th>
                                <th>Testing Status</th>
                                <th>Test Created Date</th>
                                <th>Test Threshold Pass</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container form-detail-group">
    <div class="row">
		<div class="col-md-4 form-style-list-1" style="display:none;">
            <div class="card-header card-header-primary" style="margin-bottom:20px;">
                <h3 class="card-title">Original Style</h3>
            </div>
            <div class="form-display">
                
            </div>
            <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-8 form-detail">
                        <h3 class="card-title">Form Show</h3>
                        <h3 class="card-title">Form Click</h3>
                        <h3 class="card-title">Form Click Threshold</h3>
                        <h3 class="card-title">Test Progress</h3>
                    </div>
                    <div class="col-md-4 form-detail">
                        <h3 class="card-title form-show"></h3>
                        <h3 class="card-title form-click"></h3>
                        <h3 class="card-title form-threshold"></h3>
                        <h3 class="card-title form-progress"></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 form-style-list-2" style="display:none;">
            <div class="card-header card-header-primary" style="margin-bottom:20px;">
                <h3 class="card-title">Style 2</h3>
            </div>
            <div class="form-display">
            
            </div>
            <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-8 form-detail">
                        <h3 class="card-title">Form Show</h3>
                        <h3 class="card-title">Form Click</h3>
                        <h3 class="card-title">Form Click Threshold</h3>
                        <h3 class="card-title">Test Progress</h3>
                    </div>
                    <div class="col-md-4 form-detail">
                        <h3 class="card-title form-show"></h3>
                        <h3 class="card-title form-click"></h3>
                        <h3 class="card-title form-threshold"></h3>
                        <h3 class="card-title form-progress"></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 form-style-list-3" style="display:none;">
            <div class="card-header card-header-primary" style="margin-bottom:20px;">
                <h3 class="card-title">Style 3</h3>
            </div>
            <div class="form-display">
            
            </div>
            <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-md-8 form-detail">
                        <h3 class="card-title">Form Show</h3>
                        <h3 class="card-title">Form Click</h3>
                        <h3 class="card-title">Form Click Threshold</h3>
                        <h3 class="card-title">Test Progress</h3>
                    </div>
                    <div class="col-md-4 form-detail">
                        <h3 class="card-title form-show"></h3>
                        <h3 class="card-title form-click"></h3>
                        <h3 class="card-title form-threshold"></h3>
                        <h3 class="card-title form-progress"></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css');?>" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url('assets/css/form-style.css');?>" type="text/css"/>
<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script type="text/javascript">
  var baseURL = "<?php echo base_url("/"); ?>";
</script>
<script>
$(document).ready(function() {

    var lastValue;
    var show = 0;
    var embed_code;

    var dataTable = $('#example').DataTable({
        destroy: true,
        "ajax": baseURL+"abtesting/testingdata",
        "columnDefs": [
            { 
                "orderable": false, 
                "targets": 9
            },
            {
                "targets": 0,
                "visible": false,
                "searchable": false
            },
        ],
        "order": [[ 0, "asc" ]]
    });
    
    $('#example').on('click', '#test-status', function(){
        lastValue = $(this).val();
    }).on('change', '#test-status', function(){
        var test_id = $(this).attr('data-id');
        var form_id = $(this).attr('form-id');
        var status = $(this).val();
        if(confirm("Are you sure want to change the test status?")){
            $.ajax({
                url: baseURL+"abtesting/update_status/"+test_id+"/"+status+"/"+form_id,
                success: function(result) {
                    if(result == "sukses"){
                        $('#example').DataTable({
                            destroy: true,
                            "ajax": baseURL+"abtesting/testingdata",
                            "columnDefs": [
                                {
                                    "targets": 0,
                                    "visible": false,
                                    "searchable": false
                                },
                            ],
                            "order": [[ 0, "asc" ]]
                        });
                    }
                }
            });
        } else {
            $(this).val(lastValue);
        }
    });

    $('#example').on('click', '.btn-detail', function(){
        $('.form-style-list-1, .form-style-list-2, .form-style-list-3').hide();

        $(this).removeClass("btn-detail").addClass("btn-hide").text("Hide");

        var data_row = dataTable.row($(this).closest('tr')).data();
        var test_id = JSON.parse(data_row[0]);
        var form_id = data_row[2];
        var form_show = JSON.parse(data_row[3]);
        var form_click = JSON.parse(data_row[4]);

        if(status != 1){
            status += 1;
            $.ajax({
                url: baseURL+"abtesting/get_style_code/"+test_id+"/"+form_id,
                success: function(result){
                    var code = JSON.parse(result);
                    for (var i = 0;i<code.length;i++){
                        var style = i+1;
                        $('.form-style-list-'+style).show();

                        $('.form-style-list-'+style+' .form-display').append(code[i]);
                        $(".form-style-list-"+style+" #chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
                        $(".form-style-list-"+style+" .form-show").text(form_show['style'+style]);
                        $(".form-style-list-"+style+" .form-click").text(form_click['style'+style]);

                        if(data_row[7] == "click"){
                            $(".form-style-list-"+style+" .form-threshold").text(data_row[6]);

                            var progress = (form_click['style'+style]/data_row[6])*100;
                            $(".form-style-list-"+style+" .form-progress").text(progress.toFixed(2)+"%");
                        } else {
                            $(".form-style-list-"+style+" .form-threshold").text(data_row[6]+"%");

                            var threshold = (data_row[6]*data_row[5])/100;
                            var progress = (form_click['style'+style]/threshold)*100;
                            $(".form-style-list-"+style+" .form-progress").text(progress.toFixed(2)+"%");
                        }

                        $('html, body').animate({
                            scrollTop: $('.form-detail-group').offset().top
                        }, 1000);
                    }
                }
            })
        } else {
            $('div').remove('#form-container');
            $.ajax({
                url: baseURL+"abtesting/get_style_code/"+test_id+"/"+form_id,
                success: function(result){
                    var code = JSON.parse(result);
                    for (var i = 0;i<code.length;i++){
                        var style = i+1;
                        $('.form-style-list-'+style).show();

                        $('.form-style-list-'+style+' .form-display').append(code[i]);
                        $(".form-style-list-"+style+" #chatForm :input ").not(".edit-button, .add_field, .edit-input").prop("disabled", true);
                        $(".form-style-list-"+style+" .form-show").text(form_show['style'+style]);
                        $(".form-style-list-"+style+" .form-click").text(form_click['style'+style]);

                        if(data_row[7] == "click"){
                            $(".form-style-list-"+style+" .form-threshold").text(data_row[6]);

                            var progress = (form_click['style'+style]/data_row[6])*100;
                            $(".form-style-list-"+style+" .form-progress").text(progress.toFixed(2)+"%");
                        } else {
                            $(".form-style-list-"+style+" .form-threshold").text(data_row[6]+"%");

                            var threshold = (data_row[6]*data_row[5])/100;
                            var progress = (form_click['style'+style]/threshold)*100;
                            $(".form-style-list-"+style+" .form-progress").text(progress.toFixed(2)+"%");
                        }
                    }
                }
            })
        }
    });

    $('#example').on('click', '.btn-hide', function(){
        $(this).removeClass("btn-hide").addClass("btn-detail").text("DETAIL");
        $('.form-style-list-1, .form-style-list-2, .form-style-list-3').hide();
        $('div').remove('#form-container');
    });

    $('#example').on('click', '.btn-embed', function(){
        var test_id = $(this).attr('test-id');
        $.ajax({
            url: baseURL+"abtesting/test_embed_code/"+test_id,
            success: function(result){
                embed_code = result;

                Swal.fire({
                    title: 'Click Code To Copy',
                    width: 600,
                    html:
                        $('<p>')
                        .addClass('embedcode')
                        .text(embed_code),
                    type: 'info',
                })
            }
        });
    });

    $(document).on('click', 'p.embedcode', function(){
        copyToClipboard('.embedcode');
        Swal.fire({
            title: 'Code Coppied',
            width: 600,
            html:
                $('<p>')
                .addClass('embedcode2')
                .text(embed_code),
            type: 'success',
        });
    });

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }

<?php if($this->session->flashdata('notice_message')['message']){ ?>
    $.notify({
        icon: "done",
        message: "<?php echo $this->session->flashdata('notice_message')['message']?>"
    },{
        type: 'success',
        timer: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } ?>
});
</script>