<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container form-chat">
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Create New User</h3>
                </div>
                <div class="card-body">
                    <form id="chatForm" action="<?php echo base_url('user/add');?>" method="post" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Username</label>
                                    <input required type="text" class="form-control" id="username" name="username" minlength=6>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email</label>
                                    <input required type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Password</label>
                                    <input required type="password" class="form-control" id="password" name="password" minlength=6>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Confirm Password</label>
                                    <input required type="password" class="form-control" id="passwordConfirm" name="passwordConfirm">
                                </div>
                                <p class="checkResult"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">First Name</label>
                                    <input required type="text" class="form-control" id="firstName" name="firstName">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Last Name</label>
                                    <input required type="text" class="form-control" id="lastName" name="lastName">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">User Role</label>
                                    <br>
                                    <select required name="userRole" id="my-select">
                                        <option value="" data-placeholder>Select an option</option>
                                        <option value='3'>Staff Admin</option>
                                        <option value='4'>Staff</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="end-group"></div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Create New User">
                            <a class="btn btn-danger" href="<?php echo base_url("user/userlist") ?> ">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script>
$(document).ready(function() {
    <?php if($this->session->flashdata('notice_message')){ ?>
        console.log("<?php echo $this->session->flashdata('notice_message')['message']?>");
        $.notify({
        icon: "add_alert",
        message: "<?php echo $this->session->flashdata('notice_message')['message']?>"

    },{
        type: 'danger',
        delay: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } ?>
});
</script>
<script>
$(document).ready(function() {
    var isEqual = true;

    $('#password, #passwordConfirm').keyup(function(){
        var password = $("#password").val();
        var confirmPassword = $("#passwordConfirm").val();
        
        if (password == confirmPassword && password != ''){
            isEqual = true;
            $(".checkResult").text("Password Match").css('color', 'green');
        } else if(password != ''){
            isEqual = false;
            $(".checkResult").text("Password Not Match").css('color', 'red');
        }
    });

    $('input.btn').on('click', function(e){
        if(!isEqual){
            e.preventDefault();
            $.notify({
                icon: "add_alert",
                message: "Password Not Match"

            },{
                type: 'danger',
                delay: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        }
    });
});
</script>