<head>
<title>Form Chat</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/js/views/login/login.min.js') ?>"></script>
<link href="<?php echo base_url('assets/css/register-style.css') ?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
<div class="row" style="margin:0 !important;">
    <div class="col-md-4 offset-md-4">
        <div id="formWrapper">
            <div id="form">
                <div class="logo">
                    <h1 class="text-center head">Register</h1>
                </div>
                <form action="<?php echo base_url('user/add');?>" method="post" class="login-page__login-form">   
                    <div class="input-group form-item">
                        <p class="formLabel">Username</p>
                        <input type="username" name="username" id="username" class="form-style" autocomplete="off"/>
                        <p style="color:#8a8a8a;font-size:small;">Username Minimum 6 Charracters, Contain Only Alphabet, Number, Underscore ( _ ), or Hyphen ( - )</p>
                    </div>
                    <div class="input-group form-item">
                        <p class="formLabel">Email</p>
                        <input type="username" name="email" id="email" class="form-style" autocomplete="off"/>
                    </div>
                    <div class="input-group form-item">
                        <p class="formLabel">Password</p>
                        <input type="password" name="password" id="password" class="form-style" autocomplete="off"/>
                        <p style="color:#8a8a8a;font-size:small;">Password Minimum 6 Charracters</p>
                    </div>
                    <div class="input-group form-item">
                        <p class="formLabel">Confirm Password</p>
                        <input type="password" name="passwordConfirm" id="passwordConfirm" class="form-style" autocomplete="off"/>
                        <p class="checkResult"></p>
                    </div>
                    <div class="input-group form-item">
                        <p class="formLabel">First Name</p>
                        <input type="username" id="firstName" name="firstName" class="form-style" autocomplete="off"/>
                    </div>
                    <div class="input-group form-item">
                        <p class="formLabel">Last Name</p>
                        <input type="username" id="lastName" name="lastName" class="form-style" autocomplete="off"/>
                    </div>
                    <div class="input-group form-item">
                        <p class="pull-left"><a href="<?php echo base_url()?>"><small>Already Have Account?</small></a></p>
                        <input type="submit" class="login pull-right" value="Register">
                        <div class="clear-fix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>
$(document).ready(function() {
    $('.login-page__login-form').on('click', '.login', function(e){
        if($('#username').val() == '' || $('#email').val() == '' || $('#password').val() == '' || $('#passwordConfirm').val() == '' || $('#firstName').val() == '' || $('#lastName').val() == ''){
            e.preventDefault();
            $.notify({
                icon: "notification_important",
                message: "Form Tidak Boleh Kosong"

            },{
                type: 'danger',
                delay: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
        }
    });

    <?php if($this->session->flashdata('notice_message')['message']){ ?>
        $.notify({
        icon: "notification_important",
        message: "<?php echo $this->session->flashdata('notice_message')['message']?>"

    },{
        type: 'danger',
        timer: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } ?>

    var isEqual = true;

    $('#password, #passwordConfirm').keyup(function(){
        var password = $("#password").val();
        var confirmPassword = $("#passwordConfirm").val();
        
        if (password == confirmPassword && password != ''){
            isEqual = true;
            $(".checkResult").text("Password Match").css('color', 'green');
        } else if(password != ''){
            isEqual = false;
            $(".checkResult").text("Password Not Match").css('color', 'red');
        }
    });

    $('input.login').on('click', function(e){
        var username = $('#username').val();
        var password = $('#password').val();
        var regex = /^[\w\-]{6,}$/i;

        if (!regex.test(username)) {
            $.notify({
                icon: "error",
                message: "Username Minimum 6 Characters And Contain Alphabet, Number, Underscore, or Hyphen"
            },{
                type: 'danger',
                timer: 4000,
                z_index: 4031,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        }

        if (password.length < 6) {
            $.notify({
                icon: "error",
                message: "Password Minimum 6 Characters"
            },{
                type: 'danger',
                timer: 4000,
                z_index: 4031,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        }

        if(!isEqual){
            e.preventDefault();
            $.notify({
                icon: "notification_important",
                message: "Password Not Match"

            },{
                type: 'danger',
                delay: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        }
    });
});
</script>