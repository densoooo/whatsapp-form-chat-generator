<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">List User</h3>
                </div>
                <div class="card-body">
                    <div class="row" style="margin-bottom:30px;">
                        <div class="col-md-12">
                            <a class="btn btn-primary" style="float:right;" href="<?php echo base_url("user/createuser") ?> ">Create New User</a>
                        </div>
                    </div>
                    <table id="userTable" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>User Email</th>
                                <th>User Full Name</th>
                                <th>User Role</th>
                                <th>Registered Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach($listUser as $item) {
                                    // echo($item->form_title);
                                    if($item->role_id == 1){
                                        $role = 'Admin';
                                    } else if ($item->role_id == 2){
                                        $role = 'User';
                                    } else if ($item->role_id == 3){
                                        $role = 'Staff Admin';
                                    } else {
                                        $role = 'Staff';
                                    }
                                    echo(
                                        '<tr>
                                            <td>'. $item->user_id .'</td>
                                            <td>'. $item->user_name .'</td>
                                            <td>'. $item->user_email .'</td>
                                            <td>'. $item->user_fullname .'</td>
                                            <td>'. $role .'</td>
                                            <td>'. $item->registered_date .'</td>
                                            <td><a class="material-icons" href="javascript:void(0)" rel="tooltip" data-original-title="Edit">edit</a>
                                            <a class="material-icons btn-delete" href="javascript:void(0)" value="'.$item->user_id.'" rel="tooltip" data-original-title="Delete">delete</a></td>
                                        </tr>
                                        '
                                    );
                                }
                            ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Username</th>
                                <th>User Email</th>
                                <th>User Full Name</th>
                                <th>User Role</th>
                                <th>Registered Date</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  var baseURL = "<?php echo base_url("/"); ?>";
</script>
<script>
$(document).ready(function() {
    // $('.login-page__login-form').on('click', '.login', function(e){
    //     if($('#username').val() == '' || $('#password').val() == ''){
    //         e.preventDefault();
    //         $.notify({
    //             icon: "add_alert",
    //             message: "Username / Password Tidak Boleh Kosong"

    //         },{
    //             type: 'danger',
    //             delay: 3000,
    //             placement: {
    //                 from: 'top',
    //                 align: 'right'
    //             }
    //         });
    //     }
    // });

    <?php if($this->session->flashdata('notice_message')){ ?>
        console.log("<?php echo $this->session->flashdata('notice_message')['message']?>");
        $.notify({
        icon: "check_circle",
        message: "<?php echo $this->session->flashdata('notice_message')['message']?>"

    },{
        type: 'success',
        delay: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } ?>

    $('#userTable').DataTable({
        "columnDefs": [
            {
                "targets": 0,
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 0, "asc" ]],
        "dom" : "rtip"
    });

    $('#userTable').on('click', 'a.btn-delete', function(e){
        var user_id = $(this).attr('value');
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if(result.value){
                $.ajax({
                    url: baseURL+"user/delete/"+user_id,
                    success: function(result) {
                        if(result == "sukses"){
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                            location.reload();
                        }
                    }
                });
            }
        });
    });
});
</script>