<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-profile" style="text-align:left;">
                <div class="card-avatar">
                    <a href="javascript:void(0)">
                    <img class="img" src="https://i.etsystatic.com/11657219/r/il/bc2f35/1473280720/il_570xN.1473280720_grqn.jpg" />
                    </a>
                </div>
                <div class="card-body">
                    <h6 class="card-category text-gray"></h6>
                    <h4 class="card-title">User Profile</h4>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-md-3" style="font-size:18px;">
                            <p class="card-title">
                                Username
                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-title" style="font-size:18px;">
                                <?php echo $profileData->user_name;?>
                            </p>
                        </div>
                    </div>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-md-3" style="font-size:18px;">
                            <p class="card-title">
                                Role
                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-title" style="font-size:18px;">
                                <?php echo ($profileData->role_id == 1) ? "Admin" : ($profileData->role_id == 2) ? "User" : "Staff";?>
                            </p>
                        </div>
                    </div>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-md-3" style="font-size:18px;">
                            <p class="card-title">
                                Email
                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-title txt-email" style="font-size:18px;">
                                <?php echo $profileData->user_email;?>
                            </p>
                            <div class="col-md-12 field-email" style="display:none;">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email</label>
                                    <input required type="email" class="form-control" id="newEmail" name="newEmail">
                                </div>
                                <button class="btn btn-primary btn-sm">Save</button>
                                <button class="btn btn-danger btn-sm btn-email-cancel">Cancel</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-sm btn-email">Edit Email</button>
                        </div>
                    </div>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-md-3" style="font-size:18px;">
                            <p class="card-title">
                                Password
                            </p>
                        </div>
                        <div class="col-md-7">
                            <form class="password-form" style="display:none;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Your Old Password</label>
                                        <input required type="password" class="form-control" id="oldPassword" name="oldPassword">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">New Password</label>
                                        <input required type="password" class="form-control" id="newPassword" name="newPassword" minlength=6>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Confirm Password</label>
                                        <input required type="password" class="form-control" id="passwordConfirm" name="passwordConfirm">
                                    </div>
                                    <p class="checkResult"></p>
                                </div>
                                <button class="btn btn-primary btn-sm btn-pwd-save">Save</button>
                                <button class="btn btn-danger btn-sm btn-pwd-cancel">Cancel</button>
                            </form>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-sm btn-password">Edit Password</button>
                        </div>
                    </div>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-md-3" style="font-size:18px;">
                            <p class="card-title">
                                Full Name
                            </p>
                        </div>
                        <div class="col-md-7">
                            <p class="card-title" style="font-size:18px;">
                                <?php echo $profileData->user_fullname;?>
                            </p>
                        </div>
                    </div>
                    <!-- <a href="javascript:void(0)" class="btn btn-primary btn-round">Follow</a> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('/assets/js/core/jquery-3.3.1.min.js')?>"></script>
<script>
var baseURL = "<?php echo base_url('/')?>";
</script>
<script>
    var isEqual = true;

    $('.btn-email').on('click', function(){
        $('.field-email').show();
        $('#newEmail').val($('.txt-email').text()).focus();
        $('.txt-email, .btn-password').hide();
        $(this).hide();
    });

    $('.btn-email-cancel').on('click', function(){
        $('.field-email').hide();
        $('.btn-email, .txt-email, .btn-password').show();
    });

    $('.btn-password').on('click', function(){
        $('.password-form').show();
        $('.btn-email').hide();
        $(this).hide();
    });

    $('.btn-pwd-cancel').on('click', function(){
        $('.password-form').hide();
        $('.btn-password, .btn-email, .btn-email').show();
        $('#oldPassword, #newPassword, #passwordConfirm').val('');
    });

    $('#newPassword, #passwordConfirm').keyup(function(){
        var password = $("#newPassword").val();
        var confirmPassword = $("#passwordConfirm").val();
        
        if (password == confirmPassword && password != ''){
            isEqual = true;
            $(".checkResult").text("Password Match").css('color', 'green');
        } else if(password != ''){
            isEqual = false;
            $(".checkResult").text("Password Not Match").css('color', 'red');
        }
    });

    $('.btn-pwd-save').on('click', function(e){
        e.preventDefault();
        if(!isEqual){
            $.notify({
                icon: "add_alert",
                message: "Password Not Match"

            },{
                type: 'danger',
                delay: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
            return false;
        } else {
            $.ajax({
                url: baseURL+"user/checkPassword",
                type: "POST", 
                data: {"oldPassword": $("#oldPassword").val(), "newPassword": $("#newPassword").val()},
                success: function(result) {
                    if(result == "sukses"){
                        $.notify({
                            icon: "done",
                            message: "Success Change Password"
                        },{
                            type: 'success',
                            delay: 3000,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });

                        $('.password-form').hide();
                        $('.btn-password, .btn-email, .btn-email').show();
                        $('#oldPassword, #newPassword, #passwordConfirm').val('');
                    } else {
                        $.notify({
                            icon: "add_alert",
                            message: "Old Password Wrong"
                        },{
                            type: 'danger',
                            delay: 3000,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });
                    }
                }
            });
        }
    })

</script>