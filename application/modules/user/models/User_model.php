<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends Base_Model {

	function __construct() {
        parent::__construct();

        $this->setTable('user');
    }

    /**
     * Get all users data from database with filter
     *
     * @param   array  $options
     * @return  object
     */
    public function get_all_user($options) {
		return $this->get_data($options);
	}

    /**
     * Insert user data into database
     *
     * @param   array  $options
     * @return  object
     */
    function add_user($params) {
        return $this->write($params);
    }

    /**
     * Validate data for login process
     *
     * @param   string  $email
     * @param   string  $password
     * @return  array
     */
	public function loginProcess($username_email, $password){
        $this->_read_db->where("user_email", $username_email);
		$this->_read_db->where("user_password", $password);

		$user = $this->read();        

		if ($user->num_rows() > 0){
			$user 			= $user->row();
			$ret['uid'] 	= $user->user_id;
            $ret['nama'] 	= $user->user_name;
            $ret['role_id'] 	= $user->role_id;
            $ret['user_status'] = $user->user_status;
            $ret['group_id'] = $user->group_id;
			return $ret;
        }
        
        $this->_read_db->flush_cache();
        $this->_read_db->where("user_password", $password);
        $this->_read_db->where("user_name", $username_email);
        $user = $this->read();
        
        if($user->num_rows() > 0){
            $user 			= $user->row();
			$ret['uid'] 	= $user->user_id;
            $ret['nama'] 	= $user->user_name;
            $ret['role_id'] 	= $user->role_id;
            $ret['user_status'] = $user->user_status;
            $ret['group_id'] = $user->group_id;
			return $ret;
        }
        
        return false;
    }
    
    public function get_user_list($lead_id){
        $this->_read_db->select("user_id");
        $this->_read_db->select("user_email");
        $this->_read_db->select("user_name");
        $this->_read_db->select("user_fullname");
        $this->_read_db->select("user_status");
        $this->_read_db->select("role_id");
        $this->_read_db->select("registered_date");
        $this->_read_db->where("lead_id", $lead_id);

        $data = $this->read()->result();

        return $data;
    }

    public function get_profile($user_id){
        $this->_read_db->where("user_id", $user_id);

		return $this->read()->row();
    }

    public function activate_user($email, $param){
        $this->_write_db->where('user_email', $email);

        return $this->update($param);
    }

    public function check_password($password){
        $user_id = $this->session->userdata('uid');

        $this->_read_db->where("user_id", $user_id);
        $this->_read_db->where("user_password", $password);

        $user = $this->read()->num_rows();
        
        return $user;
    }

    public function update_password($newPassword){
        $user_id = $this->session->userdata('uid');

        $this->_write_db->where("user_id", $user_id);

        return $this->update(array("user_password"=>$newPassword));
    }

    public function delete_user($user_id){
        $lead_id = $this->session->userdata('uid');

        return $this->delete(array("lead_id" => $lead_id, "user_id" => $user_id));
    }
}