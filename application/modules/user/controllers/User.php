<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct(){

		$this->load->library('email');

		$this->load->model('user/user_model', 'user');
		$this->load->model('group/group_model', 'group');
		$this->load->model('response/followup_model', 'followup');
	}
	
	public function register(){
		if(!$this->session->userdata('nama')){
			$this->load->view('user/register');
			$this->load->view('elements/script-only');
		} else {
			redirect(base_url());
		}
	}

    public function add() {
		if(!empty($this->input->post('email'))){
			// $uploads = modules::run('attachment/upload', false);
			// Form Validation
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('username', 'username', 'required');
			if($this->form_validation->run()){
				
				if($this->session->userdata('role_id') == 1){

					$data = [
						'user_email' 		=> $this->input->post('email', TRUE),
						'user_password' 	=> md5($this->input->post('password', TRUE)),
						'user_name'			=> $this->input->post('username', TRUE),
						'user_image'		=> '',
						'user_fullname'		=> $this->input->post('firstName', TRUE). " " .$this->input->post('lastName', TRUE),
						'user_status'		=> '1',
						'registered_date'	=> date('Y-m-d H:i:s'),
						'role_id'			=> 2,
						'lead_id'		    => $this->session->userdata('uid'),
						'group_id'			=> $this->input->post('groupName', TRUE)
					];
				} else if($this->session->userdata('role_id') > 1){
					
					$data = [
						'user_email' 		=> $this->input->post('email', TRUE),
						'user_password' 	=> md5($this->input->post('password', TRUE)),
						'user_name'			=> $this->input->post('username', TRUE),
						'user_image'		=> '',
						'user_fullname'		=> $this->input->post('firstName', TRUE). " " .$this->input->post('lastName', TRUE),
						'user_status'		=> '1',
						'registered_date'	=> date('Y-m-d H:i:s'),
						'role_id'			=> $this->input->post('userRole', TRUE),
						'lead_id'		    => $this->session->userdata('uid'),
						'group_id'			=> $this->session->userdata('group_id')
					];
				} else {
					$data = [
						'user_email' 		=> $this->input->post('email', TRUE),
						'user_password' 	=> md5($this->input->post('password', TRUE)),
						'user_name'			=> $this->input->post('username', TRUE),
						'user_image'		=> '',
						'user_fullname'		=> $this->input->post('firstName', TRUE). " " .$this->input->post('lastName', TRUE),
						'user_status'		=> '0',
						'registered_date'	=> date('Y-m-d H:i:s'),
						'role_id'			=> 2,
						'lead_id'		    => 1,
						'group_id'			=> 999999
					];

					$input = $this->createnew($data);
					if($input['status']){

						$this->session->set_flashdata('notice_message', array(
							"type"			=> "info",
							"icon"			=> "fa-info",
							"icon-text"		=> "Info",
							"message"		=> "Success Create Account, Check Your Email To Activate Your Account"
						));

						$this->sendactivation($data);

						redirect();
					}else{
						redirect('register');
					}
					return;
				}

				$input = $this->createnew($data);
				if($input['status']){

					$this->session->set_flashdata('notice_message', array(
					"type"			=> "info",
					"icon"			=> "fa-info",
					"icon-text"		=> "Info",
					"message"		=> "Success Add User"
				));
					redirect('user/userlist', 'refresh');
				}else{
					redirect('user/createuser');
				}
			}
		}
    }

    public function createnew($userData, $role_ids = []){
		if($userData['user_email']){
			$dataEmailUser = $this->user->get_all_user(array('where'=>array('user_email'=>$userData['user_email'])));
			$dataNameUser = $this->user->get_all_user(array('where'=>array('user_name'=>$userData['user_name'])));
			if(!empty($dataEmailUser)){

				$this->session->set_flashdata('notice_message', array(
					"type"			=> "info",
					"icon"			=> "fa-info",
					"icon-text"		=> "Info",
					"message"		=> 'Failed Add User! Email '.$dataEmailUser[0]->user_email.' Already Exist, Please Use Another Email'
				));
				
				return array(
					'status'	=> false,
					'message'	=> 'Failed Add User! Email '.$dataEmailUser[0]->user_email.' Already Exist, Please Enter Another Email',
					'user_id'	=> $dataEmailUser[0]->user_id
				);
			} else if (!empty($dataNameUser)){
				$this->session->set_flashdata('notice_message', array(
					"type"			=> "info",
					"icon"			=> "fa-info",
					"icon-text"		=> "Info",
					"message"		=> 'Failed Add User! Username '.$dataNameUser[0]->user_name.' Already Exist, Please Use Another Username'
				));
				
				return array(
					'status'	=> false,
					'message'	=> 'Failed Add User! Username '.$dataNameUser[0]->user_name.' Already Exist, Please Enter Another Username',
					'user_id'	=> $dataNameUser[0]->user_id
				);
			}
		}

		$user_id = $this->user->add_user($userData);

		return array(
			'status'	=> true,
			'message'	=> 'Success Add New User',
			'user_id'	=> $user_id
		);
	}

	public function createuser(){
		if($this->session->userdata('nama')){
			$groupData = $this->group->get_all_group();

			if($groupData){
				$data['groupData'] = $groupData;
				$data['recent_followup'] = $this->followup->recent_followup();
				
				$this->load->view('elements/sidebar');
				$this->load->view('elements/navbar', $data);
				$this->load->view('user/create_user', $data);
				$this->load->view('elements/footer');
			}
		} else {
			redirect(base_url());
		}
	}

	public function userlist(){
		if($this->session->userdata('nama')){
			$lead_id = $this->session->userdata('uid');

			if($lead_id){
				$listUser = $this->user->get_user_list($lead_id);
				
				if($listUser){
					$data = array('listUser' => $listUser);
				} else {
					$data = array('listUser' => []);
				}
				$data['recent_followup'] = $this->followup->recent_followup();
	
				$this->load->view('elements/sidebar');
				$this->load->view('elements/navbar', $data);
				$this->load->view('user/list_user', $data);
				$this->load->view('elements/footer');
			}
		} else {
			redirect(base_url());
		}
	}

	public function profile(){
		if($this->session->userdata('nama')){
			$user_id = $this->session->userdata('uid');

			if($user_id){
				$data = array('profileData' => $this->user->get_profile($user_id));
				$data['recent_followup'] = $this->followup->recent_followup();
	
				$this->load->view('elements/sidebar');
				$this->load->view('elements/navbar', $data);
				$this->load->view('user/profile', $data);
				$this->load->view('elements/footer');
			}
		} else {
			redirect(base_url());
		}
	}

	public function activate($param){
		$password="iclFORMCHAT2019";
		$decrypted_string=openssl_decrypt($param,"AES-128-ECB",$password);

		$data = [
			'user_status' 		=> 1
		];

		$result = $this->user->activate_user($decrypted_string, $data);

		if($result){
			$this->session->set_flashdata('notice_message', array(
				"type"			=> "info",
				"icon"			=> "fa-info",
				"icon-text"		=> "Info",
				"message"		=> "Activation Success, You Can Now Loggin Using Your Account"
			));

			redirect();
		}
	}

	public function checkPassword(){
		if($this->session->userdata('nama')){
			$oldPassword = md5($this->input->post('oldPassword', TRUE));
			$newPassword = md5($this->input->post('newPassword', TRUE));

			$result = $this->user->check_password($oldPassword);

			if($result == 1){
				$this->user->update_password($newPassword);
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			redirect(base_url());
		}
	}

	public function delete($user_id){
		if($this->session->userdata('nama')){
			$delete = $this->user->delete_user($user_id);
			if($delete){
				echo "sukses";
			} else {
				echo "gagal";
			}
		} else {
			redirect(base_url());
		}	
	}

	public function sendactivation($data){

		$email = $data['user_email'];
		$name = $data['user_fullname'];

		//encryption method
		$password="iclFORMCHAT2019";
		$encrypted_string=openssl_encrypt($email,"AES-128-ECB",$password);
		$decrypted_string=openssl_decrypt($encrypted_string,"AES-128-ECB",$password);

		$url = base_url('user/activate').'/'.$encrypted_string;

		$subject = 'Account Activation';

		// Get full html:
		$body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
		<html xmlns='http://www.w3.org/1999/xhtml'>
		  
		  <head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
			<meta name='viewport' content='width=device-width, initial-scale=1.0' />
			<title>Revue</title>
			<style type='text/css'>
			  #outlook a {padding:0;}
			  body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;} 
			  .ExternalClass {width:100%;}
			  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div, .ExternalClass blockquote {line-height: 100%;}
			  .ExternalClass p, .ExternalClass blockquote {margin-bottom: 0; margin: 0;}
			  #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
			  
			  img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
			  a img {border:none;} 
			  .image_fix {display:block;}
		  
			  p {margin: 1em 0;}
		  
			  h1, h2, h3, h4, h5, h6 {color: black !important;}
			  h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: black;}
			  h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {color: black;}
			  h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {color: black;}
		  
			  table td {border-collapse: collapse;}
			  table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		  
			  a {color: #3498db;}
			  p.domain a{color: black;}
		  
			  hr {border: 0; background-color: #d8d8d8; margin: 0; margin-bottom: 0; height: 1px;}
		  
			  @media (max-device-width: 667px) {
				a[href^='tel'], a[href^='sms'] {
				  text-decoration: none;
				  color: blue;
				  pointer-events: none;
				  cursor: default;
				}
		  
				.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
				  text-decoration: default;
				  color: orange !important;
				  pointer-events: auto;
				  cursor: default;
				}
		  
				h1[class='profile-name'], h1[class='profile-name'] a {
				  font-size: 32px !important;
				  line-height: 38px !important;
				  margin-bottom: 14px !important;
				}
		  
				span[class='issue-date'], span[class='issue-date'] a {
				  font-size: 14px !important;
				  line-height: 22px !important;
				}
		  
				td[class='description-before'] {
				  padding-bottom: 28px !important;
				}
				td[class='description'] {
				  padding-bottom: 14px !important;
				}
				td[class='description'] span, span[class='item-text'], span[class='item-text'] span {
				  font-size: 16px !important;
				  line-height: 24px !important;
				}
		  
				span[class='item-link-title'] {
				  font-size: 18px !important;
				  line-height: 24px !important;
				}
		  
				span[class='item-header'] {
				  font-size: 22px !important;
				}
		  
				span[class='item-link-description'], span[class='item-link-description'] span {
				  font-size: 14px !important;
				  line-height: 22px !important;
				}
		  
				.link-image {
				  width: 84px !important;
				  height: 84px !important;
				}
		  
				.link-image img {
				  max-width: 100% !important;
				  max-height: 100% !important;
				}
		  
			  }
		  
			  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
				a[href^='tel'], a[href^='sms'] {
				  text-decoration: none;
				  color: blue;
				  pointer-events: none;
				  cursor: default;
				}
		  
				.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {
				  text-decoration: default;
				  color: orange !important;
				  pointer-events: auto;
				  cursor: default;
				}
			  }
			</style>
			<!--[if gte mso 9]>
			  <style type='text/css'>
				#contentTable {
				  width: 600px;
				}
			  </style>
			<![endif]-->
		  </head>
		  
		  <body style='width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;'>
			<table cellpadding='0' cellspacing='0' border='0' id='backgroundTable' style='margin:0; padding:0; width:100% !important; line-height: 100% !important; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;'
			width='100%'>
			  <tr>
				<td width='10' valign='top'>&nbsp;</td>
				<td valign='top' align='center'>
				  <!--[if (gte mso 9)|(IE)]>
					<table width='600' align='center' cellpadding='0' cellspacing='0' border='0' style='background-color: #FFF; border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;'>
					  <tr>
						<td>
						<![endif]-->
						<table cellpadding='0' cellspacing='0' border='0' align='center' style='width: 100%; max-width: 600px; background-color: #FFF; border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;'
						id='contentTable'>
						  <tr>
							<td width='600' valign='top' align='center' style='border-collapse:collapse;'>
							  <table align='center' border='0' cellpadding='0' cellspacing='0' style='border: 1px solid #E0E4E8;'
							  width='100%'>
								<tr>
								  <td align='center' valign='top'>
									<img alt='Revue' width='140' height='140' style='vertical-align: middle;object-fit: contain;' src='https://i.imgur.com/p6vymLp.png'/>
								  </td>
								</tr>
								<tr>
								  <td align='left' style='padding: 0px 56px 28px 56px;' valign='top'>
									<div style='font-family: 'lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 28px;font-size: 18px; color: #333;font-weight:bold;'>Hey $name!</div>
								  </td>
								</tr>
								<tr>
								  <td align='left' style='padding: 0 56px 28px 56px;' valign='top'>
									<div style='font-family: 'lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 28px;font-size: 18px; color: #333;'>Please click the following link to confirm that <strong>$email</strong> is
									  your email address to activate your account</div>
								  </td>
								</tr>
								<tr>
								  <td align='left' style='padding: 0 56px;' valign='top'>
									<div>
									  <!--[if mso]>
										<v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word'
										href='#'
										style='height:44px;v-text-anchor:middle;width:250px;' arcsize='114%' stroke='f'
										fillcolor='#E15718'>
										  <w:anchorlock/>
										<![endif]-->
										<a style='background-color:#E15718;border-radius:50px;color:#ffffff;display:inline-block;font-family: &#39;lato&#39;, &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif;font-size:18px;line-height:44px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;'
										href='$url'>Confirm email address</a>
										<!--[if mso]>
										</v:roundrect>
									  <![endif]-->
									</div>
								  </td>
								  <tr>
									<td align='left' style='padding: 28px 56px 28px 56px;' valign='top'></td>
								  </tr>
								</tr>
							  </table>
							  <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>
								<tr>
								  <td align='center' style='padding: 30px 56px 28px 56px;' valign='middle'>
		<span style='font-family: 'lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 28px;font-size: 16px; color: #A7ADB5; vertical-align: middle;'>If this email doesn't make any sense, please ignore it</span>
		
								  </td>
								</tr>
							  </table>
							</td>
						  </tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
						</td>
					  </tr>
					</table>
				  <![endif]-->
				</td>
				<td width='10' valign='top'>&nbsp;</td>
			  </tr>
			</table>
			
		  </body>
		
		</html>";
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
			->from('buatweb30@gmail.com')
			->to($email)
			->subject($subject)
			->message($body)
			->send();

		return;
	}
}