<head>
<title>Form Chat</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/js/views/login/login.min.js') ?>"></script>
<link href="<?php echo base_url('assets/css/login-style.min.css') ?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
</head>

<!-- <script src="https://code.jquery.com/jquery-2.1.0.min.js" ></script> -->
<body>
<div id="formWrapper">
    <div id="form">
        <div class="logo">
            <h1 class="text-center head">Log In</h1>
        </div>
        <form action="<?php echo base_url("auth/login"); ?>" method="post" class="login-page__login-form">   
            <div class="input-group form-item">
                <p class="formLabel">Username / Email</p>
                <input type="username" name="username" id="username" class="form-style" autocomplete="off"/>
            </div>
            <div class="input-group form-item">
                <p class="formLabel">Password</p>
                <input type="password" name="password" id="password" class="form-style" />
                <!-- <div class="pw-view"><i class="fa fa-eye"></i></div> -->
                <!-- <p><a href="#" ><small>Forgot Password ?</small></a></p>	 -->
            </div>
            <div class="input-group form-item">
                <p class="pull-left"><a href="<?php echo base_url('register')?>"><small>Don't have an account?</small></a></p>
                <input type="submit" class="login pull-right" value="Log In">
                <div class="clear-fix"></div>
            </div>
        </form>
    </div>
</div>
</body>
</html>

<script>
$(document).ready(function() {
    $('.login-page__login-form').on('click', '.login', function(e){
        if($('#username').val() == '' || $('#password').val() == ''){
            e.preventDefault();
            $.notify({
                icon: "notification_important",
                message: "Username / Password Tidak Boleh Kosong"

            },{
                type: 'danger',
                delay: 3000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
        }
    });

    <?php if($this->session->flashdata('LoginNotif')){ ?>
    $.notify({
        icon: "notification_important",
        message: "<?php echo $this->session->flashdata('LoginNotif')?>"
    },{
        type: 'danger',
        timer: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } else if($this->session->flashdata('notice_message')['message']) {?>
    $.notify({
        icon: "done",
        message: "<?php echo $this->session->flashdata('notice_message')['message']?>"
    },{
        type: 'SUCCESS',
        timer: 4000,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
    <?php } ?>
});
</script>