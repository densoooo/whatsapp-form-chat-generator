<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {
    
    public function __construct(){
        parent::__construct();

        $this->load->model('user/user_model', 'user');
    }

    public function index() {
		if(!$this->session->userdata('nama')){
			$this->load->view('login');
			$this->load->view('elements/script-only');
		} else {
			redirect(base_url('dashboard'));
		}
    }

    public function login(){
        if ($this->input->post('password')){

			$username_email = trim($this->input->post('username', TRUE));
			$password		= md5($this->input->post('password', TRUE));
            $check 			= $this->user->loginProcess($username_email, $password);

			if($check != false){
				$array = array(
					'uid'		=> $check['uid'],
					'nama'		=> $check['nama'],
					'role_id' 	=> $check['role_id'],
					'user_status'=> $check['user_status'],
					'group_id'=> $check['group_id']
				);

				$this->session->set_userdata( $array );
				
				if ( $this->session->userdata('user_status') != '1' ) {
					$this->session->unset_userdata("uid");
					$this->session->unset_userdata("nama");
					$this->session->unset_userdata("role_id");
					$this->session->unset_userdata("user_status");
					$this->session->unset_userdata("group_id");
					$this->session->set_flashdata('failed', "Your Account Not Yet Activated, Check Your Email To Activate Your Account");
					$this->session->set_flashdata("LoginNotif","Your Account Not Yet Activated, Check Your Email To Activate Your Account");
			
					redirect(base_url());
				}else{
					redirect('dashboard');
				}
			} else {
				$this->session->set_flashdata('failed', "Invalid Username Or Password");
				$this->session->set_flashdata('LoginNotif', 'Invalid Username Or Password');
				redirect(base_url());
			}
		} else {
				redirect(base_url());
		}
    }

    public function logout(){
        $this->session->unset_userdata("uid");
		$this->session->unset_userdata("nama");
		$this->session->unset_userdata("role_id");
		$this->session->unset_userdata("origin_id");
		$this->session->set_flashdata('success', 'Already Logout');

        redirect(base_url());
    }
}