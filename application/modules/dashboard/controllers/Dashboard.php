<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
    public function __construct(){
        parent::__construct();

        $this->load->model('user/user_model', 'user');
        $this->load->model('campaign/campaign_model', 'campaign');
        $this->load->model('response/response_model', 'response');
        $this->load->model('response/followup_model', 'followup');
        $this->load->model('group/group_model', 'group');
        $this->load->model('form/form_model', 'form');
        $this->load->model('abtesting/abtesting_model', 'abtesting');
    }

    public function index() {
        if($this->session->userdata('nama')){

            $dashboard_data = $this->get_data();
            $data['recent_followup'] = $this->followup->recent_followup();

			$this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('dashboard/index', $dashboard_data);
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		} 
    }

    public function get_data(){
        $uid = $this->session->userdata('uid');

        $user_data = $this->user->get_user_list($uid);
        $data['user_count'] = sizeof($user_data);
        $data['user_active'] = 0;
        $data['user_inactive'] = 0;
        
        foreach($user_data as $row){
            if($row->user_status == 1){
                $data['user_active'] += 1;
            } else {
                $data['user_inactive'] += 1;
            }
        }

        $campaign_data = $this->campaign->get_all_campaign(array('where'=>array('user_id'=>$uid)));
        $data['campaign_count'] = sizeof($campaign_data);
        $data['campaign_active'] = 0;
        $data['campaign_inactive'] = 0;

        $form_data = $this->form->get_form_statistic($uid);
        $data['form_count'] = sizeof($form_data);
        $data['form_show'] = 0;
        $data['form_click'] = 0;

        foreach($form_data as $row){
            $data['form_show'] += $row->form_show;
            $data['form_click'] += $row->form_click;
        }

        $response_data = $this->response->get_response_data();
        $data['response_count'] = sizeof($response_data);
        $data['response_followup'] = 0;
        $data['response_next'] = 0;
        $data['response_closing'] = 0;

        foreach($response_data as $row){
            if($row->status_followup == 0){
                $data['response_followup'] += 1;
            } else if($row->status_followup == 1){
                $data['response_next'] += 1;
            } else if($row->status_followup == 2){
                $data['response_closing'] += 1;
            }
        }

        $testing_data = $this->abtesting->get_test_number();
        $data['testing_count'] = sizeof($testing_data);
        $data['inactive'] = 0;
        $data['active'] = 0;
        $data['finish'] = 0;

        foreach($testing_data as $row){
            if($row->testing_status == 0){
                $data['inactive'] += 1;
            }else if($row->testing_status == 1){
                $data['active'] += 1;
            }else if($row->testing_status == 2){
                $data['finish'] += 1;
            }
        }

        return $data;
    }
}