<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">show_chart</i>
                        </div>
                        <h3 class="card-title">Statistic</h3>
                    </div>
                    <div class="card-footer">
                        <div class="row" style="width:100%;text-align: center;">
                            <div class="col-lg-6 col-md-6">
                                <h3>Total Staff</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $user_count ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Campaign Number</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $campaign_count ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Form Number</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $form_count ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Total Form View</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $form_show ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Total Form Click</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $form_count ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-info card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">reply</i>
                        </div>
                        <p class="card-category">Response Number</p>
                        <h3 class="card-title"><?php echo $response_count ?></h3>
                    </div>
                    <div class="card-footer">
                        <div class="row" style="width:100%;text-align: center;">
                            <div class="col-lg-6 col-md-6">
                                <h3><a href="<?php echo base_url('response/responselist?status=0'); ?>">Followup</a></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $response_followup ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><a href="<?php echo base_url('response/responselist?status=1'); ?>">Next Followup</a></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $response_next ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><a href="<?php echo base_url('response/responselist?status=2'); ?>">Closing</a></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $response_closing ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-rose card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">layers</i>
                        </div>
                        <p class="card-category">Testing Number</p>
                        <h3 class="card-title"><?php echo $testing_count ?></h3>
                    </div>
                    <div class="card-footer">
                        <div class="row" style="width:100%;text-align: center;">
                            <div class="col-lg-6 col-md-6">
                                <h3>Inactive Testing</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $inactive ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Active Testing</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $active ?></h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3>Finished Testing</h3>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h3><?php echo $finish ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>