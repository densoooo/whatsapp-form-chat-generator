<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Edit Campaign</h3>
                </div>
                <div class="card-body">
                    <form id="campaignForm" action="<?php echo base_url('campaign/save');?>" method="post" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Campaign Name</label>
                                    <input required type="text" class="form-control" id="campaignname" name="campaignname" value="<?php echo $campaign->campaign_name; ?>">
                                </div>
                            </div>
                        </div>
                        <input type="text" class="form-control" id="campaignID" name="campaignID" value="<?php echo $campaign->campaign_id; ?>" style="display:none;">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Save">
                            <a class="btn btn-danger" href="<?php echo base_url("campaign") ?> ">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->