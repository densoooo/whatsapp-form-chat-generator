<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign_model extends Base_Model {

	function __construct() {
        parent::__construct();

    $this->setTable('campaign_data');
    }

    /**
     * Insert campaign data into database
     *
     * @param   array  $options
     * @return  object
     */
    function add_campaign($params) {
        return $this->write($params);
    }

    /**
     * Get all campaign data from database
     *
     * @return  object
     */
    public function get_all_campaign($param) {
        $campaign = $this->get_data($param);

        if(!empty($campaign)){
            foreach($campaign as $r) {
                $data[] = array(
                    $r->campaign_id,
                    '<a href="'.base_url('form?campaign_group='.$r->campaign_name).'">'.$r->campaign_name.'</a>',
                    $r->campaign_created_date,
                    '<td><a class="material-icons" href="'.base_url('campaign/edit/'.$r->campaign_id).'" rel="tooltip" data-original-title="Edit">edit</a>
                    <a class="material-icons btn-delete" href="javascript:void(0)" value="'.$r->campaign_id.'" rel="tooltip" data-original-title="Delete">delete</a></td>'
                );
            }
        } else {
            $data = null;
        }

        return $data;
    }
    
    public function check_campaign($campaign_id){
        $user_id = $this->session->userdata('uid');

        $this->_read_db->where("user_id", $user_id);
        $this->_read_db->where("campaign_id", $campaign_id);

        $user = $this->read()->num_rows();
        
        return $user;
    }

    public function get_campaign($campaign_id){
        $campaign = $this->get_data($campaign_id);

        return $campaign[0];
    }

    public function save_edit($campaign_id, $campaign_name){
        $this->_write_db->where("campaign_id", $campaign_id);

        return $this->update(array("campaign_name"=>$campaign_name,'campaign_edited_date'=>date('Y-m-d H:i:s')));
    }

}