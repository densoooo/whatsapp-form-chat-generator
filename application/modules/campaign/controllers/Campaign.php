<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign extends MX_Controller {

	public function __construct(){

        $this->load->model('campaign/campaign_model', 'campaign');
        $this->load->model('response/followup_model', 'followup');
        
    }

    public function index(){
        if($this->session->userdata('nama')){
            $data['recent_followup'] = $this->followup->recent_followup();

			$this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('campaign/campaign_list');
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function create(){
        if($this->session->userdata('nama')){
            $data['recent_followup'] = $this->followup->recent_followup();

            $this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('campaign/campaign_create');
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function add(){
        if($this->session->userdata('nama')){
            $this->form_validation->set_rules('campaignname', 'campaignname', 'required');
        
            if($this->form_validation->run()){
    
                $data = [
                    'user_id' 		            => $this->session->userdata('uid'),
                    'campaign_name' 	        => $this->input->post('campaignname', TRUE),
                    'campaign_created_date'    => date('Y-m-d H:i:s'),
                    'campaign_edited_date'     => date('Y-m-d H:i:s')
                ];
    
                $input = $this->campaign->add_campaign($data);
    
                if($input){
                    redirect(base_url('campaign'));
                }
            }
		} else {
			redirect(base_url());
		}
    }

    public function campaigndata(){
        if($this->session->userdata('nama')){
            $param = $this->session->userdata('uid');
            $campaignGroup = $this->campaign->get_all_campaign(array('where'=>array('user_id'=>$param)));
    
            if($campaignGroup){
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($campaignGroup),
                    "recordsFiltered" => sizeof($campaignGroup),
                    "data" => $campaignGroup);
    
            } else {
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($campaignGroup),
                    "recordsFiltered" => sizeof($campaignGroup),
                    "data" => []);
            }

            echo json_encode($output);

		} else {
			redirect(base_url());
		}
    }

    public function edit($campaign_id){
        if($this->session->userdata('nama')){
            $isOwned = $this->campaign->check_campaign($campaign_id);
            if($isOwned){
                $data['campaign'] = $this->campaign->get_campaign(array('where'=>array('campaign_id'=>$campaign_id)));
                $data['recent_followup'] = $this->followup->recent_followup();

                $this->load->view('elements/sidebar');
                $this->load->view('elements/navbar', $data);
                $this->load->view('campaign/campaign_edit', $data);
                $this->load->view('elements/footer');
            } else {
                redirect(base_url());
            }
        } else {
			redirect(base_url());
		}
    }

    public function save(){
        if($this->session->userdata('nama')){
            $campaign_name = $this->input->post('campaignname', TRUE);
            $campaign_id = $this->input->post('campaignID', TRUE);

            $this->campaign->save_edit($campaign_id, $campaign_name);

            //add flash data sukses

            redirect(base_url('campaign'));
        } else {
			redirect(base_url());
		}  
    }
}