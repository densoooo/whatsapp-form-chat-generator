<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends MX_Controller {

	public function __construct(){

        $this->load->model('group/group_model', 'group');
        $this->load->model('response/followup_model', 'followup');
        
    }

    public function index(){
        if($this->session->userdata('nama')){
            $data['recent_followup'] = $this->followup->recent_followup();

			$this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('group/group_list');
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function create(){
        if($this->session->userdata('nama')){
            $data['recent_followup'] = $this->followup->recent_followup();

            $this->load->view('elements/sidebar');
            $this->load->view('elements/navbar', $data);
            $this->load->view('group/group_create');
            $this->load->view('elements/footer');
		} else {
			redirect(base_url());
		}
    }

    public function add(){
        if($this->session->userdata('nama')){
			$this->form_validation->set_rules('groupname', 'groupname', 'required');
            
            if($this->form_validation->run()){

                $data = [
                    'admin_id' 		        => $this->session->userdata('uid'),
                    'group_name' 	        => $this->input->post('groupname', TRUE),
                    'group_created_date'    => date('Y-m-d H:i:s'),
                    'group_edited_date'     => date('Y-m-d H:i:s')
                ];

                $input = $this->group->add_group($data);

                if($input){
                    redirect(base_url('group'));
                }
            }
		} else {
			redirect(base_url());
		}
    }

    public function groupdata(){
        if($this->session->userdata('nama')){
            $dataGroup = $this->group->get_all_group();

            if($dataGroup){
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($dataGroup),
                    "recordsFiltered" => sizeof($dataGroup),
                    "data" => $dataGroup);
            } else {
                $output = array(
                    "draw" => $this->input->post('draw', TRUE),
                    "recordsTotal" => sizeof($dataGroup),
                    "recordsFiltered" => sizeof($dataGroup),
                    "data" => []);
            }

            echo json_encode($output);
            
		} else {
			redirect(base_url());
		}
    }
}