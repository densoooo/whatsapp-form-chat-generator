<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">List Group</h3>
                </div>
                <div class="card-body">
                    <div class="row" style="margin-bottom:30px;">
                        <div class="col-md-12">
                            <a class="btn btn-primary" style="float:right;" href="<?php echo base_url("group/create") ?> ">Create New Group</a>
                        </div>
                    </div>
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Admin ID</th>
                                <th>Group Name</th>
                                <th>Created Date</th>
                                <th>Edited Date</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Admin ID</th>
                                <th>Group Name</th>
                                <th>Created Date</th>
                                <th>Edited Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('#example').DataTable({
        destroy: true,
        "ajax": "<?php echo base_url('group/groupdata'); ?>",
        "columnDefs": [
            {
                "targets": 0,
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 0, "asc" ]]
    });
</script>