<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary" style="margin-bottom:30px;">
                    <h3 class="card-title">Create New Group</h3>
                </div>
                <div class="card-body">
                    <form id="groupForm" action="<?php echo base_url('group/add');?>" method="post" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Group Name</label>
                                    <input required type="text" class="form-control" id="groupname" name="groupname" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Create New Group">
                            <a class="btn btn-danger" href="<?php echo base_url("group") ?> ">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->