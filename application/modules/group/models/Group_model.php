<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_model extends Base_Model {

	function __construct() {
        parent::__construct();

    $this->setTable('group_data');
    }

    /**
     * Insert group data into database
     *
     * @param   array  $options
     * @return  object
     */
    function add_group($params) {
        return $this->write($params);
    }

    /**
     * Get all group data from database
     *
     * @return  object
     */
    public function get_all_group() {
        $group = $this->get_data();

        if(!empty($group)){
            foreach($group as $r) {
                $data[] = array(
                    $r->group_id,
                    $r->admin_id,
                    $r->group_name,
                    $r->group_created_date,
                    $r->group_edited_date,
                );
            }
        } else {
            $data = null;
        }

        return $data;
	}

}