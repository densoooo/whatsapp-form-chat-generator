// Anonymous "self-invoking" function
(function() {
    // Load the script
    
    var script = document.createElement("SCRIPT");
    script.src = baseURL+'assets/js/core/jquery-3.3.1.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Start polling...
    checkReady(function($) {
        $(function() {
            
            $.getMultiScripts = function(arr, path) {
                var _arr = $.map(arr, function(scr) {
                    return $.getScript( (path||"") + scr );
                });
            
                _arr.push($.Deferred(function( deferred ){
                    $( deferred.resolve );
                }));
            
                return $.when.apply($, _arr);
            }
        
            var script_arr = [
 
                'assets/js/plugins/jquery-migrate-1.4.1.min.js',
            ];
            
            $.getMultiScripts(script_arr, baseURL).done(function() {
                // all scripts loaded
                $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
                    address = data['ip'];   
                }); 

                // $('body').bootstrapMaterialDesign();

                // var checkReady = function(callback) {
                //     if (typeof bootstrapMaterialDesign != 'undefined' && $.isFunction(bootstrapMaterialDesign)) {
                //         $('body').bootstrapMaterialDesign();
                //     } else {
                //         window.setTimeout(function() { checkReady(callback); }, 20);
                //     }
                // };
        
                var parentUrl = window.location.href;  
                var browser = Object.keys(jQuery.browser)[0];

                $("<link/>", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: baseURL+"/assets/css/form-style.css"
                 }).appendTo("head");

                //  $("<link/>", {
                //     rel: "stylesheet",
                //     type: "text/css",
                //     href: "http://formchat.dev.cc/assets/css/material-dashboard.css"
                //  }).appendTo("head");
                 
                // $("<link/>", {
                //     rel: "stylesheet",
                //     type: "text/css",
                //     href: "http://formchat.dev.cc/assets/css/bootstrap-material-design.min.css"
                //  }).appendTo("head");
        
                $('#chatForm').submit(function () {
                    var url = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_url").val(parentUrl);
                    $('#chatForm').append(url);
        
                    var form_browser = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_browser").val(browser);
                    $('#chatForm').append(form_browser); 
        
                    var ip = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "ip_address").val(address);
                    $('#chatForm').append(ip);

                    if (typeof test_id !== 'undefined') {
                        
                        var test = $("<input>")
                                .attr("type", "hidden")
                                .attr("name", "test_id").val(test_id);
                        $('#chatForm').append(test);

                        var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_slug").val(form_slug);
                        $('#chatForm').append(input);
                    } else {
                        var form_slugs = $('div.form-chat').prop('id').substr(10);

                        var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_slug").val(form_slugs);
                        $('#chatForm').append(input);
                    }
                });
            });
        });
    });
})();

// http://facebook.com/anders.tornblad
// anders.tornblad@gmail.com

