$(document).ready(function() {
    
    $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
            address = data['ip'];
            console.log(address);   
        }); 

    var parentUrl = window.location.href;  
    var browser = Object.keys(jQuery.browser)[0];
    var form_id = $('div.form-chat').prop('id').split('-')[2];

    $('#chatForm').submit(function () {
        var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "form_id").val(form_id);
        $('#chatForm').append(input);

        var url = $("<input>")
                .attr("type", "hidden")
                .attr("name", "form_url").val(parentUrl);
        $('#chatForm').append(url);

        var form_browser = $("<input>")
                .attr("type", "hidden")
                .attr("name", "form_browser").val(browser);
        $('#chatForm').append(form_browser); 

        var ip = $("<input>")
                .attr("type", "hidden")
                .attr("name", "ip_address").val(address);
        $('#chatForm').append(ip);
    });
});