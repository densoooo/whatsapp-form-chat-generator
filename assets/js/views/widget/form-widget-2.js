(function() {

    // Localize jQuery variable
    var jQuery;
    console.log("FFF");
    
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute("src",
            "http://formchat.dev.cc/assets/js/core/jquery-3.3.1.min.js");
        if (script_tag.readyState) {
          script_tag.onreadystatechange = function () { // For old versions of IE
              if (this.readyState == 'complete' || this.readyState == 'loaded') {
                  scriptLoadHandler();
              }
          };
        } else { // Other browsers
          script_tag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }
    
    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main(); 
    }
    
    /******** Our main function ********/
    function main() { 
        jQuery(document).ready(function($) { 
            $.getMultiScripts = function(arr, path) {
                var _arr = $.map(arr, function(scr) {
                    return $.getScript( (path||"") + scr );
                });
            
                _arr.push($.Deferred(function( deferred ){
                    $( deferred.resolve );
                }));
            
                return $.when.apply($, _arr);
            }
        
            var script_arr = [
                'assets/js/core/popper.min.js', 
                'assets/js/core/bootstrap-material-design.min.js', 
                'assets/js/plugins/perfect-scrollbar.jquery.min.js',
                'assets/js/material-dashboard.1.js?v=2.1.1',
                'assets/js/plugins/jquery-migrate-1.4.1.min.js',
                'assets/js/views/widget/form-widget.min.js'
            ];
            
            $.getMultiScripts(script_arr, 'http://formchat.dev.cc/').done(function() {
                // all scripts loaded
                $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
                        address = data['ip'];
                        console.log(address);   
                    }); 
        
                var parentUrl = window.location.href;  
                var browser = Object.keys(jQuery.browser)[0];
                var form_id = $('div.form-chat').prop('id').split('-')[2];

                $("<link/>", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: "http://formchat.dev.cc/assets/css/test.css"
                 }).appendTo("head");

                //  $("<link/>", {
                //     rel: "stylesheet",
                //     type: "text/css",
                //     href: "http://formchat.dev.cc/assets/css/material-dashboard.css"
                //  }).appendTo("head");
                 
                // $("<link/>", {
                //     rel: "stylesheet",
                //     type: "text/css",
                //     href: "http://formchat.dev.cc/assets/css/bootstrap-material-design.min.css"
                //  }).appendTo("head");
        
                $('#chatForm').submit(function () {
                    var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_id").val(form_id);
                    $('#chatForm').append(input);
        
                    var url = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_url").val(parentUrl);
                    $('#chatForm').append(url);
        
                    var form_browser = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "form_browser").val(browser);
                    $('#chatForm').append(form_browser); 
        
                    var ip = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "ip_address").val(address);
                    $('#chatForm').append(ip);
                });
            });
        });
    }
    
    })(); // We call our anonymous function immediately