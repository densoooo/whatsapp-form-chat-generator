/* widget.js */
(function(window, document) {"use strict";  /* Wrap code in an IIFE */

var jQuery, $; // Localize jQuery variables

function loadScript(url, callback) {
  /* Load script from url and calls callback once it's loaded */
  var scriptTag = document.createElement('script');
  scriptTag.setAttribute("type", "text/javascript");
  scriptTag.setAttribute("src", url);
  if (typeof callback !== "undefined") {
    if (scriptTag.readyState) {
      /* For old versions of IE */
      scriptTag.onreadystatechange = function () { 
        if (this.readyState === 'complete' || this.readyState === 'loaded') {
          callback();
        }
      };
    } else {
      scriptTag.onload = callback;
    }
  }
  (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(scriptTag);
}

function main() {
  /* The main logic of our widget */
  jQuery(document).ready(function($) { 
    $.getMultiScripts = function(arr, path) {
        var _arr = $.map(arr, function(scr) {
            return $.getScript( (path||"") + scr );
        });
    
        _arr.push($.Deferred(function( deferred ){
            $( deferred.resolve );
        }));
    
        return $.when.apply($, _arr);
    }

    var script_arr = [
        'assets/js/core/popper.min.js', 
        'assets/js/core/bootstrap-material-design.min.js', 
        'assets/js/plugins/perfect-scrollbar.jquery.min.js',
        'assets/js/material-dashboard.1.js?v=2.1.1',
        'assets/js/plugins/jquery-migrate-1.4.1.js',
        'assets/js/views/widget/form-widget.min.js'
    ];
    
    $.getMultiScripts(script_arr, 'http://formchat.dev.cc/').done(function() {
        // all scripts loaded
        $.getJSON('https://api.ipify.org?format=jsonp&callback=?', function(data) {
                address = data['ip'];
                console.log(address);   
            }); 

        var parentUrl = window.location.href;  
        var browser = Object.keys(jQuery.browser)[0];
        var form_id = $('div.form-chat').prop('id').split('-')[2];

        $('#chatForm').submit(function () {
            var input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "form_id").val(form_id);
            $('#chatForm').append(input);

            var url = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "form_url").val(parentUrl);
            $('#chatForm').append(url);

            var form_browser = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "form_browser").val(browser);
            $('#chatForm').append(form_browser); 

            var ip = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "ip_address").val(address);
            $('#chatForm').append(ip);
        });
    });
});
}

/* Load jQuery */
loadScript("http://formchat.dev.cc/assets/js/core/jquery-3.3.1.min.js", function() {
  /* Restore $ and window.jQuery to their previous values and store the
     new jQuery in our local jQuery variables. */
  $ = jQuery = window.jQuery.noConflict(true);
  main(); /* Execute the main logic of our widget once jQuery is loaded */
});

}(window, document)); /* end IIFE */